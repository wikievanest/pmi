<nav class="navbar navbar-fixed-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                	
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <?php include "notification.php"; ?>
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $_SESSION['nama_lengkap']; ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                <?php 
						           	#include '../config/application.config.php';

						            
						            $user_id = $_SESSION['person_id'];
						            $sql = "SELECT a.foto,DATE_FORMAT(a.TMK,'%d %b %Y') FROM mst_karyawan a
						                	WHERE a.karyawan_id='".$user_id."'";
						            $result = $statement->query($sql);
						            $data = $statement->fetch_array($result);
						            $foto = $image->getimage($data[0]);
					            ?>				
					                        	
					                <?php 
					                	if($data[0]!="") {
					                ?>
                                    <img src="<?php echo $foto; ?>" class="img-circle" alt="User Image" />
                                    <?php 
                                    	} else {
									?>
									<img src="assets/img/avatar3.png" class="img-circle" alt="User Image" />
									<?php
										}
                                    ?>
                                    <p>
                                        <?php echo $_SESSION['nama_lengkap']; ?> - <?php echo $_SESSION['bagian']; ?>
                                        <small>Member since <?php echo $data[1]; ?></small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>