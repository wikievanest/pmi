        <!-- Bootstrap -->
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/js/plugins/morris/raphael-2.1.0.min.js" type="text/javascript"></script>
        <script src="assets/js/plugins/morris/morris.min.js" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="assets/js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="assets/js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="assets/js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="assets/js/plugins/datetimepicker/datetimepicker.min.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="assets/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="assets/js/plugins/chosen/chosen.jquery.min.js" type="text/javascript"></script>
        <!-- DATA TABES SCRIPT -->
        <script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- allcon App -->
        <script src="assets/js/allcon/app.js" type="text/javascript"></script>
         
    </body>
</html>