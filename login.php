<?php
use statements\statement;
include_once 'config/application.config.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login Sistem | PT. Pasadena Medical Indonesia  </title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="assets/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="assets/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="assets/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="assets/css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="assets/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="assets/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="assets/css/allcon.css" rel="stylesheet" type="text/css" />
        <script src="assets/js/jquery-2.0.3.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
<body class="bg-black">
	<script type="text/javascript">
    	
        function login(){
            var username=$('#username').val();
            var password=$('#password').val();
            var url=$('#url').val();
            $.ajax({
                    type: "POST",
                    url: "modules/login/login-access.php",
                    data: {"username":username,"password":password},
                    success: function(resp){   
                        if(!resp == "") {
                        	html = "<div class=\"alert alert-danger alert-dismissable\" style=\"margin-top: 5px;\">Username atau Password Salah</div>";
                        	document.getElementById('peringatan').innerHTML = html;
                        	document.getElementById('username').value = "";
                        	document.getElementById('password').value = "";
                        	$('#username').focus();
                        } else {
                            window.location.assign('index.php')
                   	 		
                        }
                    }
                });
        }
    </script>
    <?php 
    
   	$statement = new statement();
   	$image = new Image();
   	$sql = "SELECT a.NAMA_PERUSAHAAN,
			   	a.ALAMAT,
			   	a.TELEPON,
				a.FAX,
				a.EMAIL,
				a.LOGO
			FROM mst_perusahaan a";
   	$result = $statement->query($sql);
   	$row = $statement->fetch_row($result);
   	$foto = $image->getimage($row[5]);
    ?>
        <div class="form-box" id="login-box">
            <div class="header">
            	<div style="margin-bottom: 10px">Sistem Login</div>
            	<div style="text-align: center">
            		<?php 
						if($row[0]!="") {
					?>
              	        <img src="<?php echo $foto; ?>" class="img-circle" alt="User Image" width="150px"/>
                    <?php 
                       	} else {
						?>
						<img src="assets/img/avatar3.png" class="img-circle" alt="User Image" width="150px" />
						<?php
						}
                    ?>
            	</div>
            </div>
            <form action="javascript:login();" method="POST">
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" id="username" class="form-control" placeholder="User ID" autofocus required/>
                    </div>
                    <div class="form-group">
                        <input type="password" id="password" class="form-control" placeholder="Password" required/>
                    </div>          
                    <div id="peringatan">
                    </div>
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block">Sign In</button>  
                </div>
            </form>

            <div class="margin text-center">
                <span><?php echo $row[0]; ?></span><br>
                <span><?php echo $row[1]; ?></span><br>
                <span>Telp: <?php echo $row[2]; ?></span><br>
                <span>Fax: <?php echo $row[3]; ?></span><br>
                
                <br/>

            </div>
        </div>
        <!-- Bootstrap -->
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/js/plugins/morris/raphael-2.1.0.min.js" type="text/javascript"></script>
        <script src="assets/js/plugins/morris/morris.min.js" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="assets/js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="assets/js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="assets/js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="assets/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="assets/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        
        <!-- DATA TABES SCRIPT -->
        <script src="assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- allcon App -->
        <script src="assets/js/allcon/app.js" type="text/javascript"></script>
    </body>
</html>