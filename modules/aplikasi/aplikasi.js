function refresh() {
	document.location.reload();
}

function kosongkan(){
	  var sett_id = document.getElementById("sett_id");
	  sett_id.value='';
	  var jatuh_tempo = document.getElementById("jatuh_tempo");
	  jatuh_tempo.value='';
	  var ppn = document.getElementById("ppn");
	  ppn.value='';
	  var pph = document.getElementById("pph");
	  pph.value='';
	};

function MsgBox(res){
	if(res=="i"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil disimpan.</div>";
	}else if(res=="d"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil dihapus.</div>";
	}else if(res=="u"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil diupdate.</div>";
	}else if(res=="g"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data gagal diproses.</div>";
	}	
}
	
function tambahData(){
		$.ajax({
				type: "POST",
				url: "modules/aplikasi/aplikasi.form.php",
				success: function(resp){
				document.getElementById("container").innerHTML="";
				$("#container").html(resp);
				},
			});
};

function simpanData(){
	var sett_id = $('#sett_id').val();
	var jatuh_tempo = $('#jatuh_tempo').val();
	var ppn = $('#ppn').val();
	var pph = $('#pph').val();
	if(sett_id =="") {
		if(jatuh_tempo ==""){
			document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
			$('#jatuh_tempo').focus();
		} else {
			$.ajax({
				type: "POST",
				url: "modules/aplikasi/aplikasi.action.php",
				data: {"sett_id":sett_id,"jatuh_tempo":jatuh_tempo,"ppn":ppn,"pph":pph,},
				success: function(resp){	
					MsgBox(resp);				
					refresh();						
					},	
			});	
		}
	} else {
		updateData(sett_id);
	}
					
};

function editData(a,b,c,d) {
	$.ajax({
		type: "POST",
		url: "modules/aplikasi/aplikasi.form.php",
		success: function(resp){
			document.getElementById("container").innerHTML="";
			$("#container").html(resp);
			document.getElementById("title").innerHTML="";
			document.getElementById("title").innerHTML="Edit Setting";
			var sett_id = document.getElementById("sett_id");
			sett_id.value=a;
			var jatuh_tempo = document.getElementById("jatuh_tempo");
			jatuh_tempo.value=b;
			var ppn = document.getElementById("ppn");
			ppn.value=c;
			var pph = document.getElementById("pph");
			pph.value=d;
		},
	});	
}

function updateData(sett_id){
	var jatuh_tempo = $('#jatuh_tempo').val();
	var ppn = $('#ppn').val();
	var pph = $('#pph').val();
	if(jatuh_tempo ==""){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#jatuh_tempo').focus();
	} else {
		$.ajax({
			type: "POST",
			url: "modules/aplikasi/aplikasi.action.php",
			data: {"sett_id":sett_id,"jatuh_tempo":jatuh_tempo,"ppn":ppn,"pph":pph},
			success: function(resp){	
				MsgBox(resp);				
				refresh();						
				},	
		});	
	}				
};

function deleteData(a,b,c,d) {
	var r = confirm("Yakin data akan di hapus?");
	if(r==true) {
		$.ajax({
		  type: "POST",
		  url: "modules/aplikasi/aplikasi.action.php",
		  data: {"action":"delete","sett_id":a,"jatuh_tempo":b,"ppn":c,"pph":d},
		  success: function(resp){			
			  refresh();
		  },
		});
	};
}
