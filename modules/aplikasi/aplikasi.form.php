<?php 
$sql = "SELECT a.sett_id
			,a.jatuh_tempo
			,a.ppn
			,a.pph
		FROM app_setting a";
$result = $statement->query($sql);
$row = $statement->fetch_array($result);

?>
<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title" id="title">Setting Aplikasi</h3>
	</div>
	<br>
	<form class="form-horizontal">
	  <div class="form-group">
	    <label for="Menu" class="col-sm-2 control-label">Jatuh Tempo :</label>
	    <div class="col-sm-4">
	      <input type="hidden" class="form-control" id="sett_id">
	      <input type="number" class="form-control" id="jatuh_tempo" value="<?php echo $row['1'];?>" autofocus>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Deskripsi" class="col-sm-2 control-label">PPN ( % ) :</label>
	    <div class="col-sm-4">
	      <input type="number" class="form-control" id="ppn" value="<?php echo $row['2'];?>">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Deskripsi" class="col-sm-2 control-label">PPH ( % ) :</label>
	    <div class="col-sm-4">
	      <input type="number" class="form-control" id="pph" value="<?php echo $row['3'];?>">
	    </div>
	  </div>
	  <div class="col-sm-8">
		  <div id="loading"></div>
		  <div id="peringatan"></div>
	  </div>
	  <br>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="button" class="btn btn-primary" onclick="javascript:simpanData()"><i class="fa fa-save"></i> Simpan</button>
	      <button type="button" class="btn btn-default" onclick=javascript:refresh();><i class="fa fa-refresh"></i> Refresh</button>
	    </div>
	  </div>
		<div class="box-footer">
		</div>
	</form>
</div>
<!-- /.box -->
</div>
<!-- /.box -->