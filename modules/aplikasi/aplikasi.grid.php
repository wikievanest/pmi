<script>
    $(document).ready(function() {
		var data = "modules/aplikasi/aplikasi.data.php";
        var oTable = $('#tableaplikasi').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": false,
				"bFilter": false,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": false,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "jatuh_tempo","sWidth": "10%" },
					{ "mData": "ppn","sWidth": "10%" },
					{ "mData": "pph","sWidth": "10%" },
					{ "mData": "Aksi","sWidth": "5%" }
				]
		  });
        $('#tableaplikasi').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tableaplikasi" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Jatuh Tempo</th>
			<th>PPN</th>
			<th>PPH</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>