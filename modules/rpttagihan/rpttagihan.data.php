<?php 
include 'module.php';
$sql = "select 	a.PENJUALAN_ID, 
			d.TAGIHAN_ID,
			a.NO_TRAN, 
			a.NO_PO, 
			d.TOTAL_BAYAR, 
			DATE_FORMAT(a.CREATED,'%d-%m-%Y'),
			CASE d.STATUS_BAYAR WHEN 0
				THEN 'KREDIT'
			WHEN 1 
				THEN 'LUNAS'
			END AS STATUS,
			DATE_FORMAT(a.TGL_JATUH_TEMPO,'%d-%m-%Y')
		from tran_penjualan a
		inner join mst_user b
		on b.USER_ID=a.USER_ID
		inner join mst_karyawan c
		on c.KARYAWAN_ID=b.PERSON_ID
		inner join mst_tagihan d
		on d.NO_TRAN=a.NO_TRAN
		ORDER by a.NO_TRAN asc
		";
$result = $statement->query($sql);
$no = 1;
while($row = $statement->fetch_array($result)) {
	$orders[]= array(
			'No' => $no,
			'notran' => $row[2],
			'nopo' => $row[3],
			'total_bayar' => "Rp. ".number_format($row[4],0),
			'tgl_buat' => $row[5],
			'status' => $row[6],
			'tgl_jatuh_tempo' => $row[7],
			'Aksi' => "<button class=\"btn btn-sm btn-outline btn-primary\" onClick=\"javascript:detailData('$row[1]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Detail Data $row[2] \"><i class=\"fa fa-list\"></i> Detail</button>"

	);
	$no++;
}
$data= json_encode($orders);
print '{'.'"aaData":'.json_encode($orders).'}';

?>
