function refresh() {
	document.location.reload();
}

function newCode() {
	var grup_id = $('#grup_id').val();
	$.ajax({
		type: "POST",
		url: "modules/barang/barang.grup.php",
		data: {"grup_id":grup_id},
		success: function(resp){	
			$('#kode').val(resp);		
			$('#nama').focus();	
			},	
	});	
}

function kosongkan(){
	  var barang_id = document.getElementById("barang_id");
	  barang_id.value='';
	  var kode = document.getElementById("kode");
	  kode.value='';
	  var nama = document.getElementById("nama");
	  nama.value='';
	  var grup_id = document.getElementById("grup_id");
	  grup_id.value='';
	  var satuan_id = document.getElementById("satuan_id");
	  satuan_id.value='';
	  var hrg_beli = document.getElementById("hrg_beli");
	  hrg_beli.value='';
	  var hrg_jual = document.getElementById("hrg_jual");
	  hrg_jual.value='';
	  var stok = document.getElementById("stok");
	  stok.value='';
	};

function MsgBox(res){
	if(res=="i"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil disimpan.</div>";
	}else if(res=="d"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil dihapus.</div>";
	}else if(res=="u"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil diupdate.</div>";
	}else if(res=="g"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data gagal diproses.</div>";
	}	
}
	
function tambahData(){
		$.ajax({
				type: "POST",
				url: "modules/barang/barang.form.php",
				success: function(resp){
				document.getElementById("container").innerHTML="";
				$("#container").html(resp);
				},
			});
};


function simpanData(){
	var barang_id = $('#barang_id').val();
	var kode = $('#kode').val();
	var nama = $('#nama').val();
	var grup_id = $('#grup_id').val();
	var satuan_id = $('#satuan_id').val();
	var hrg_beli = $('#hrg_beli').val();
	var hrg_jual = $('#hrg_jual').val();
	var stok = $('#stok').val();
	if(barang_id =="") {
		if((kode =="") || (nama =="")){
			document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
			$('#supplier_id').focus();
		} else {
			$.ajax({
				type: "POST",
				url: "modules/barang/barang.action.php",
				data: {"barang_id":barang_id,"kode":kode,"nama":nama,"grup_id":grup_id,"satuan_id":satuan_id,"hrg_beli":hrg_beli,"hrg_jual":hrg_jual,"stok":stok},
				success: function(resp){	
					MsgBox(resp);				
					refresh();						
					},	
			});	
		}
	} else {
		updateData(barang_id);
	}
					
};

function editData(a,b,c,d,e,f,g,h) {
	$.ajax({
		type: "POST",
		url: "modules/barang/barang.form.php",
		success: function(resp){
			document.getElementById("container").innerHTML="";
			$("#container").html(resp);
			document.getElementById("title").innerHTML="";
			document.getElementById("title").innerHTML="Edit Data barang";
			var barang_id = document.getElementById("barang_id");
			barang_id.value=a;
			var kode = document.getElementById("kode");
			kode.value=b;
			var nama = document.getElementById("nama");
			nama.value=c;
			var grup_id = document.getElementById("grup_id");
			grup_id.value=d;
			var satuan_id = document.getElementById("satuan_id");
			satuan_id.value=e;
			var hrg_beli = document.getElementById("hrg_beli");
			hrg_beli.value=f;
			var hrg_jual = document.getElementById("hrg_jual");
			hrg_jual.value=g;
			var stok = document.getElementById("stok");
			stok.value=h;
		},
	});	
}

function updateData(barang_id){
	var kode = $('#kode').val();
	var nama = $('#nama').val();
	var grup_id = $('#grup_id').val();
	var satuan_id = $('#satuan_id').val();
	var hrg_beli = $('#hrg_beli').val();
	var hrg_jual = $('#hrg_jual').val();
	var stok = $('#stok').val();
	if(kode ==""){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#kode').focus();
	} else {
		$.ajax({
			type: "POST",
			url: "modules/barang/barang.action.php",
			data: {"barang_id":barang_id,"kode":kode,"nama":nama,"grup_id":grup_id,"satuan_id":satuan_id,"hrg_beli":hrg_beli,"hrg_jual":hrg_jual,"stok":stok},
			success: function(resp){	
				MsgBox(resp);				
				refresh();						
				},	
		});	
	}				
};

function deleteData(barang_id,hrg_jual,hrg_beli,stok) {
	var r = confirm("Yakin data akan di hapus?");
	if(r==true) {
		$.ajax({
		  type: "POST",
		  url: "modules/barang/barang.action.php",
		  data: "action=delete"+"&barang_id="+barang_id+"&hrg_jual="+hrg_jual+"&hrg_beli="+hrg_beli+"&stok="+stok,
		  success: function(resp){			
			  refresh();
		  },
		});
	};
}
