<script>
  $(function() {
       $('#tgl_jatuh_tempo').datetimepicker({
          pickTime: false,
        });
  });
</script>
<?php
include 'module.php';
$no_tran = $_POST['no_tran'];
?>
<div class="row">
<div class="col-xs-8">
<div class="box">
	<div class="box-body table-responsive no-padding">
		<table class="table table-hover">
			<tbody>
			<tr>
		    	<th>Kode</th>
		        <th>Nama Barang</th>
		        <th>Qty</th>
            	<th>Harga</th>
            	<th>Diskon	</th>
		        <th>Subtotal</th>
		    </tr>
		    <?php 
		    $sql = "SELECT a.penjualan_item_id,
				    	b.kode,
				    	b.nama_barang,
				    	a.jml,
				    	a.harga,
				    	a.diskon,
				    	a.subtotal
				    FROM tran_penjualan_item a
				    INNER JOIN mst_barang b
				    ON b.BARANG_ID=a.BARANG_ID
				    WHERE a.NO_TRAN='$no_tran'";
		    $result = $statement->query($sql);
		    while ($row=$statement->fetch_array($result)) {
		    	echo "<tr>";
				echo "<td style=\"width: 15%\">$row[1]</td>";
				echo "<td style=\"width: 35%\">$row[2]</td>";
				echo "<td style=\"width: 15%\">$row[3]</td>";
				echo "<td style=\"width: 15%\">".number_format($row[4], 0, '.', ',')."</td>";
				echo "<td style=\"width: 15%\">$row[5]</td>";
				echo "<td style=\"width: 20%\">".number_format($row[6], 0, '.', ',')."</td>";
				echo "<td><button type=\"button\" style=\"text-align: right\"
				class=\"btn btn-danger\" id=\"add\" onclick=\"javascript:deleteItem('$row[0]','$row[3]','$row[4]','$row[5]','$row[6]')\">
				<i class=\"fa fa-trash-o\"></i>
				</button></td>";
				echo "</tr>";
		    }
		    
		    ?>
		    </tbody>
		</table>
		</div>

</div>
</div>
<div class="col-xs-4">
<div class="box">
	<div class="box-body table-responsive no-padding">
		<table class="table">
		<?php		
		$sql = "select sum(a.subtotal) from tran_penjualan_item a
		WHERE a.no_tran='$no_tran'";
		$result = $statement->query($sql);
		$row = $statement->fetch_array($result);
		?>
			<tr>
				<th style="width:50%">Subtotal:</th>
					<td style="text-align: right"><b><?php echo number_format($row[0], 2, '.', ',');?></b></td>
				    <td><input type="hidden" style="text-align: right" class="form-control" id="subtotalx" name="subtotalx" value="<?php echo $row[0];?>"></td>
				</tr>
				<tr>
					<th>PPN (%) <!-- <button class="btn btn-small btn-primary" onclick="javascript:addPpn()"><i class="fa fa-plus"></i></button> --></th>
					<td><input type="number" style="text-align: right" class="form-control" id="ppn" name="ppn" placeholder="PPN" onchange="javascript:totalPPN()" value=0></td>
					<td><input type="hidden" style="text-align: right" class="form-control" id="total_ppn" name="total_ppn" value=0></td>
				</tr>
				<tr>
					<th>Total:</th>
					<td><input type="number" style="text-align: right" class="form-control" id="total" name="total" value="<?php echo $row[0];?>"></td>
				</tr>
				<tr>
					<th>Jatuh Tempo (Hari):</th>
					<td><input type="number" style="text-align: right" class="form-control" id="jatuh_tempo" name="jatuh_tempo"></td>
				</tr>
				<tr>
					<th>Status Pembayaran:</th>
					<td>
						<select id="status_bayar" class="form-control">
							<option value="">Pilih Status</option>
							<option value="1">TUNAI</option>
							<option value="0">KREDIT</option>
						</select>
					</td>
				</tr>
				<!-- 
				<tr>
					<th>Tanggal Jatuh Tempo:</th>
						<td><input type="text" class="form-control" id="tgl_jatuh_tempo" name="tgl_jatuh_tempo" placeholder="DD-MM-YYYY" data-format="DD-MM-YYYY"></td>
				</tr>
				 -->
				<tr>
					<td><button type="button" class="btn btn-success"  onclick="javascript:simpanData()"><i class="fa fa-credit-card"></i> Simpan</button>  </td>
					<!-- <th><button class="btn btn-default pull-right" onclick="window.print();"><i class="fa fa-print"></i> Print</button></th>  -->
				</tr>
		</table>
		</div>

</div>
</div>
</div>