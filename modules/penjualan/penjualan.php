<script>
  $(function() {
	  $("#pelanggan_id").chosen();
	  $("#salesman_id").chosen();
      $("#barang_id").chosen();
      noTran();
  });
</script>
<?php 
include 'module.php';
?>
<script src="modules/penjualan/penjualan.js"></script>
<div class="col-xs-12">
<div id="container">
	<div class="box box-primary">
	<div class="box-header">
		<h2 class="box-title" id="judul">Transaksi Penjualan</h2>
		<div style="text-align: right; margin-top: 10px; margin-right: 10px">
			<button class="btn btn-primary" onclick="javascript:refresh()"><i class="fa fa-plus"></i> Baru</button>
		</div>
	</div>
	<div class="box-body table-responsive">
		<form class="form-horizontal" method="POST" action="#" enctype="multipart/form-data">
	  	<div class="form-group">
	  	<button type="button" class="btn btn-danger btn-small" onclick=javascript:noTranPajak()><i class="fa fa-list"></i></button>
	  	<input type="hidden" class="form-control" id="penjualan_id" name="penjualan_id">
	    <label for="notran" class="col-sm-2 control-label">No. Transaksi :</label>
	    <div class="col-sm-3">
	    	<input type="text" class="form-control" id="no_tran" name="no_tran">
	    	
	    </div>
	    
	    <label for="notran" class="col-sm-2 control-label">Barang :</label>
	      <div class="col-sm-3">
	      <select id='barang_id' name='barang_id' class="form-control" onchange="javascript:cariBarang()">
		  	<option value="" selected>-- Pilih --</option>
			<?php
				$sql = "SELECT a.BARANG_ID,a.KODE,a.NAMA_BARANG from mst_barang a";
				$result=$statement->query($sql);
				while($row=$statement->fetch_array($result)){
			?>
			<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?> - <?php print $row[2]; ?></option>
			<?php
				}
			?>
		  </select> 
	  	  </div>
	  </div>
	  <div class="form-group">
	  	  <label for="notran" class="col-sm-2 control-label">No. PO :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="no_po" name="no_po" placeholder="No PO"> 
	      </div>
	      <label for="notran" class="col-sm-2 control-label">Satuan :</label>
	      <div class="col-sm-3">
	      	<select id='satuan_id' name='satuan_id' class="form-control" readonly autofocus>
		  	<option value="" selected>-- Pilih --</option>
			<?php
				$sql = "SELECT a.SATUAN_ID,a.SATUAN from mst_satuan a";
				$result=$statement->query($sql);
				while($row=$statement->fetch_array($result)){
			?>
			<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?></option>
			<?php
				}
			?>
		  </select>  
	      </div>
	  </div>
	  <div class="form-group">
	    <label for="notran" class="col-sm-2 control-label">Pelanggan :</label>
	    <div class="col-sm-3">
	      <select id='pelanggan_id' name='pelanggan_id' class="form-control" onchange="javascript:cariPelanggan()">
		  	<option value="" selected>-- Pilih --</option>
			<?php
				$sql = "SELECT a.PELANGGAN_ID,a.KODE,a.NAMA from mst_pelanggan a";
				$result=$statement->query($sql);
				while($row=$statement->fetch_array($result)){
			?>
			<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?> - <?php print $row[2]; ?></option>
			<?php
				}
			?>
		  </select> 
		 </div>
		 <label for="notran" class="col-sm-2 control-label">Qty :</label>
	    <div class="col-sm-3">
	      	<input type="number" style="text-align: right" class="form-control" id="jumlah" name="jumlah" onchange="javascript:jumlahItem()"> 
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="notran" class="col-sm-2 control-label">Nama Pelanggan :</label>
	    <div class="col-sm-3">
	      	<input type="text" class="form-control" id="nama_supp" name="nama_supp" readonly> 
	    </div>
	    <label for="notran" class="col-sm-2 control-label">Harga :</label>
	      <div class="col-sm-3">
	      	<input type="number" style="text-align: right" class="form-control" id="harga" name="harga" onchange="javascript:hargaItem()"> 
	      </div>
	  </div>
	  <div class="form-group">
	    <label for="notran" class="col-sm-2 control-label">Telepon :</label>
	    <div class="col-sm-3">
	      	<input type="text" class="form-control" id="telp_supp" name="telp_supp" readonly> 
	    </div>
	    <label for="notran" class="col-sm-2 control-label">Diskon :</label>
	    <div class="col-sm-3">
	      	<input type="number" style="text-align: right" class="form-control" id="diskonItem" name="diskonItem" onchange="javascript:totalDiskonItem()"> 
	    </div>
	  </div>
	  <div class="form-group">
	  <label for="salesman" class="col-sm-2 control-label">Salesman :</label>
		<div class="col-sm-3">
		<select id='salesman_id' name='salesman_id' class="form-control">
			<option value="" selected>-- Pilih Salesman --</option>
			<?php
				$sql = "SELECT a.KARYAWAN_ID,a.NAMA FROM mst_karyawan a
						WHERE a.BAGIAN='d34dd070-fdcd-11e3-9909-d63a9e6b40fc'";
				$result=$statement->query($sql);
				while($row=$statement->fetch_array($result)){
			?>
			<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?></option>
			<?php
				}
			?>
		</select>
		</div>
		<label for="notran" class="col-sm-2 control-label">Subtotal :</label>
	    <div class="col-sm-3">
	      	<input type="number" style="text-align: right" class="form-control" id="subtotal" name="subtotal"> 
	    </div>
	  </div>
	  <div class="form-group">
	  	<div class="col-sm-2"></div>
		<div class="col-sm-5">
	      	<button type="button" class="btn btn-primary" onclick="javascript:saveItem()"><i class="fa fa-save"></i> Tambah</button>
	    </div>
	  </div>
	  <div class="form-group">
	      <div class="col-sm-7">
	      	<div class="col-sm-8">
			  <div id="loading"></div>
			  <div id="peringatan"></div>
		  </div>
	      </div>
	      
	  </div>
	</form>
	</div>
</div>
</div>
<div id="gridItem"></div>
</div>