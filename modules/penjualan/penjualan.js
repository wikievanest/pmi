
function refresh() {
	document.location.reload();
}


function kosongkan(){
	  var barang_id = document.getElementById("barang_id");
	  barang_id.value='';
	  var satuan_id = document.getElementById("satuan_id");
	  satuan_id.value='';
	  var jumlah = document.getElementById("jumlah");
	  jumlah.value='';
	  var harga = document.getElementById("harga");
	  harga.value='';
	  var diskonItem = document.getElementById("diskonItem");
	  diskonItem.value='';
	  var subtotal = document.getElementById("subtotal");
	  subtotal.value='';
};
	
function noTran() {
	$.ajax({
		type: "POST",
		url: "modules/penjualan/penjualan.data.php",
		success: function(resp){	
			$('#no_tran').val(resp);		
			$('#no_po').focus();	
			},	
	});	
}

function noTranPajak() {
	$.ajax({
		type: "POST",
		url: "modules/penjualan/penjualan.data.php",
		data: "action=pakaipajak",
		success: function(resp){	
			var no_tran = document.getElementById("no_tran");
				no_tran.value='';
			$('#no_tran').val(resp);		
			$('#no_po').focus();	
			},	
	});	
}

function cariBarang() {
	var barang_id = $("#barang_id").val();
		$.ajax({
			type: "POST",
			url: "modules/penjualan/penjualan.barang.php",
			data: "action=caribarang"+"&barang_id="+barang_id,
			success: function(resp){
				var obj = jQuery.parseJSON(resp);
				$('#harga').val(obj.harga_jual);
				$('#satuan_id').val(obj.satuan_id);
				$('#jumlah').focus();
			},
		});
}
function cariPelanggan() {
	var pelanggan_id = $("#pelanggan_id").val();
		$.ajax({
			type: "POST",
			url: "modules/penjualan/penjualan.barang.php",
			data: "action=caripelanggan"+"&pelanggan_id="+pelanggan_id,
			success: function(resp){
				var obj = jQuery.parseJSON(resp);
				$('#nama_supp').val(obj.nama);
				$('#telp_supp').val(obj.telepon);
				$('#barang_id').focus();
			},
		});
}

function jumlahItem() {
	var jumlah = $("#jumlah").val();
	var harga = $("#harga").val();
	var subtotal = jumlah * harga;
	$("#subtotal").val(subtotal)
	$("#diskonItem").focus()
}


function hargaItem() {
	var jumlah = $("#jumlah").val();
	var harga = $("#harga").val();
	var subtotal = jumlah * harga;
	$("#subtotal").val(subtotal)
	$("#subtotal").focus()
}

function totalDiskonItem() {
	var jumlah = $("#jumlah").val();
	var harga = $("#harga").val();
	var diskonItem = parseInt($("#diskonItem").val());
	var subtotal = jumlah * harga;
	var totalDiskonItem= diskonItem * subtotal /100;
	var total = subtotal - totalDiskonItem;
	$("#subtotal").val(total);
	
}

function totalDiskon() {
	var subtotalx = parseInt($("#subtotalx").val());
	var diskon = parseInt($("#diskon").val());
	var totaldiskon = diskon * subtotalx /100;
	$("#total_diskon").val(totaldiskon);
	var total = subtotalx - totaldiskon;
	$("#total").val(total);
	
}

function totalPPN() {
	var subtotalx = parseInt($("#subtotalx").val());
	var ppn = parseInt($("#ppn").val());
	var total_ppn = ppn * subtotalx / 100;
	$("#total_ppn").val(total_ppn);
	var total = parseInt($("#subtotalx").val()) + total_ppn ;
	$("#total").val(total);
	$("#status_bayar").focus();
}

function addPpn() {
	xppn = document.getElementById("ppn");
	xppn.value="";
	xppn.value=10;
	totalPPN();
	$("#ppn").focus();
	
}
function saveItem() {
	var no_tran = $("#no_tran").val();
	var pelanggan_id = $("#pelanggan_id").val();
	var barang_id = $("#barang_id").val();
	var jumlah = $("#jumlah").val();
	var diskonItem = $("#diskonItem").val();
	var harga = $("#harga").val();
	var subtotal = $("#subtotal").val();
	if(no_tran == "" || barang_id =="" || jumlah =="" || harga =="" || subtotal =="" || pelanggan_id ==""){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#menu_id').focus();
	} else {
		document.getElementById("loading").innerHTML="<img src='assets/img/ajax-loader.gif' height='32' width='32'/> loading ...";
			$.ajax({
				type: "POST",
				url: "modules/penjualan/penjualan.item.action.php",
				data: "barang_id="+barang_id+"&jumlah="+jumlah+"&harga="+harga+"&diskonItem="+diskonItem+"&subtotal="+subtotal+"&no_tran="+no_tran+"&pelanggan_id="+pelanggan_id,
				success: function(resp){
					document.getElementById("loading").innerHTML="";
					addItem();
					totalItem();
					kosongkan();
				},
			});
	}
}

function addItem() {
	var no_tran = $("#no_tran").val();
	document.getElementById("loading").innerHTML="<img src='assets/img/ajax-loader.gif' height='32' width='32'/> update keranjang ...";
		$.ajax({
			type: "POST",
			url: "modules/penjualan/penjualan.item.php",
			data: "no_tran="+no_tran,
			success: function(resp){
				document.getElementById("loading").innerHTML="";
				$("#gridItem").html(resp);				
			},
		});
}

function deleteItem(a,b,c,d,e) {
	$.ajax({
		type: "POST",
		url: "modules/penjualan/penjualan.item.action.php",
		data: "action=delete"+"&penjualan_item_id="+a+"&jumlah="+b+"&harga="+c+"&diskonItem="+d+"&subtotal="+e,
		success: function(resp){
			addItem();
			totalItem();
		},
	});
}

function totalItem() {
	var no_tran = $("#no_tran").val();
		$.ajax({
			type: "POST",
			url: "modules/penjualan/penjualan.item.total.php",
			data: "action=totalItem&no_tran="+no_tran,
			success: function(resp){
				$("#totalBayar").html(resp);
			},
		});
}

function simpanData(){
	var penjualan_id = $('#penjualan_id').val();
	var no_tran = $('#no_tran').val();
	var no_po = $('#no_po').val();
	var subtotalx = $('#subtotalx').val();
	var ppn = $('#total_ppn').val();
	var total = $('#total').val();
	var status = $('#status_bayar').val();
	var jatuh_tempo = $('#jatuh_tempo').val();
	var salesman_id = $('#salesman_id').val();
	if(total=="") {
		alert('Total Bayar Masih Kosong');
		$('#total').focus();
	} else if(status == "") {
		alert('Silahkan Pilih Status Terlebihi dahulu');
		$('#status_bayar').focus();
	} else {
		$.ajax({
			type: "POST",
			url: "modules/penjualan/penjualan.action.php",
			data: {"penjualan_id":penjualan_id,"no_tran":no_tran,"no_po":no_po,"subtotalx":subtotalx,"ppn":ppn,"total":total,"status":status,"jatuh_tempo":jatuh_tempo,"salesman_id":salesman_id},
			success: function(resp){	
					//refresh();
				if(resp=='i') {
					window.location.assign('reports.php?type=faktur&notran='+no_tran);
				}
					
				},	
		});
	}
	
					
};