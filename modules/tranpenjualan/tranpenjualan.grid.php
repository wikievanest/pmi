<script>
    $(document).ready(function() {
		var data = "modules/tranpenjualan/tranpenjualan.data.php";
        var oTable = $('#tabletranpenjualan').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": true,
				"bFilter": true,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": false,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "notran","sWidth": "10%" },
					{ "mData": "nopo","sWidth": "10%" },
					{ "mData": "total_bayar","sWidth": "8%" },
					{ "mData": "tgl_buat","sWidth": "10%" },
					{ "mData": "status","sWidth": "5%" },
					{ "mData": "tgl_jatuh_tempo","sWidth": "8%" },
					{ "mData": "Aksi","sWidth": "10%" }
				]
		  });
        $('#tabletranpenjualan').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tabletranpenjualan" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>No. Tran</th>
			<th>No. PO</th>
			<th>Total</th>
			<th>Tanggal</th>
			<th>Status</th>
			<th>Jatuh Tempo</th>
			<th>Detail</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>