<?php 
include 'module.php';
$sql = "select 	a.PENJUALAN_ID, 
			a.NO_TRAN, 
			a.NO_PO, 
			a.TOTAL_BAYAR, 
			DATE_FORMAT(a.CREATED,'%d-%m-%Y %h:%m:%s'),
			CASE STATUS_BAYAR WHEN 0
				THEN 'KREDIT'
			WHEN 1 
				THEN 'LUNAS'
			END AS STATUS,
			DATE_FORMAT(a.TGL_JATUH_TEMPO,'%d-%m-%Y')
		from tran_penjualan a
		inner join mst_user b
		on b.USER_ID=a.USER_ID
		inner join mst_karyawan c
		on c.KARYAWAN_ID=b.PERSON_ID
		ORDER by a.NO_TRAN asc
		";
$result = $statement->query($sql);
$no = 1;
while($row = $statement->fetch_array($result)) {
	$orders[]= array(
			'No' => $no,
			'notran' => $row[1],
			'nopo' => $row[2],
			'total_bayar' => "Rp. ".number_format($row[3],0),
			'tgl_buat' => $row[4],
			'status' => $row[5],
			'tgl_jatuh_tempo' => $row[6],
			'Aksi' => "
			<button class=\"btn btn-sm btn-outline btn-primary\" onClick=\"javascript:detailData('$row[1]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Detail Data $row[2] \"><i class=\"fa fa-list\"></i> Detail</button>
			<a href=\"reports.php?type=pdf&act=printfaktur&notran=$row[1]\" target=\"_blank\" class=\"btn btn-sm btn-outline btn-success\"><i class=\"fa fa-print\"></i> Faktur</a>"

	);
	$no++;
}
$data= json_encode($orders);
print '{'.'"aaData":'.json_encode($orders).'}';

?>
