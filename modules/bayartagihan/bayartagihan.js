
function refresh() {
	document.location.reload();
}

function noTran() {
	$.ajax({
		type: "POST",
		url: "modules/bayartagihan/bayartagihan.data.php",
		data: "action=getCode",
		success: function(resp){	
			$('#kode_tran').val(resp);		
			$('#no_tran').focus();	
			},	
	});	
}

function MsgBox(res){
	if(res=="i"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil disimpan.</div>";
	}else if(res=="d"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil dihapus.</div>";
	}else if(res=="u"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil diupdate.</div>";
	}else if(res=="g"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data gagal diproses.</div>";
	}	
}

function kosongkan(){
	  var tagihan_id = document.getElementById("tagihan_id");
	  tagihan_id.value='';
	  var notran = document.getElementById("no_tran");
	  notran.value='';
	  var nama = document.getElementById("nama");
	  nama.value='';
	  var telepon = document.getElementById("telepon");
	  telepon.value='';
	  var total_bayar = document.getElementById("total_bayar");
	  total_bayar.value='';
	  var total_tagihan = document.getElementById("total_tagihan");
	  total_tagihan.value='';
	  var bayar = document.getElementById("bayar");
	  bayar.value='';
	  var sisa_tagihan = document.getElementById("sisa_tagihan");
	  sisa_tagihan.value='';
	  $("#no_tran").focus();
};
	
function cariData() {
	var no_tran = $("#no_tran").val();
		$.ajax({
			type: "POST",
			url: "modules/bayartagihan/bayartagihan.data.php",
			data: "action=tagihan"+"&no_tran="+no_tran,
			success: function(resp){
				if(resp==000) {
					alert('Data Tagihan No. Transaksi '+no_tran+' Tidak ada');
					kosongkan();
					
				} else {
					cariPelanggan();
					var obj = jQuery.parseJSON(resp);
					$('#tagihan_id').val(obj.tagihan_id);
					$('#total_bayar').val(obj.total_bayar);
					$('#total_tagihan').val(obj.total_tagihan);
					$("#bayar").focus();
				}
				
			},
		});
}

function cariPelanggan() {
	var no_tran = $("#no_tran").val();
		$.ajax({
			type: "POST",
			url: "modules/bayartagihan/bayartagihan.data.php",
			data: "action=pelanggan"+"&no_tran="+no_tran,
			success: function(resp){
				var obj = jQuery.parseJSON(resp);
				$('#nama').val(obj.nama);
				$('#telepon').val(obj.telepon);
			},
		});
}

function bayarTagihan() {
	var total_tagihan = $("#total_tagihan").val();
	var bayar = $("#bayar").val();
	var sisa_tagihan =  total_tagihan - bayar;
	$("#sisa_tagihan").val(sisa_tagihan)
}

function simpanData(){
	var kode_tran = $('#kode_tran').val();
	var tagihan_id = $('#tagihan_id').val();
	var bayar = $('#bayar').val();
	var no_tran = $('#no_tran').val();
	if(no_tran == "") {
		alert('No Tagihan Masih Kosong');
		$('#no_tran').focus();
	} else if(bayar == "") {
		alert('Total Bayar Masih Kosong');
		$('#bayar').focus();
	} else {
		$.ajax({
			type: "POST",
			url: "modules/bayartagihan/bayartagihan.action.php",
			data: {"kode_tran":kode_tran,"tagihan_id":tagihan_id,"bayar":bayar},
			success: function(resp){	
				MsgBox(resp);
				kosongkan();
				noTran();
				},	
		});
	}
	
					
};