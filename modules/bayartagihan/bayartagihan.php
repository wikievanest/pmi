<script>
  $(function() {
      noTran();
  });
</script>
<?php 
include 'module.php';
?>
<script src="modules/bayartagihan/bayartagihan.js"></script>
<div class="col-xs-12">
<div id="container">
	<div class="box box-primary">
	<div class="box-header">
		<h2 class="box-title" id="judul">Transaksi Bayar Tagihan</h2>
		<div style="text-align: right; margin-top: 10px; margin-right: 10px">
			<button class="btn btn-primary" onclick="javascript:refresh()"><i class="fa fa-plus"></i> Baru</button>
		</div>
	</div>
	<div class="box-body table-responsive">
		<form class="form-horizontal" method="POST" action="#" enctype="multipart/form-data">
		  <div class="form-group">
		    <label for="notran" class="col-sm-2 control-label">No. Transaksi :</label>
		    <div class="col-sm-3">
		    	<input type="text" class="form-control" id="kode_tran" name="kode_tran" readonly> 
		    </div>
		    <label for="notran" class="col-sm-2 control-label">Total Bayar :</label>
		    <div class="col-sm-3">
		    	<input style="text-align: right" type="text" class="form-control" id="total_bayar" name="total_bayar" readonly>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="notran" class="col-sm-2 control-label">No. Tagihan :</label>
		    <div class="col-sm-3">
		    	<input type="text" class="form-control" id="no_tran" name="no_tran" onchange="javascript:cariData();" autofocus > 
		    	<input type="hidden" class="form-control" id="tagihan_id" name="tagihan_id">
		    </div>
		    <label for="notran" class="col-sm-2 control-label">Total Tagihan :</label>
		    <div class="col-sm-3">
		    	<input style="text-align: right" type="text" class="form-control" id="total_tagihan" name="total_tagihan" readonly>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="notran" class="col-sm-2 control-label">Nama Pelanggan :</label>
		    <div class="col-sm-3">
		    	<input type="text" class="form-control" id="nama" name="nama" readonly>
		    </div>
		    <label for="notran" class="col-sm-2 control-label">Bayar :</label>
		    <div class="col-sm-3">
		    	<input style="text-align: right" type="text" class="form-control" id="bayar" name="bayar" onchange="javascript:bayarTagihan();">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="notran" class="col-sm-2 control-label">Telepon :</label>
		    <div class="col-sm-3">
		    	<input type="text" class="form-control" id="telepon" name="telepon" readonly>
		    </div>
		    <label for="notran" class="col-sm-2 control-label">Sisa Tagihan :</label>
		    <div class="col-sm-3">
		    	<input style="text-align: right" type="text" class="form-control" id="sisa_tagihan" name="sisa_tagihan" readonly>
		    </div>
		  </div>
		  <div class="col-sm-8">
			  <div id="loading"></div>
			  <div id="peringatan"></div>
		  </div>
		  <br>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="button" class="btn btn-primary" onclick="javascript:simpanData()"><i class="fa fa-save"></i> Simpan</button>
		      <button type="button" class="btn btn-default" onclick=javascript:refresh();><i class="fa fa-refresh"></i> Refresh</button>
		    </div>
		  </div>
			<div class="box-footer">
			</div>
		</form>
	</div>
</div>
</div>
<div id="gridItem"></div>
</div>