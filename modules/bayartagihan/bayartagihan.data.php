<?php
include 'module.php';
$action = $_POST['action'];
$no_tran = $_POST['no_tran'];
if($action == "tagihan") {
	$sql = "SELECT a.TAGIHAN_ID,
				a.TOTAL_BAYAR,
				a.TOTAL_TAGIHAN
			FROM mst_tagihan a 
			WHERE a.STATUS_BAYAR = 0
			AND a.NO_TRAN='$no_tran'
			";
	$result = $statement->query($sql);
	$row = $statement->fetch_array($result);
	if($row['0'] != '') {
		$data = array(
				'tagihan_id' => $row['0'],
				'total_bayar' => $row['1'],
				'total_tagihan' => $row['2'],
		);
		echo json_encode($data);
	} else {
		echo 000;
	}
	
} elseif ($action == "pelanggan")  {
	$sql = "SELECT a.NAMA,
				a.TELEPON
			FROM mst_pelanggan a
			INNER JOIN tran_penjualan_item b
			ON a.PELANGGAN_ID=b.PELANGGAN_ID
			WHERE b.NO_TRAN='$no_tran'
			";
	$result = $statement->query($sql);
	$row = $statement->fetch_array($result);
	$data = array(
			'nama' => $row['0'],
			'telepon' =>  $row['1'],
			);
	echo json_encode($data);
} elseif($action=='getCode') {
	$date = date('dm');
	$code = $date.'TGH';
	//Mencari Maximal NO Transaksi
	$sql1 = "select MAX(a.KODE_TRAN) from tran_tagihan a where a.KODE_TRAN LIKE '$code%'";
	$result1=$statement->query($sql1);
	$row1 = $statement->fetch_array($result1);
	//Membuat Kode Barang Baru
	$sql2 = "SELECT SUBSTRING('".$row1[0]."' FROM 8 + 1)+1";
	$result2=$statement->query($sql2);
	$row2 = $statement->fetch_array($result2);
	$notran= sprintf("%04s",$row2[0]);
	echo $code.$notran;
}