function refresh() {
	document.location.reload();
}

function kosongkan(){
	  var supplier_id = document.getElementById("supplier_id");
	  supplier_id.value='';
	  var kode = document.getElementById("kode");
	  kode.value='';
	  var nama = document.getElementById("nama");
	  nama.value='';
	  var alamat = document.getElementById("alamat");
	  alamat.value='';
	  var kota = document.getElementById("kota");
	  kota.value='';
	  var telepon = document.getElementById("telepon");
	  telepon.value='';
	  var hp = document.getElementById("hp");
	  hp.value='';
	  var fax = document.getElementById("fax");
	  fax.value='';
	  var kontak = document.getElementById("kontak");
	  kontak.value='';
	  var bank = document.getElementById("bank");
	  bank.value='';
	  var alm_bank = document.getElementById("alm_bank");
	  alm_bank.value='';
	  var rekening = document.getElementById("rekening");
	  rekening.value='';
	  var an_bank = document.getElementById("an_bank");
	  an_bank.value='';
	  var nm_produk = document.getElementById("nm_produk");
	  nm_produk.value='';
	  var pkp = document.getElementById("pkp");
	  pkp.value='';
	  var alm_pkp = document.getElementById("alm_pkp");
	  alm_pkp.value='';
	  var nm_pkp = document.getElementById("nm_pkp");
	  nm_pkp.value='';
	  var no_pkp = document.getElementById("no_pkp");
	  no_pkp.value='';
	  var tgl_pkp = document.getElementById("tgl_pkp");
	  tgl_pkp.value='';
	  var npwp = document.getElementById("npwp");
	  npwp.value='';
	  var no_seri1 = document.getElementById("no_seri1");
	  no_seri1.value='';
	  var no_seri2 = document.getElementById("no_seri2");
	  no_seri2.value='';
	  var salesman_id = document.getElementById("salesman_id");
	  salesman_id.value='';
	};

function MsgBox(res){
	if(res=="i"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil disimpan.</div>";
	}else if(res=="d"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil dihapus.</div>";
	}else if(res=="u"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil diupdate.</div>";
	}else if(res=="g"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data gagal diproses.</div>";
	}	
}
	
function tambahData(){
		$.ajax({
				type: "POST",
				url: "modules/supplier/supplier.form.php",
				success: function(resp){
				document.getElementById("container").innerHTML="";
				$("#container").html(resp);
				},
			});
};


function simpanData(){
	var supplier_id = $('#supplier_id').val();
	var kode = $('#kode').val();
	var nama = $('#nama').val();
	var alamat = $('#alamat').val();
	var kota = $('#kota').val();
	var hp = $('#hp').val();
	var telepon = $('#telepon').val();
	var hp = $('#hp').val();
	var fax = $('#fax').val();
	var kontak = $('#kontak').val();
	var bank = $('#bank').val();
	var alm_bank = $('#alm_bank').val();
	var rekening = $('#rekening').val();
	var an_bank = $('#an_bank').val();
	var nm_produk = $('#nm_produk').val();
	var pkp = $('#pkp').val();
	var alm_pkp = $('#alm_pkp').val();
	var nm_pkp = $('#nm_pkp').val();
	var no_pkp = $('#no_pkp').val();
	var tgl_pkp = $('#tgl_pkp').val();
	var npwp = $('#npwp').val();
	var no_seri1 = $('#no_seri1').val();
	var no_seri2 = $('#no_seri2').val();
	var salesman_id = $('#salesman_id').val();
	
	if(supplier_id =="") {
		if(nama == "" || salesman_id ==""){
			document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
			$('#kode').focus();
		} else {
			$.ajax({
				type: "POST",
				url: "modules/supplier/supplier.action.php",
				data: {"supplier_id":supplier_id,"kode":kode,"nama":nama,"alamat":alamat,"hp":hp,"telepon":telepon,"hp":hp,"fax":fax,"kontak":kontak,"bank":bank,"alm_bank":alm_bank,"rekening":rekening,"an_bank":an_bank,"nm_produk":nm_produk,"pkp":pkp,"alm_pkp":alm_pkp,"nm_pkp":nm_pkp,"no_pkp":no_pkp,"tgl_pkp":tgl_pkp,"npwp":npwp,"no_seri1":no_seri1,"no_seri2":no_seri2,"salesman_id":salesman_id,"kota":kota},
				success: function(resp){	
					MsgBox(resp);				
					refresh();						
					},	
			});	
		}
	} else {
		updateData(supplier_id);
	}
					
};

function editData(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w) {
	$.ajax({
		type: "POST",
		url: "modules/supplier/supplier.form.php",
		success: function(resp){
			document.getElementById("container").innerHTML="";
			$("#container").html(resp);
			document.getElementById("title").innerHTML="";
			document.getElementById("title").innerHTML="Edit Data supplier";
			var supplier_id = document.getElementById("supplier_id");
			  supplier_id.value=a;
			  var kode = document.getElementById("kode");
			  kode.value=b;
			  var nama = document.getElementById("nama");
			  nama.value=c;
			  var alamat = document.getElementById("alamat");
			  alamat.value=d;
			  var kota = document.getElementById("kota");
			  kota.value=e;
			  var telepon = document.getElementById("telepon");
			  telepon.value=f;
			  var hp = document.getElementById("hp");
			  hp.value=g;
			  var fax = document.getElementById("fax");
			  fax.value=h;
			  var kontak = document.getElementById("kontak");
			  kontak.value=i;
			  var bank = document.getElementById("bank");
			  bank.value=j;
			  var alm_bank = document.getElementById("alm_bank");
			  alm_bank.value=k;
			  var rekening = document.getElementById("rekening");
			  rekening.value=l;
			  var an_bank = document.getElementById("an_bank");
			  an_bank.value=m;
			  var nm_produk = document.getElementById("nm_produk");
			  nm_produk.value=n;
			  var pkp = document.getElementById("pkp");
			  pkp.value=o;
			  var alm_pkp = document.getElementById("alm_pkp");
			  alm_pkp.value=p;
			  var nm_pkp = document.getElementById("nm_pkp");
			  nm_pkp.value=q;
			  var no_pkp = document.getElementById("no_pkp");
			  no_pkp.value=r;
			  var tgl_pkp = document.getElementById("tgl_pkp");
			  tgl_pkp.value=s;
			  var npwp = document.getElementById("npwp");
			  npwp.value=t;
			  var no_seri1 = document.getElementById("no_seri1");
			  no_seri1.value=u;
			  var no_seri2 = document.getElementById("no_seri2");
			  no_seri2.value=v;
			  var salesman_id = document.getElementById("salesman_id");
			  salesman_id.value=w;
		},
	});	
}

function updateData(supplier_id){
	var supplier_id = $('#supplier_id').val();
	var kode = $('#kode').val();
	var nama = $('#nama').val();
	var alamat = $('#alamat').val();
	var kota = $('#kota').val();
	var hp = $('#hp').val();
	var telepon = $('#telepon').val();
	var hp = $('#hp').val();
	var fax = $('#fax').val();
	var kontak = $('#kontak').val();
	var bank = $('#bank').val();
	var alm_bank = $('#alm_bank').val();
	var rekening = $('#rekening').val();
	var an_bank = $('#an_bank').val();
	var nm_produk = $('#nm_produk').val();
	var pkp = $('#pkp').val();
	var alm_pkp = $('#alm_pkp').val();
	var nm_pkp = $('#nm_pkp').val();
	var no_pkp = $('#no_pkp').val();
	var tgl_pkp = $('#tgl_pkp').val();
	var npwp = $('#npwp').val();
	var no_seri1 = $('#no_seri1').val();
	var no_seri2 = $('#no_seri2').val();
	var salesman_id = $('#salesman_id').val();
	if((nama =="") || (salesman_id =="")){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#nama').focus();
	} else {
		$.ajax({
			type: "POST",
			url: "modules/supplier/supplier.action.php",
			data: {"supplier_id":supplier_id,"kode":kode,"nama":nama,"alamat":alamat,"hp":hp,"telepon":telepon,"hp":hp,"fax":fax,"kontak":kontak,"bank":bank,"alm_bank":alm_bank,"rekening":rekening,"an_bank":an_bank,"nm_produk":nm_produk,"pkp":pkp,"alm_pkp":alm_pkp,"nm_pkp":nm_pkp,"no_pkp":no_pkp,"tgl_pkp":tgl_pkp,"npwp":npwp,"no_seri1":no_seri1,"no_seri2":no_seri2,"salesman_id":salesman_id,"kota":kota},
			success: function(resp){	
				MsgBox(resp);				
				refresh();						
				},	
		});	
	}				
};

function deleteData(supplier_id,tgl_pkp) {
	var r = confirm("Yakin data akan di hapus?");
	if(r==true) {
		$.ajax({
		  type: "POST",
		  url: "modules/supplier/supplier.action.php",
		  data: "action=delete"+"&supplier_id="+supplier_id+"&tgl_pkp="+tgl_pkp,
		  success: function(resp){			
			  refresh();
		  },
		});
	};
}
