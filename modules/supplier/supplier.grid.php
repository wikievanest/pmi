<script>
    $(document).ready(function() {
		var data = "modules/supplier/supplier.data.php";
        var oTable = $('#tablesupplier').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": true,
				"bFilter": true,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": false,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "kode","sWidth": "8%" },
					{ "mData": "nama","sWidth": "20%" },
					{ "mData": "alamat","sWidth": "25%" },
					{ "mData": "telepon","sWidth": "10%" },
					{ "mData": "salesman","sWidth": "10%" },
					{ "mData": "Aksi","sWidth": "10%" }
				]
		  });
        $('#tablesupplier').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tablesupplier" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode</th>
			<th>Nama Supplier</th>
			<th>Alamat</th>
			<th>Telepon</th>
			<th>Salesman</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>