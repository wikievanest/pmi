<script>
  $(function() {
       $('#tgl_pkp').datetimepicker({
          pickTime: false,
        });
  });
</script>
<?php 
include 'module.php';
?>
<script src="modules/supplier/supplier.js"></script>
	<div class="box box-primary">
	<div class="box-header">
		<h2 class="box-title" id="title">Data Supplier</h2>
	</div>
	<div class="box-body table-responsive">
		<form class="form-horizontal" method="POST" action="#" enctype="multipart/form-data">
	  <div class="form-group">
	  	<input type="hidden" class="form-control" id="supplier_id" name="supplier_id">
	    <label for="notran" class="col-sm-2 control-label">Kode Supplier :</label>
	    <div class="col-sm-3">
	    	<input type="text" class="form-control" id="kode" name="kode" placeholder="Kode Supplier"> 
	    </div>
	    <label for="notran" class="col-sm-2 control-label">Nama Produk :</label>
	    <div class="col-sm-3">
	    	<input type="text" class="form-control" id="nm_produk" name="nm_produk" placeholder="Nama Produk"> 
	    </div>
	  </div>
	  <div class="form-group">
	  	  <label for="notran" class="col-sm-2 control-label">Nama Supplier :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Supplier">	
	  	  </div>
	  	  <label for="notran" class="col-sm-2 control-label">PKP :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="pkp" name="pkp" placeholder="PKP">	
	  	  </div>
	  </div>
	  <div class="form-group">
	  	  <label for="notran" class="col-sm-2 control-label">Alamat :</label>
	      <div class="col-sm-3">
	      	<textarea rows="2" id="alamat" name="alamat" class="form-control" placeholder="Alamat"></textarea>
	  	  </div>
	  	  <label for="notran" class="col-sm-2 control-label">Alamat PKP:</label>
	      <div class="col-sm-3">
	      	<textarea rows="2" id="alm_pkp" name="alm_pkp" class="form-control" placeholder="Alamat PKP"></textarea>
	  	  </div>
	  	  
	  </div>
	  <div class="form-group">
	  	  <label for="notran" class="col-sm-2 control-label">Kota :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="kota" name="kota" placeholder="Kota">	
	  	  </div>
	  	  <label for="notran" class="col-sm-2 control-label">Nama PKP :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="nm_pkp" name="nm_pkp" placeholder="Nama PKP">	
	  	  </div>
	  </div>
	  <div class="form-group">
	  	  <label for="notran" class="col-sm-2 control-label">Telepon :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="telepon" name="telepon" placeholder="Telepon">	
	  	  </div>
	  	  <label for="notran" class="col-sm-2 control-label">No PKP :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="no_pkp" name="no_pkp" placeholder="No PKP">	
	  	  </div>
	  </div>
	  <div class="form-group">
	  	  <label for="notran" class="col-sm-2 control-label">HP :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="hp" name="hp" placeholder="HP">	
	  	  </div>
	  	  <label for="notran" class="col-sm-2 control-label">TGL PKP :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="tgl_pkp" name="tgl_pkp" placeholder="DD-MM-YYYY" data-format="DD-MM-YYYY">	
	  	  </div>
	  </div>
	  <div class="form-group">
	  	  <label for="notran" class="col-sm-2 control-label">FAX :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="fax" name="fax" placeholder="FAX">	
	  	  </div>
	  	  <label for="notran" class="col-sm-2 control-label">NPWP :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="npwp" name="npwp" placeholder="NPWP">	
	  	  </div>
	  </div>
	  <div class="form-group">
	  	  <label for="notran" class="col-sm-2 control-label">Kontak :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="kontak" name="kontak" placeholder="Kontak">	
	  	  </div>
	  	  <label for="notran" class="col-sm-2 control-label">No Seri 1 :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="no_seri1" name="no_seri1" placeholder="No Seri 1">	
	  	  </div>
	  </div>
	  <div class="form-group">
	  	  <label for="notran" class="col-sm-2 control-label">Bank :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="bank" name="bank" placeholder="Bank">	
	  	  </div>
	  	  <label for="notran" class="col-sm-2 control-label">No Seri 2 :</label>
	  	  <div class="col-sm-3">
	      	<input type="text" class="form-control" id="no_seri2" name="no_seri2" placeholder="No Seri 2">	
	  	  </div>
	  </div>
	  <div class="form-group">
	  	  <label for="notran" class="col-sm-2 control-label">Alamat Bank :</label>
	      <div class="col-sm-3">
	      	<textarea rows="2" id="alm_bank" name="alm_bank" class="form-control" placeholder="Alamat Bank"></textarea>
	  	  </div>
	  	  <label for="salesman" class="col-sm-2 control-label">Salesman :</label>
		  <div class="col-sm-3">
				<select id='salesman_id' name='salesman_id' class="form-control">
					<option value="" selected>-- Pilih Salesman --</option>
				     <?php
				     	$sql = "SELECT a.KARYAWAN_ID,a.NAMA FROM mst_karyawan a
								WHERE a.BAGIAN='d34dd070-fdcd-11e3-9909-d63a9e6b40fc'";
				        $result=$statement->query($sql);
				        while($row=$statement->fetch_array($result)){
				     ?>
				     <option value="<?php print $row[0]; ?>"><?php print $row[1]; ?></option>
				     <?php
				        }
				     ?>
				</select>
		  </div>
	  </div>
	  <div class="form-group">
	  	  <label for="notran" class="col-sm-2 control-label">Rekening :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="rekening" name="rekening" placeholder="Rekening">	
	  	  </div>
	  	  
	      
	  </div>
	  <div class="form-group">
	  	  <label for="notran" class="col-sm-2 control-label">Atas Nama Bank :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="an_bank" name="an_bank" placeholder="Atas Nama Bank">	
	  	  </div>
	  	  
	  </div>
	  <div class="form-group">
	  	  <div class="col-sm-8">
	    	<div id="loading"></div>
			<div id="peringatan"></div>
			</div>
			<div class="box-footer">
			   	<div class="col-sm-offset-2 col-sm-10">
			      <button type="button" class="btn btn-primary" onclick="javascript:simpanData()"><i class="fa fa-save"></i> Simpan</button>
			      <button type="button" class="btn btn-default" onclick=javascript:refresh();><i class="fa fa-refresh"></i> Refresh</button>
				</div>
	  </div>
	  </div>
	  
	</form>
	</div>
</div>