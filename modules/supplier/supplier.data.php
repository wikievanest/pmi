<?php 
include 'module.php';
$sql = "select a.SUPPLIER_ID, 
			a.KODE, 
			a.NAMA, 
			a.ALAMAT, 
			a.KOTA, 
			a.TELEPON, 
			a.HP, 
			a.FAX, 
			a.KONTAK, 
			a.BANK, 
			a.ALM_BANK, 
			a.REKENING, 
			a.AN_BANK, 
			a.NM_PRODUK, 
			a.PKP, 
			a.NM_PKP, 
			a.ALM_PKP, 
			a.NO_PKP, 
			a.TGL_PKP, 
			a.NPWP, 
			a.NO_SERI1, 
			a.NO_SERI2, 
			a.SALESMAN_ID,
			b.NAMA
		from mst_supplier a
		LEFT OUTER JOIN mst_karyawan b
		ON b.KARYAWAN_ID=a.SALESMAN_ID
		";
$result = $statement->query($sql);
$no = 1;
while($row = $statement->fetch_array($result)) {
	$orders[]= array(
			'No' => $no,
			'kode' => $row[1],
			'nama' => $row[2],
			'alamat' => $row[3],
			'telepon' => $row[4],
			'salesman' => $row[23],
			'Aksi' => "<button class=\"btn btn-sm btn-outline btn-warning\" onClick=\"javascript:editData('$row[0]','$row[1]','$row[2]','$row[3]','$row[4]','$row[5]','$row[6]','$row[7]','$row[8]','$row[9]','$row[10]','$row[11]','$row[12]','$row[13]','$row[14]','$row[15]','$row[16]','$row[17]','$row[18]','$row[19]','$row[20]','$row[21]','$row[22]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Edit Data $row[1] \"><i class=\"fa fa-edit\"></i></button>
			<button class=\"btn btn-sm btn-outline btn-primary\" onClick=\"javascript:deleteData('$row[0]','$row[18]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Hapus Data $row[2] \"><i class=\"fa fa-trash-o\"></i></button>"

	);
	$no++;
}
$data= json_encode($orders);
print '{'.'"aaData":'.json_encode($orders).'}';

?>
