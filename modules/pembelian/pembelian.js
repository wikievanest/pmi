
function refresh() {
	document.location.reload();
}


function kosongkan(){
	  var barang_id = document.getElementById("barang_id");
	  barang_id.value='';
	  var satuan_id = document.getElementById("satuan_id");
	  satuan_id.value='';
	  var jumlah = document.getElementById("jumlah");
	  jumlah.value='';
	  var harga = document.getElementById("harga");
	  harga.value='';
	  var diskon = document.getElementById("diskon");
	  diskon.value='';
	  var subtotal = document.getElementById("subtotal");
	  subtotal.value='';
	  var expired = document.getElementById("expired");
	  expired.value='';
};
	
function noTran() {
	$.ajax({
		type: "POST",
		url: "modules/pembelian/pembelian.data.php",
		success: function(resp){	
			$('#no_tran').val(resp);		
			$('#no_po').focus();	
			},	
	});	
}

function cariBarang() {
	var barang_id = $("#barang_id").val();
		$.ajax({
			type: "POST",
			url: "modules/pembelian/pembelian.barang.php",
			data: "action=caribarang"+"&barang_id="+barang_id,
			success: function(resp){
				var obj = jQuery.parseJSON(resp);
				$('#harga').val(obj.harga_beli);
				$('#satuan_id').val(obj.satuan_id);
				$('#jumlah').focus();
			},
		});
}
function cariSupplier() {
	var supplier_id = $("#supplier_id").val();
		$.ajax({
			type: "POST",
			url: "modules/pembelian/pembelian.barang.php",
			data: "action=carisupplier"+"&supplier_id="+supplier_id,
			success: function(resp){
				var obj = jQuery.parseJSON(resp);
				$('#nama_supp').val(obj.nama);
				$('#telp_supp').val(obj.telepon);
				$('#expired').focus();
			},
		});
}

function jumlahItem() {
	var jumlah = $("#jumlah").val();
	var harga = $("#harga").val();
	var subtotal = jumlah * harga;
	$("#subtotal").val(subtotal)
	$("#diskon").focus()
}

function hargaItem() {
	var jumlah = $("#jumlah").val();
	var harga = $("#harga").val();
	var subtotal = jumlah * harga;
	$("#subtotal").val(subtotal)
	$("#diskon").focus()
}

function totalDiskon() {
	var jumlah = $("#jumlah").val();
	var harga = $("#harga").val();
	var diskon = $("#diskon").val();;
	var subtotal = jumlah * harga;
	var total = subtotal - diskon;
	$("#subtotal").val(total);
}

function totalOngkos() {
	var ongkos = parseInt($("#ongkos").val());
	var total = parseInt($("#total").val()) + ongkos ;
	$("#total").val(total)
}

function totalPPN() {
	var ppn = parseInt($("#ppn").val());
	var total = parseInt($("#total").val()) - ppn ;
	$("#total").val(total)
}

function saveItem() {
	var no_tran = $("#no_tran").val();
	var supplier_id = $("#supplier_id").val();
	var barang_id = $("#barang_id").val();
	var jumlah = $("#jumlah").val();
	var harga = $("#harga").val();
	var diskon = $("#diskon").val();
	var expired = $("#expired").val();
	var subtotal = $("#subtotal").val();
	if(no_tran == "" || barang_id =="" || jumlah =="" || harga =="" || subtotal =="" || supplier_id =="" || expired ==""){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#menu_id').focus();
	} else {
		document.getElementById("loading").innerHTML="<img src='assets/img/ajax-loader.gif' height='32' width='32'/> loading ...";
			$.ajax({
				type: "POST",
				url: "modules/pembelian/pembelian.item.action.php",
				data: "barang_id="+barang_id+"&jumlah="+jumlah+"&harga="+harga+"&subtotal="+subtotal+"&no_tran="+no_tran+"&supplier_id="+supplier_id+"&diskon="+diskon+"&expired="+expired,
				success: function(resp){
					document.getElementById("loading").innerHTML="";
					addItem();
					totalItem();
					kosongkan();
				},
			});
	}
}

function addItem() {
	var no_tran = $("#no_tran").val();
	document.getElementById("loading").innerHTML="<img src='assets/img/ajax-loader.gif' height='32' width='32'/> update keranjang ...";
		$.ajax({
			type: "POST",
			url: "modules/pembelian/pembelian.item.php",
			data: "no_tran="+no_tran,
			success: function(resp){
				document.getElementById("loading").innerHTML="";
				$("#gridItem").html(resp);				
			},
		});
}

function deleteItem(a,b,c,d,e) {
	$.ajax({
		type: "POST",
		url: "modules/pembelian/pembelian.item.action.php",
		data: "action=delete"+"&pembelian_item_id="+a+"&jumlah="+b+"&harga="+c+"&diskon="+d+"&subtotal="+e,
		success: function(resp){
			addItem();
			totalItem();
		},
	});
}

function totalItem() {
	var no_tran = $("#no_tran").val();
		$.ajax({
			type: "POST",
			url: "modules/pembelian/pembelian.item.total.php",
			data: "action=totalItem&no_tran="+no_tran,
			success: function(resp){
				$("#totalBayar").html(resp);
			},
		});
}

function simpanData(){
	var pembelian_id = $('#pembelian_id').val();
	var no_tran = $('#no_tran').val();
	var no_po = $('#no_po').val();
	var subtotalx = $('#subtotalx').val();
	var kemasan_id = $('#kemasan_id').val();
	var total = $('#total').val();
	var status = $('#status_bayar').val();
	if(kemasan_id == "") {
		alert('Kemasan Masih Kosong!');
		$('#kemasan_id').focus();
	} else if(status == "") {
		alert('Status Bayar Masih Kosong !');
		$('#status').focus();
	} else {
		$.ajax({
			type: "POST",
			url: "modules/pembelian/pembelian.action.php",
			data: {"pembelian_id":pembelian_id,"no_tran":no_tran,"no_po":no_po,"subtotalx":subtotalx,"total":total,"kemasan_id":kemasan_id,"status":status},
			success: function(resp){	
					refresh();
				},	
		});
	}
					
};