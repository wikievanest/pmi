<script>
  $(function() {
	  $("#supplier_id").chosen();
      $("#barang_id").chosen();
      $('#expired').datetimepicker({
          pickTime: false,
        });
      noTran();
  });
</script>
<?php 
include 'module.php';
?>
<script src="modules/pembelian/pembelian.js"></script>
<div class="col-xs-12">
<div id="container">
	<div class="box box-primary">
	<div class="box-header">
		<h2 class="box-title" id="judul">Transaksi Pembelian</h2>
		<div style="text-align: right; margin-top: 10px; margin-right: 10px">
			<button class="btn btn-primary" onclick="javascript:refresh()"><i class="fa fa-plus"></i> Baru</button>
		</div>
	</div>
	<div class="box-body table-responsive">
		<form class="form-horizontal" method="POST" action="#" enctype="multipart/form-data">
	  <div class="form-group">
	  	<input type="hidden" class="form-control" id="pembelian_id" name="pembelian_id">
	    <label for="notran" class="col-sm-2 control-label">No. Transaksi :</label>
	    <div class="col-sm-3">
	    	<input type="text" class="form-control" id="no_tran" name="no_tran"> 
	    </div>
	    <label for="notran" class="col-sm-2 control-label">Barang :</label>
	      <div class="col-sm-3">
	      <select id='barang_id' name='barang_id' class="form-control" onchange="javascript:cariBarang()">
		  	<option value="" selected>-- Pilih --</option>
			<?php
				$sql = "SELECT a.BARANG_ID,a.KODE,a.NAMA_BARANG from mst_barang a";
				$result=$statement->query($sql);
				while($row=$statement->fetch_array($result)){
			?>
			<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?> - <?php print $row[2]; ?></option>
			<?php
				}
			?>
		  </select> 
	  	  </div>
	  </div>
	  <div class="form-group">
	  	  <label for="notran" class="col-sm-2 control-label">No. PO :</label>
	      <div class="col-sm-3">
	      	<input type="text" class="form-control" id="no_po" name="no_po" placeholder="No PO"> 
	      </div>
	      <label for="notran" class="col-sm-2 control-label">Satuan :</label>
	      <div class="col-sm-3">
	      	<select id='satuan_id' name='satuan_id' class="form-control" autofocus>
		  	<option value="" selected>-- Pilih --</option>
			<?php
				$sql = "SELECT a.SATUAN_ID,a.SATUAN from mst_satuan a";
				$result=$statement->query($sql);
				while($row=$statement->fetch_array($result)){
			?>
			<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?></option>
			<?php
				}
			?>
		  </select>  
	      </div>
	  </div>
	  <div class="form-group">
	    <label for="notran" class="col-sm-2 control-label">Supplier :</label>
	    <div class="col-sm-3">
	      <select id='supplier_id' name='supplier_id' class="form-control" onchange="javascript:cariSupplier()">
		  	<option value="" selected>-- Pilih --</option>
			<?php
				$sql = "SELECT a.SUPPLIER_ID,a.KODE,a.NAMA from mst_supplier a";
				$result=$statement->query($sql);
				while($row=$statement->fetch_array($result)){
			?>
			<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?> - <?php print $row[2]; ?></option>
			<?php
				}
			?>
		  </select> 
		 </div>
		 <label for="notran" class="col-sm-2 control-label">Qty :</label>
	    <div class="col-sm-3">
	      	<input type="number" style="text-align: right" class="form-control" id="jumlah" name="jumlah" onchange="javascript:jumlahItem()"> 
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="notran" class="col-sm-2 control-label">Nama Supplier :</label>
	    <div class="col-sm-3">
	      	<input type="text" class="form-control" id="nama_supp" name="nama_supp" readonly> 
	    </div>
	    <label for="notran" class="col-sm-2 control-label">Harga :</label>
	      <div class="col-sm-3">
	      	<input type="number" style="text-align: right" class="form-control" id="harga" name="harga" onchange="javascript:hargaItem()"> 
	      </div>
	  </div>
	  <div class="form-group">
	    <label for="notran" class="col-sm-2 control-label">Telepon :</label>
	    <div class="col-sm-3">
	      	<input type="text" class="form-control" id="telp_supp" name="telp_supp" readonly> 
	    </div>
	    <label for="notran" class="col-sm-2 control-label">Diskon :</label>
	    <div class="col-sm-3">
	      	<input type="number" style="text-align: right" class="form-control" id="diskon" name="diskon" onchange="javascript:totalDiskon()"> 
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="notran" class="col-sm-2 control-label">Tanggal Expired :</label>
	    <div class="col-sm-3">
	      	<input type="text" class="form-control" id="expired" name="expired" placeholder="DD-MM-YYYY" data-format="DD-MM-YYYY"> 
	    </div>
	    <label for="notran" class="col-sm-2 control-label">Subtotal :</label>
	    <div class="col-sm-3">
	      	<input type="number" style="text-align: right" class="form-control" id="subtotal" name="subtotal"> 
	    </div>
	  </div>
	  <div class="form-group">
	      <div class="col-sm-7">
	      	<div class="col-sm-8">
			  <div id="loading"></div>
			  <div id="peringatan"></div>
		  </div>
	      </div>
	      <div class="col-sm-5">
	      	<button type="button" class="btn btn-primary" onclick="javascript:saveItem()"><i class="fa fa-save"></i> Tambah</button>
	      </div>
	  </div>
	</form>
	</div>
</div>
</div>
<div id="gridItem"></div>
</div>