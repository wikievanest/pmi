<?php
use statements\statement;
include 'module.php';
$statement = new statement ();

?>
<script>
  $(function() {
       $('#tgl_npwp').datetimepicker({
          pickTime: false,
        });
       $("#salesman_id").chosen();
  });
</script>
<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title" id="title">Tambah Data pelanggan</h3>
	</div>
	<div class="box-body">
	<form class="form-horizontal" method="POST" enctype="multipart/form-data">
		<div class=row>
	     	<!-- left column -->
	        <div class="col-md-6">
		        <!-- general form elements -->
		        <div class="box box-primary">
			        <div class="box-header">
			        	<h3 class="box-title">Info Pelanggan</h3>
			        </div><!-- /.box-header --><!-- form start -->
				      <br>
					  <div class="form-group">
					    <label for="perusahaan" class="col-sm-3 control-label">Kode :</label>
					    <div class="col-sm-9">
					      <input type="hidden" class="form-control" id="pelanggan_id" name="pelanggan_id">
					      <input type="text" class="form-control" id="kode" name="kode" placeholder="Kode Pelanggan" autofocus> 
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Telepon" class="col-sm-3 control-label">Nama pelanggan :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Pelanggan">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Alamat" class="col-sm-3 control-label">Alamat :</label>
					    <div class="col-sm-9">
					      <textarea rows="2" id="alamat" name="alamat" class="form-control" placeholder="Alamat"></textarea>
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Telepon" class="col-sm-3 control-label">Telepon :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="telepon" name="telepon" placeholder="Telepon">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Bagian" class="col-sm-3 control-label">Fax :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="fax" name="fax" placeholder="Fax">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Bagian" class="col-sm-3 control-label">Kota :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="kota" name="kota" placeholder="Kota">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Bagian" class="col-sm-3 control-label">Propinsi :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="propinsi" name="propinsi" placeholder="Propinsi">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Email" class="col-sm-3 control-label">Email :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="email" name="email" placeholder="Email">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="salesman" class="col-sm-3 control-label">Salesman :</label>
					    <div class="col-sm-9">
					       <select id='salesman_id' name='salesman_id' class="form-control">
				              <option value="" selected>-- Pilih Salesman --</option>
				              <?php
				          		$sql = "SELECT a.KARYAWAN_ID,a.NAMA FROM mst_karyawan a
										WHERE a.BAGIAN='d34dd070-fdcd-11e3-9909-d63a9e6b40fc'";
				                $result=$statement->query($sql);
				          		while($row=$statement->fetch_array($result)){
				        	  		?>
				          		<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?></option>
				        		<?php
				          		}
				              ?>
				               </select>
					    </div>
					  </div>
					  <div class="box-header">
			        	<h3 class="box-title">Info NPWP</h3>
			        </div><!-- /.box-header --><!-- form start -->
				      <br>
					  <div class="form-group">
					    <label for="perusahaan" class="col-sm-3 control-label">NPWP :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="npwp" name="npwp" placeholder="Nomor NPWP"> 
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Telepon" class="col-sm-3 control-label">Nama NPWP :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="nama_npwp" name="nama_npwp" placeholder="Nama NPWP">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Alamat" class="col-sm-3 control-label">Alamat :</label>
					    <div class="col-sm-9">
					      <textarea rows="2" id="alamat_npwp" name="alamat_npwp" class="form-control" placeholder="Alamat NPWP"></textarea>
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Telepon" class="col-sm-3 control-label">Tanggal :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="tgl_npwp" name="tgl_npwp" placeholder="DD-MM-YYYY" data-format="DD-MM-YYYY">
					    </div>
					  </div>
				</div>
	    	</div>
	    	<!-- left column -->
	        <div class="col-md-6">
		        <!-- general form elements -->
		        <div class="box box-primary">
			        <div class="box-header">
			        	<h3 class="box-title">Kontak Person</h3>
			        </div><!-- /.box-header --><!-- form start -->
				      <br>
					  <div class="form-group">
					    <label for="Telepon" class="col-sm-3 control-label">Nama :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="nama_cp" name="nama_cp" placeholder="Nama Kontak Person">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Telepon" class="col-sm-3 control-label">Jabatan :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="jabatan_cp" name="jabatan_cp" placeholder="Jabatan">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Alamat" class="col-sm-3 control-label">Alamat :</label>
					    <div class="col-sm-9">
					      <textarea rows="2" id="alamat_cp" name="alamat_cp" class="form-control" placeholder="Alamat"></textarea>
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Telepon" class="col-sm-3 control-label">Agama :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="agama_cp" name="agama_cp" placeholder="Agama">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Bagian" class="col-sm-3 control-label">Telepon :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="telepon_cp" name="telepon_cp" placeholder="Telepon">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Bagian" class="col-sm-3 control-label">HP :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="hp_cp" name="hp_cp" placeholder="Handphone">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Bagian" class="col-sm-3 control-label">Kota :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="kota_cp" name="kota_cp" placeholder="Kota">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Bagian" class="col-sm-3 control-label">Propinsi :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="propinsi_cp" name="propinsi_cp" placeholder="Propinsi">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Email" class="col-sm-3 control-label">Email :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="email_cp" name="email_cp" placeholder="Email">
					    </div>
					  </div>
					  <div class="box-header">
			        	<h3 class="box-title">Penanggung Jawab</h3>
			        </div><!-- /.box-header --><!-- form start -->
				      <br>
					  <div class="form-group">
					    <label for="Telepon" class="col-sm-3 control-label">Nama :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="nama_pj" name="nama_pj" placeholder="Nama">
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Alamat" class="col-sm-3 control-label">Alamat :</label>
					    <div class="col-sm-9">
					      <textarea rows="2" id="alamat_pj" name="alamat_pj" class="form-control" placeholder="Alamat"></textarea>
					    </div>
					  </div>
					  <div class="form-group">
					    <label for="Telepon" class="col-sm-3 control-label">Telepon :</label>
					    <div class="col-sm-9">
					      <input type="text" class="form-control" id="telepon_pj" name="telepon_pj" placeholder="Telepon">
					    </div>
					  </div>
				</div>
	    	</div>
	    	<div class="col-sm-8">
	    	<div id="loading"></div>
			<div id="peringatan"></div>
			</div>
			<div class="box-footer">
			   	<div class="col-sm-offset-2 col-sm-10">
			      <button type="button" class="btn btn-primary" onclick="javascript:simpanData()"><i class="fa fa-save"></i> Simpan</button>
			      <button type="button" class="btn btn-default" onclick=javascript:refresh();><i class="fa fa-refresh"></i> Refresh</button>
				</div>
			</div>
	    </div>
	</form>
	</div>
</div>
<!-- /.box -->