<?php 
include 'module.php';
$sql = "select a.PELANGGAN_ID, 
			a.KODE, 
			a.NAMA, 
			a.ALAMAT, 
			a.TELEPON, 
			a.FAX, 
			a.KOTA, 
			a.PROPINSI, 
			a.EMAIL, 
			a.NPWP, 
			a.NAMA_NPWP, 
			a.ALAMAT_NPWP, 
			a.TGL_NPWP, 
			a.NAMA_CP, 
			a.JABATAN_CP, 
			a.ALAMAT_CP, 
			a.AGAMA_CP, 
			a.TELEPON_CP, 
			a.HP_CP, 
			a.KOTA_CP, 
			a.PROPINSI_CP, 
			a.EMAIL_CP, 
			a.NAMA_PJ, 
			a.ALAMAT_PJ, 
			a.TELEPON_PJ, 
			a.SALESMAN_ID,
			b.NAMA
			
		from mst_pelanggan a
		LEFT OUTER JOIN mst_karyawan b
		ON b.KARYAWAN_ID=a.SALESMAN_ID
		";
$result = $statement->query($sql);
$no = 1;
while($row = $statement->fetch_array($result)) {
	$orders[]= array(
			'No' => $no,
			'kode' => $row[1],
			'nama' => $row[2],
			'alamat' => $row[3],
			'telepon' => $row[4],
			'salesman' => $row[26],
			'Aksi' => "<button class=\"btn btn-sm btn-outline btn-warning\" onClick=\"javascript:editData('$row[0]','$row[1]','$row[2]','$row[3]','$row[4]','$row[5]','$row[6]','$row[7]','$row[8]','$row[9]','$row[10]','$row[11]','$row[12]','$row[13]','$row[14]','$row[15]','$row[16]','$row[17]','$row[18]','$row[19]','$row[20]','$row[21]','$row[22]','$row[23]','$row[24]','$row[25]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Edit Data $row[1] \"><i class=\"fa fa-edit\"></i></button>
			<button class=\"btn btn-sm btn-outline btn-primary\" onClick=\"javascript:deleteData('$row[0]','$row[12]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Hapus Data $row[2] \"><i class=\"fa fa-trash-o\"></i></button>"

	);
	$no++;
}
$data= json_encode($orders);
print '{'.'"aaData":'.json_encode($orders).'}';

?>
