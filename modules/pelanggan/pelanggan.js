function refresh() {
	document.location.reload();
}

function kosongkan(){
	  var pelanggan_id = document.getElementById("pelanggan_id");
	  pelanggan_id.value='';
	  var kode = document.getElementById("kode");
	  kode.value='';
	  var nama = document.getElementById("nama");
	  nama.value='';
	  var alamat = document.getElementById("alamat");
	  alamat.value='';
	  var telepon = document.getElementById("telepon");
	  telepon.value='';
	  var fax = document.getElementById("fax");
	  fax.value='';
	  var kota = document.getElementById("kota");
	  kota.value='';
	  var propinsi = document.getElementById("propinsi");
	  propinsi.value='';
	  var email = document.getElementById("email");
	  email.value='';
	  var npwp = document.getElementById("npwp");
	  npwp.value='';
	  var nama_npwp = document.getElementById("nama_npwp");
	  nama_npwp.value='';
	  var alamat_npwp = document.getElementById("alamat_npwp");
	  alamat_npwp.value='';
	  var tgl_npwp = document.getElementById("tgl_npwp");
	  tgl_npwp.value='';
	  var nama_cp = document.getElementById("nama_cp");
	  nama_cp.value='';
	  var jabatan_cp = document.getElementById("jabatan_cp");
	  jabatan_cp.value='';
	  var alamat_cp = document.getElementById("alamat_cp");
	  alamat_cp.value='';
	  var agama_cp = document.getElementById("agama_cp");
	  agama_cp.value='';
	  var telepon_cp = document.getElementById("telepon_cp");
	  telepon_cp.value='';
	  var hp_cp = document.getElementById("hp_cp");
	  hp_cp.value='';
	  var kota_cp = document.getElementById("kota_cp");
	  kota_cp.value='';
	  var propinsi_cp = document.getElementById("propinsi_cp");
	  propinsi_cp.value='';
	  var email_cp = document.getElementById("email_cp");
	  email_cp.value='';
	  var nama_pj = document.getElementById("nama_pj");
	  nama_pj.value='';
	  var alamat_pj = document.getElementById("alamat_pj");
	  alamat_pj.value='';
	  var telepon_pj = document.getElementById("telepon_pj");
	  telepon_pj.value='';
	  var salesman_id = document.getElementById("salesman_id");
	  salesman_id.value='';
	};

function MsgBox(res){
	if(res=="i"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil disimpan.</div>";
	}else if(res=="d"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil dihapus.</div>";
	}else if(res=="u"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil diupdate.</div>";
	}else if(res=="g"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data gagal diproses.</div>";
	}	
}
	
function tambahData(){
		$.ajax({
				type: "POST",
				url: "modules/pelanggan/pelanggan.form.php",
				success: function(resp){
				document.getElementById("container").innerHTML="";
				$("#container").html(resp);
				},
			});
};


function simpanData(){
	var pelanggan_id = $('#pelanggan_id').val();
	var kode = $('#kode').val();
	var nama = $('#nama').val();
	var alamat = $('#alamat').val();
	var telepon = $('#telepon').val();
	var fax = $('#fax').val();
	var kota = $('#kota').val();
	var propinsi = $('#propinsi').val();
	var email = $('#email').val();
	var npwp = $('#npwp').val();
	var nama_npwp = $('#nama_npwp').val();
	var alamat_npwp = $('#alamat_npwp').val();
	var tgl_npwp = $('#tgl_npwp').val();
	var nama_cp = $('#nama_cp').val();
	var jabatan_cp = $('#jabatan_cp').val();
	var alamat_cp = $('#alamat_cp').val();
	var agama_cp = $('#agama_cp').val();
	var telepon_cp = $('#telepon_cp').val();
	var hp_cp = $('#hp_cp').val();
	var kota_cp = $('#kota_cp').val();
	var propinsi_cp = $('#propinsi_cp').val();
	var email_cp = $('#email_cp').val();
	var nama_pj = $('#nama_pj').val();
	var alamat_pj = $('#alamat_pj').val();
	var telepon_pj = $('#telepon_pj').val();
	var salesman_id = $('#salesman_id').val();
	
	if(pelanggan_id =="") {
		if(nama == "" || salesman_id ==""){
			document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
			$('#kode').focus();
		} else {
			$.ajax({
				type: "POST",
				url: "modules/pelanggan/pelanggan.action.php",
				data: {"pelanggan_id":pelanggan_id,"kode":kode,"nama":nama,"alamat":alamat,"telepon":telepon,"fax":fax,"kota":kota,"propinsi":propinsi,"email":email,"npwp":npwp,"nama_npwp":nama_npwp,"alamat_npwp":alamat_npwp,"tgl_npwp":tgl_npwp,"nama_cp":nama_cp,"jabatan_cp":jabatan_cp,"alamat_cp":alamat_cp,"agama_cp":agama_cp,"telepon_cp":telepon_cp,"hp_cp":hp_cp,"kota_cp":kota_cp,"propinsi_cp":propinsi_cp,"email_cp":email_cp,"nama_pj":nama_pj,"alamat_pj":alamat_pj,"telepon_pj":telepon_pj,"salesman_id":salesman_id},
				success: function(resp){	
					MsgBox(resp);				
					refresh();						
					},	
			});	
		}
	} else {
		updateData(pelanggan_id);
	}
					
};

function editData(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z) {
	$.ajax({
		type: "POST",
		url: "modules/pelanggan/pelanggan.form.php",
		success: function(resp){
			document.getElementById("container").innerHTML="";
			$("#container").html(resp);
			document.getElementById("title").innerHTML="";
			document.getElementById("title").innerHTML="Edit Data pelanggan";
			var pelanggan_id = document.getElementById("pelanggan_id");
			  pelanggan_id.value=a;
			  var kode = document.getElementById("kode");
			  kode.value=b;
			  var nama = document.getElementById("nama");
			  nama.value=c;
			  var alamat = document.getElementById("alamat");
			  alamat.value=d;
			  var telepon = document.getElementById("telepon");
			  telepon.value=e;
			  var fax = document.getElementById("fax");
			  fax.value=f;
			  var kota = document.getElementById("kota");
			  kota.value=g;
			  var propinsi = document.getElementById("propinsi");
			  propinsi.value=h;
			  var email = document.getElementById("email");
			  email.value=i;
			  var npwp = document.getElementById("npwp");
			  npwp.value=j;
			  var nama_npwp = document.getElementById("nama_npwp");
			  nama_npwp.value=k;
			  var alamat_npwp = document.getElementById("alamat_npwp");
			  alamat_npwp.value=l;
			  var tgl_npwp = document.getElementById("tgl_npwp");
			  tgl_npwp.value=m;
			  var nama_cp = document.getElementById("nama_cp");
			  nama_cp.value=n;
			  var jabatan_cp = document.getElementById("jabatan_cp");
			  jabatan_cp.value=o;
			  var alamat_cp = document.getElementById("alamat_cp");
			  alamat_cp.value=p;
			  var agama_cp = document.getElementById("agama_cp");
			  agama_cp.value=q;
			  var telepon_cp = document.getElementById("telepon_cp");
			  telepon_cp.value=r;
			  var hp_cp = document.getElementById("hp_cp");
			  hp_cp.value=s;
			  var kota_cp = document.getElementById("kota_cp");
			  kota_cp.value=t;
			  var propinsi_cp = document.getElementById("propinsi_cp");
			  propinsi_cp.value=u;
			  var email_cp = document.getElementById("email_cp");
			  email_cp.value=v;
			  var nama_pj = document.getElementById("nama_pj");
			  nama_pj.value=w;
			  var alamat_pj = document.getElementById("alamat_pj");
			  alamat_pj.value=x;
			  var telepon_pj = document.getElementById("telepon_pj");
			  telepon_pj.value=y;
			  var salesman_id = document.getElementById("salesman_id");
			  salesman_id.value=z;
		},
	});	
}

function updateData(pelanggan_id){
	var pelanggan_id = $('#pelanggan_id').val();
	var kode = $('#kode').val();
	var nama = $('#nama').val();
	var alamat = $('#alamat').val();
	var telepon = $('#telepon').val();
	var fax = $('#fax').val();
	var kota = $('#kota').val();
	var propinsi = $('#propinsi').val();
	var email = $('#email').val();
	var npwp = $('#npwp').val();
	var nama_npwp = $('#nama_npwp').val();
	var alamat_npwp = $('#alamat_npwp').val();
	var tgl_npwp = $('#tgl_npwp').val();
	var nama_cp = $('#nama_cp').val();
	var jabatan_cp = $('#jabatan_cp').val();
	var alamat_cp = $('#alamat_cp').val();
	var agama_cp = $('#agama_cp').val();
	var telepon_cp = $('#telepon_cp').val();
	var hp_cp = $('#hp_cp').val();
	var kota_cp = $('#kota_cp').val();
	var propinsi_cp = $('#propinsi_cp').val();
	var email_cp = $('#email_cp').val();
	var nama_pj = $('#nama_pj').val();
	var alamat_pj = $('#alamat_pj').val();
	var telepon_pj = $('#telepon_pj').val();
	var salesman_id = $('#salesman_id').val();
	if((nama =="") || (salesman_id =="")){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#nama').focus();
	} else {
		$.ajax({
			type: "POST",
			url: "modules/pelanggan/pelanggan.action.php",
			data: {"pelanggan_id":pelanggan_id,"kode":kode,"nama":nama,"alamat":alamat,"telepon":telepon,"fax":fax,"kota":kota,"propinsi":propinsi,"email":email,"npwp":npwp,"nama_npwp":nama_npwp,"alamat_npwp":alamat_npwp,"tgl_npwp":tgl_npwp,"nama_cp":nama_cp,"jabatan_cp":jabatan_cp,"alamat_cp":alamat_cp,"agama_cp":agama_cp,"telepon_cp":telepon_cp,"hp_cp":hp_cp,"kota_cp":kota_cp,"propinsi_cp":propinsi_cp,"email_cp":email_cp,"nama_pj":nama_pj,"alamat_pj":alamat_pj,"telepon_pj":telepon_pj,"salesman_id":salesman_id},
			success: function(resp){	
				MsgBox(resp);				
				refresh();						
				},	
		});	
	}				
};

function deleteData(pelanggan_id,tgl_npwp) {
	var r = confirm("Yakin data akan di hapus?");
	if(r==true) {
		$.ajax({
		  type: "POST",
		  url: "modules/pelanggan/pelanggan.action.php",
		  data: "action=delete"+"&pelanggan_id="+pelanggan_id+"&tgl_npwp="+tgl_npwp,
		  success: function(resp){			
			  refresh();
		  },
		});
	};
}
