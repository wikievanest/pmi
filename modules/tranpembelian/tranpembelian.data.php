<?php 
include 'module.php';
$sql = "select 	a.PEMBELIAN_ID, 
			a.NO_TRAN, 
			a.NO_PO, 
			a.TOTAL_BAYAR, 
			DATE_FORMAT(a.CREATED,'%d-%m-%Y %h:%m:%s'),
			c.NAMA,
			a.TOTAL_DISKON
		from tran_pembelian a
		inner join mst_user b
		on b.USER_ID=a.USER_ID
		inner join mst_karyawan c
		on c.KARYAWAN_ID=b.PERSON_ID
		ORDER by a.CREATED desc
		";
$result = $statement->query($sql);
$no = 1;
while($row = $statement->fetch_array($result)) {
	$orders[]= array(
			'No' => $no,
			'notran' => $row[1],
			'nopo' => $row[2],
			'total_bayar' => "Rp. ".number_format($row[3],0),
			'total_diskon' => "Rp. ".number_format($row[6],0),
			'tgl_buat' => $row[4],
			'nama' => $row[5],
			'Aksi' => "<button class=\"btn btn-sm btn-outline btn-primary\" onClick=\"javascript:detailData('$row[1]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Detail Data $row[2] \"><i class=\"fa fa-list\"></i> Detail</button>"

	);
	$no++;
}
$data= json_encode($orders);
print '{'.'"aaData":'.json_encode($orders).'}';

?>
