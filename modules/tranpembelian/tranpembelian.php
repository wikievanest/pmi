<?php 
include 'module.php';
?>
<script src="modules/tranpembelian/tranpembelian.js"></script>
<div class="col-xs-12">
<div id="container">
	<div class="box box-primary">
	<div class="box-header">
		<h2 class="box-title" id="judul">Data Transaksi Pembelian</h2>
		<div style="text-align: right; margin-top: 10px; margin-right: 10px">
			<a href="?v=05ae5721e0d84adb7476df10dc76c597a994e97460c2b12854c33b9372ba35590b09f7928d80ea5376ceb10f8d37b4d1"><button class="btn btn-primary" onclick="javascript:tambahData()"><i class="fa fa-plus"></i> New</button></a>
		</div>
	</div>
	<div class="box-body table-responsive">
		<?php include 'tranpembelian.grid.php'; ?>
	</div>
</div>
</div>
</div>
<div class="modal fade" id="frmDetail" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" style="width:800px">
    <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="myModalLabel">Detail Pembelian</h4>
        </div>
      <div class="modal-body">
        
          <div id="detailPembelian"></div>

      </div>
      <div class="modal-footer">
      <div style='float: left'>

      </div>
              <button type="button" class="btn btn-primary" onclick="javascript:tutupForm();"><i class="fa fa-sign-out"></i> Tutup</button>
      </div>
    </div>
  </div>
</div>
