<script>
    $(document).ready(function() {
		var data = "modules/tranpembelian/tranpembelian.data.php";
        var oTable = $('#tabletranpembelian').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": true,
				"bFilter": true,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": true,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "notran","sWidth": "10%" },
					{ "mData": "nopo","sWidth": "10%" },
					{ "mData": "total_diskon","sWidth": "8%" },
					{ "mData": "total_bayar","sWidth": "8%" },
					{ "mData": "tgl_buat","sWidth": "10%" },
					{ "mData": "nama","sWidth": "10%" },
					{ "mData": "Aksi","sWidth": "5%" }
				]
		  });
        $('#tabletranpembelian').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tabletranpembelian" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>No. Tran</th>
			<th>No. PO</th>
			<th>Diskon</th>
			<th>Total</th>
			<th>Tanggal</th>
			<th>User</th>
			<th>Detail</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>