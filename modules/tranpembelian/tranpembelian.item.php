<?php
include 'module.php';
$no_tran = $_POST['no_tran'];
?>
		<table class="table table-hover table-bordered">
			<tbody>
			<tr>
				<th style="text-align:center">No.</th>
		    	<th style="text-align:center">Kode</th>
		        <th>Nama Barang</th>
		        <th style="text-align:center">Qty</th>
            	<th style="text-align:center">Harga</th>
            	<th style="text-align:center">Diskon</th>
		        <th style="text-align:center">Subtotal</th>
		        <th style="text-align:center">Expired</th>
		    </tr>
		    <?php 
		    $sql = "SELECT a.pembelian_item_id,
				    	b.kode,
				    	b.nama_barang,
				    	a.jml,
				    	a.harga,
				    	a.subtotal,
				    	a.diskon,
				    	DATE_FORMAT(a.expired,'%d-%m-%Y')
				    FROM tran_pembelian_item a
				    INNER JOIN mst_barang b
				    ON b.BARANG_ID=a.BARANG_ID
				    WHERE a.NO_TRAN='$no_tran'";
		    $result = $statement->query($sql);
		    $i=1;
		    while ($row=$statement->fetch_array($result)) {
		    	echo "<tr>";
		    	echo "<td style=\"text-align:center\">$i</td>";
				echo "<td style=\"text-align:center\">$row[1]</td>";
				echo "<td style=\"text-align:left\">$row[2]</td>";
				echo "<td style=\"text-align:center\">$row[3]</td>";
				echo "<td style=\"text-align:right\">".number_format($row[4], 0, '.', ',')."</td>";
				echo "<td style=\"text-align:right\">".number_format($row[6], 0, '.', ',')."</td>";
				echo "<td style=\"text-align:right\">".number_format($row[5], 0, '.', ',')."</td>";
				echo "<td style=\"text-align:center\">$row[7]</td>";
				echo "</tr>";
				$i++;
		    }
		    
		    ?>
		    </tbody>
		</table>
		<table class="table">
		<?php		
		$sql = "select sum(a.subtotal),sum(a.diskon) from tran_pembelian_item a
		WHERE a.no_tran='$no_tran'";
		$result = $statement->query($sql);
		$row = $statement->fetch_array($result);
		?>
			<tr>
				<th>Total Diskon:</th>
					<td style="text-align: right"><b>Rp. <?php echo number_format($row[1], 2, '.', ',');?></b></td>
				</tr>
				<tr>
				<th>Total Bayar:</th>
					<td style="text-align: right"><b>Rp. <?php echo number_format($row[0], 2, '.', ',');?></b></td>
				</tr>
		</table>