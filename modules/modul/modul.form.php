<?php 
use statements\statement;
include 'module.php';
$statement = new statement();

?>

<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title" id="title">Tambah Data Modul</h3>
	</div>
	<br>
	<form class="form-horizontal">
	  <div class="form-group">
	    <label for="modul" class="col-sm-2 control-label">Pilih Menu :</label>
	    <div class="col-sm-6">
	       <select id='menu_id' name='menu_id' class="form-control" autofocus>
              <option value="" selected>-- Pilih Main Menu --</option>
              <?php
          		$sql = "SELECT a.MENU_ID,a.NAMA_MENU FROM mst_menu a order by a.MENU_ORDER";
                $result=$statement->query($sql);
          		while($row=$statement->fetch_array($result)){
        	  		?>
          		<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?></option>
        		<?php
          		}
              ?>
               </select>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="modul" class="col-sm-2 control-label">Nama Modul :</label>
	    <div class="col-sm-6">
	      <input type="hidden" class="form-control" id="modul_id">
	      <input type="text" class="form-control" id="nama_modul" placeholder="Nama Modul">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Deskripsi" class="col-sm-2 control-label">Deskripsi :</label>
	    <div class="col-sm-6">
	      <textarea rows="2" id="deskripsi" class="form-control" placeholder="Deskripsi"></textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="modul" class="col-sm-2 control-label">Modul Path :</label>
	    <div class="col-sm-6">
	      <input type="text" class="form-control" id="modul_path" placeholder="path">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Deskripsi" class="col-sm-2 control-label">Modul Order :</label>
	    <div class="col-sm-1">
	      <input type="number" class="form-control" id="modul_order">
	    </div>
	  </div>
	  <div class="col-sm-8">
		  <div id="loading"></div>
		  <div id="peringatan"></div>
	  </div>
	  <br>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="button" class="btn btn-primary" onclick="javascript:simpanData()"><i class="fa fa-save"></i> Simpan</button>
	      <button type="button" class="btn btn-default" onclick=javascript:refresh();><i class="fa fa-refresh"></i> Refresh</button>
	    </div>
	  </div>
		<div class="box-footer">
		</div>
	</form>
</div>
<!-- /.box -->
</div>
<!-- /.box -->