<?php 
include 'module.php';
$sql = "SELECT a.MODUL_ID,
			a.MENU_ID,
			b.NAMA_MENU,
			a.NAMA_MODUL,
			a.MODUL_ORDER,
			a.MODUL_PATH,
			a.DESKRIPSI
		FROM mst_modul a
		INNER JOIN mst_menu b
		ON a.MENU_ID=b.MENU_ID";
$result = $statement->query($sql);
$no = 1;
while($row = $statement->fetch_array($result)) {
	$orders[]= array(
			'No' => $no,
			'nama_menu' => $row[2],
			'nama_modul' => $row[3],
			'modul_order' => $row[4],
			'modul_path' => $row[5],
			'deskripsi' => $row[6],
			'Aksi' => "<button class=\"btn btn-sm btn-outline btn-warning\" onClick=\"javascript:editData('$row[0]','$row[1]','$row[3]','$row[6]','$row[5]','$row[4]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Edit Data $row[2] \"><i class=\"fa fa-edit\"></i></button>
			<button class=\"btn btn-sm btn-outline btn-primary\" onClick=\"javascript:deleteData('$row[0]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Hapus Data $row[2] \"><i class=\"fa fa-trash-o\"></i></button>"

	);
	$no++;
}
$data= json_encode($orders);
print '{'.'"aaData":'.json_encode($orders).'}';

?>
