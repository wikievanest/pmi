<script>
    $(document).ready(function() {
		var data = "modules/modul/modul.data.php";
        var oTable = $('#tablemodul').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": true,
				"bFilter": true,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": true,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "nama_modul","sWidth": "20%" },
					{ "mData": "nama_menu","sWidth": "10%" },
					{ "mData": "modul_order","sWidth": "3%" },
					{ "mData": "modul_path","sWidth": "10%" },
					{ "mData": "deskripsi","sWidth": "20%" },
					{ "mData": "Aksi","sWidth": "7%" }
				]
		  });
        $('#tablemodul').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tablemodul" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Modul</th>
			<th>Nama Menu</th>
			<th>Urutan</th>
			<th>Modul Path</th>
			<th>Deksripsi</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>