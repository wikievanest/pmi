function refresh() {
	document.location.reload();
}

function kosongkan(){
	  var modul_id = document.getElementById("modul_id");
	  modul_id.value='';
	  var menu_id = document.getElementById("menu_id");
	  menu_id.value='';
	  var nama_modul = document.getElementById("nama_modul");
	  nama_modul.value='';
	  var deskripsi = document.getElementById("deskripsi");
	  deskripsi.value='';
	  var modul_path = document.getElementById("modul_path");
	  modul_path.value='';
	  var modul_order = document.getElementById("modul_order");
	  modul_order.value='';
	};

function MsgBox(res){
	if(res=="i"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil disimpan.</div>";
	}else if(res=="d"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil dihapus.</div>";
	}else if(res=="u"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil diupdate.</div>";
	}else if(res=="g"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data gagal diproses.</div>";
	}	
}
	
function tambahData(){
		$.ajax({
				type: "POST",
				url: "modules/modul/modul.form.php",
				success: function(resp){
				document.getElementById("container").innerHTML="";
				$("#container").html(resp);
				},
			});
};

function simpanData(){
	var modul_id = $('#modul_id').val();
	var menu_id = $('#menu_id').val();
	var nama_modul = $('#nama_modul').val();
	var deskripsi = $('#deskripsi').val();
	var modul_path = $('#modul_path').val();
	var modul_order = $('#modul_order').val();
	
	if(modul_id =="") {
		if(menu_id == "" || nama_modul ==""){
			document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
			$('#menu_id').focus();
		} else {
			$.ajax({
				type: "POST",
				url: "modules/modul/modul.action.php",
				data: {"modul_id":modul_id,"menu_id":menu_id,"nama_modul":nama_modul,"deskripsi":deskripsi,"modul_path":modul_path,"modul_order":modul_order},
				success: function(resp){	
					MsgBox(resp);				
					refresh();						
					},	
			});	
		}
	} else {
		updateData(modul_id);
	}
					
};

function editData(a,b,c,d,e,f) {
	$.ajax({
		type: "POST",
		url: "modules/modul/modul.form.php",
		success: function(resp){
			document.getElementById("container").innerHTML="";
			$("#container").html(resp);
			document.getElementById("title").innerHTML="";
			document.getElementById("title").innerHTML="Edit Data Modul";
			var modul_id = document.getElementById("modul_id");
			modul_id.value=a;
			var menu_id = document.getElementById("menu_id");
			menu_id.value=b;
			var nama_modul = document.getElementById("nama_modul");
			nama_modul.value=c;
			var deskripsi = document.getElementById("deskripsi");
			deskripsi.value=d;
			var modul_path = document.getElementById("modul_path");
			modul_path.value=e;
			var modul_order = document.getElementById("modul_order");
			modul_order.value=f;
		},
	});	
}

function updateData(modul_id){
	var menu_id = $('#menu_id').val();
	var nama_modul = $('#nama_modul').val();
	var deskripsi = $('#deskripsi').val();
	var modul_path = $('#modul_path').val();
	var modul_order = $('#modul_order').val();
	
	if(nama_modul ==""){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#nama_modul').focus();
	} else {
		$.ajax({
			type: "POST",
			url: "modules/modul/modul.action.php",
			data: {"modul_id":modul_id,"menu_id":menu_id,"nama_modul":nama_modul,"deskripsi":deskripsi,"modul_path":modul_path,"modul_order":modul_order},
			success: function(resp){	
				MsgBox(resp);				
				refresh();						
				},	
		});	
	}				
};

function deleteData(modul_id) {
	var r = confirm("Yakin data akan di hapus?");
	if(r==true) {
		$.ajax({
		  type: "POST",
		  url: "modules/modul/modul.action.php",
		  data: "action=delete"+"&modul_id="+modul_id,
		  success: function(resp){			
			  refresh();
		  },
		});
	};
}
