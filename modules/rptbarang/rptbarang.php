<?php 
include 'module.php';
?>
<script src="modules/rptbarang/rptbarang.js"></script>
<div class="col-xs-12">
<div id="container">
	<div class="box box-primary">
	<div class="box-header">
		<h2 class="box-title" id="judul">Laporan Data Barang</h2>
		<div style="text-align: right; margin-top: 10px; margin-right: 10px">
			<button class="btn btn-primary"><i class="fa fa-print"></i> Cetak</button>
			<button class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
		</div>
	</div>
	<div class="box-body table-responsive">
		<?php include 'rptbarang.grid.php'; ?>
	</div>
</div>
</div>
</div>