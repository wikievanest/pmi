<?php 
include 'module.php';
$sql = "SELECT a.PERUSAHAAN_ID,
			a.NAMA_PERUSAHAAN,
			a.ALAMAT,
			a.KOTA,
			a.TELEPON,
			a.FAX,
			a.EMAIL,
			a.NPWP,
			a.IJIN,
			a.NAMA_NPWP,
			a.TGL_NPWP,
			a.KOP_PAJAK,
			a.ALM_PAJAK,
			a.BANK,
			a.NOREK
		FROM mst_perusahaan a
		";
$result = $statement->query($sql);
$no = 1;
while($row = $statement->fetch_array($result)) {
	$orders[]= array(
			'No' => $no,
			'nama' => $row[1],
			'alamat' => $row[2],
			'kota' => $row[3],
			'telepon' => $row[4],
			'fax' => $row[5],
			'email' => $row[6],
			'npwp' => $row[7],
			'Aksi' => "<button class=\"btn btn-sm btn-outline btn-warning\" onClick=\"javascript:editData('$row[0]','$row[1]','$row[2]','$row[3]','$row[4]','$row[5]','$row[6]','$row[7]','$row[8]','$row[9]','$row[10]','$row[11]','$row[12]','$row[13]','$row[14]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Edit Data $row[1] \"><i class=\"fa fa-edit\"></i></button>
			<button class=\"btn btn-sm btn-outline btn-primary\" onClick=\"javascript:deleteData('$row[0]','$row[9]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Hapus Data $row[0] \"><i class=\"fa fa-trash-o\"></i></button>"

	);
	$no++;
}
$data= json_encode($orders);
print '{'.'"aaData":'.json_encode($orders).'}';

?>
