function refresh() {
	document.location.reload();
}

function kosongkan(){
	  var perusahaan_id = document.getElementById("perusahaan_id");
	  perusahaan_id.value='';
	  var nama = document.getElementById("nama");
	  nama.value='';
	  var alamat = document.getElementById("alamat");
	  alamat.value='';
	  var kota = document.getElementById("kota");
	  kota.value='';
	  var telepon = document.getElementById("telepon");
	  telepon.value='';
	  var email = document.getElementById("email");
	  email.value='';
	  var fax = document.getElementById("fax");
	  fax.value='';
	  var npwp = document.getElementById("npwp");
	  npwp.value='';
	};

function MsgBox(res){
	if(res=="i"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil disimpan.</div>";
	}else if(res=="d"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil dihapus.</div>";
	}else if(res=="u"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil diupdate.</div>";
	}else if(res=="g"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data gagal diproses.</div>";
	}	
}
	
function tambahData(){
		$.ajax({
				type: "POST",
				url: "modules/perusahaan/perusahaan.form.php",
				success: function(resp){
				document.getElementById("container").innerHTML="";
				$("#container").html(resp);
				},
			});
};

function editData(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o) {
	$.ajax({
		type: "POST",
		url: "modules/perusahaan/perusahaan.form.php",
		success: function(resp){
			document.getElementById("container").innerHTML="";
			$("#container").html(resp);
			document.getElementById("title").innerHTML="";
			document.getElementById("title").innerHTML="Edit Data Perusahaan";
			var perusahaan_id = document.getElementById("perusahaan_id");
			perusahaan_id.value=a;
			var nama = document.getElementById("nama");
			nama.value=b;
			var alamat = document.getElementById("alamat");
			alamat.value=c;
			var kota = document.getElementById("kota");
			kota.value=d;
			var telepon = document.getElementById("telepon");
			telepon.value=e;
			var email = document.getElementById("email");
			email.value=g;
			var fax = document.getElementById("fax");
			fax.value=f;
			var npwp = document.getElementById("npwp");
			npwp.value=h;
			var ijin_npwp = document.getElementById("ijin_npwp");
			ijin_npwp.value=i;
			var nama_npwp = document.getElementById("nama_npwp");
			nama_npwp.value=j;
			var tgl_npwp = document.getElementById("tgl_npwp");
			tgl_npwp.value=k;
			var kop_pajak = document.getElementById("kop_pajak");
			kop_pajak.value=l;
			var alamat_bank = document.getElementById("alamat_bank");
			alamat_bank.value=m;
			var bank = document.getElementById("bank");
			bank.value=n;
			var norek = document.getElementById("norek");
			norek.value=o;
		},
	});	
}

function deleteData(perusahaan_id,tgl_npwp) {
	var r = confirm("Yakin data akan di hapus?");
	if(r==true) {
		$.ajax({
		  type: "POST",
		  url: "modules/perusahaan/perusahaan.action.php",
		  data: "action=delete"+"&perusahaan_id="+perusahaan_id+"&tgl_npwp="+tgl_npwp,
		  success: function(resp){			
			  refresh();
		  },
		});
	};
}
