<?php 
use statements\statement;
include 'module.php';
$statement = new statement();

?>
<script>
  $(function() {
       $('#tgl_npwp').datetimepicker({
          pickTime: false,
        });
  });
</script>
<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title" id="title">Tambah Data Perusahaan</h3>
	</div>
	<br>
	<form class="form-horizontal" method="POST" action="modules/perusahaan/perusahaan.action.php" enctype="multipart/form-data">
	  <div class="form-group">
	    <label for="perusahaan" class="col-sm-2 control-label">Nama Perusahaan :</label>
	    <div class="col-sm-5">
	      <input type="hidden" class="form-control" id="perusahaan_id" name="perusahaan_id">
	      <input type="hidden" class="form-control" id="action" name="action">
	      <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Perusahaan" autofocus> 
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Alamat" class="col-sm-2 control-label">Alamat :</label>
	    <div class="col-sm-5">
	      <textarea rows="2" id="alamat" name="alamat" class="form-control" placeholder="Alamat"></textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Telepon" class="col-sm-2 control-label">Kota :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="kota" name="kota" placeholder="Kota">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Telepon" class="col-sm-2 control-label">Telepon :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="telepon" name="telepon" placeholder="Telepon">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Bagian" class="col-sm-2 control-label">Fax :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="fax" name="fax" placeholder="Fax">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Email" class="col-sm-2 control-label">Email :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="email" name="email" placeholder="Email">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="NPWP" class="col-sm-2 control-label">NPWP :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="npwp" name="npwp" placeholder="Nomor NPWP">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="IJIN" class="col-sm-2 control-label">Ijin NPWP :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="ijin_npwp" name="ijin_npwp" placeholder="Ijin NPWP">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="NPWP" class="col-sm-2 control-label">Nama NPWP :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="nama_npwp" name="nama_npwp" placeholder="Nama NPWP">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="NPWP" class="col-sm-2 control-label">Tanggal NPWP :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="tgl_npwp" name="tgl_npwp"  data-format="YYYY-MM-DD"  placeholder="Tanggal NPWP">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="NPWP" class="col-sm-2 control-label">KOP PAJAK :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="kop_pajak" name="kop_pajak" placeholder="KOP PAJAK">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="NPWP" class="col-sm-2 control-label">Alamat Pajak :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="alamat_bank" name="alamat_bank" placeholder="Alamat Bank">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="NPWP" class="col-sm-2 control-label">Nama Bank :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="bank" name="bank" placeholder="Nama Bank">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="NPWP" class="col-sm-2 control-label">No. Rekening :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="norek" name="norek" placeholder="Nomor Rekening">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Foto" class="col-sm-2 control-label">Logo :</label>
	    <div class="col-sm-3">
	      <input type="file" class="form-control" id="logo" name="logo">
	    </div>
	  </div>
	  <div class="col-sm-8">
		  <div id="loading"></div>
		  <div id="peringatan"></div>
	  </div>
	  <br>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
	      <button type="button" class="btn btn-default" onclick=javascript:refresh();><i class="fa fa-refresh"></i> Refresh</button>
	    </div>
	  </div>
		<div class="box-footer">
		</div>
	</form>
</div>
<!-- /.box -->
</div>
<!-- /.box -->