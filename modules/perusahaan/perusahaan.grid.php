<script>
    $(document).ready(function() {
		var data = "modules/perusahaan/perusahaan.data.php";
        var oTable = $('#tableperusahaan').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": false,
				"bFilter": false,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": false,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "nama","sWidth": "15%" },
					{ "mData": "alamat","sWidth": "20%" },
					{ "mData": "kota","sWidth": "20%" },
					{ "mData": "telepon","sWidth": "15%" },
					{ "mData": "fax","sWidth": "15%" },
					{ "mData": "email","sWidth": "10%" },
					{ "mData": "npwp","sWidth": "10%" },
					{ "mData": "Aksi","sWidth": "10%" }
				]
		  });
        $('#tableperusahaan').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tableperusahaan" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Perusahaan</th>
			<th>Alamat</th>
			<th>Kota</th>
			<th>Telepon</th>
			<th>Fax</th>
			<th>Email</th>
			<th>NPWP</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>