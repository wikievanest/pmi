<?php 
use statements\statement;
include 'module.php';
$statement = new statement();

?>
<script>
  $(function() {
       $('#tgl_npwp').datetimepicker({
          pickTime: false,
        });
       $("#grup_ids").chosen();
  });
</script>
<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title" id="title">Tambah Data Barang</h3>
	</div>
	<br>
	<form class="form-horizontal" method="POST" action="#" enctype="multipart/form-data">
	  <div class="form-group">
	    <label for="karyawan" class="col-sm-2 control-label">Grup :</label>
	    <div class="col-sm-4">
	       <input type="hidden" class="form-control" id="barang_id" name="barang_id">
	       <select id='grup_id' name='grup_id' class="form-control" onchange="javascript:newCode()">
              <option value="" selected>-- Pilih Grup --</option>
              <?php
          		$sql = "SELECT a.GRUP_ID,a.KODE,a.NAMA FROM mst_barang_grup a";
                $result=$statement->query($sql);
          		while($row=$statement->fetch_array($result)){
        	  		?>
          		<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?> - <?php print $row[2]; ?></option>
        		<?php
          		}
              ?>
               </select>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="barang" class="col-sm-2 control-label">Kode Barang :</label>
	    <div class="col-sm-2">
	      <input type="text" class="form-control" id="kode" name="kode" placeholder="Kode"> 
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Telepon" class="col-sm-2 control-label">Nama Barang :</label>
	    <div class="col-sm-4">
	      <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Barang">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="karyawan" class="col-sm-2 control-label">Satuan :</label>
	    <div class="col-sm-2">
	       <select id='satuan_id' name='satuan_id' class="form-control">
              <option value="" selected>-- Pilih Satuan --</option>
              <?php
          		$sql = "SELECT a.SATUAN_ID,a.SATUAN FROM mst_satuan a";
                $result=$statement->query($sql);
          		while($row=$statement->fetch_array($result)){
        	  		?>
          		<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?></option>
        		<?php
          		}
              ?>
               </select>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="NPWP" class="col-sm-2 control-label">Harga Beli :</label>
	    <div class="col-sm-2">
	      <input type="number" style="text-align: right" class="form-control" id="hrg_beli" name="hrg_beli" placeholder="Harga Beli">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="NPWP" class="col-sm-2 control-label">Harga Jual :</label>
	    <div class="col-sm-2">
	      <input type="number" style="text-align: right" class="form-control" id="hrg_jual" name="hrg_jual" placeholder="Harga Jual">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="NPWP" class="col-sm-2 control-label">Stok Awal :</label>
	    <div class="col-sm-2">
	      <input type="number" style="text-align: right" class="form-control" id="stok" name="stok" placeholder="Stok Tersedia">
	    </div>
	  </div>
	  <div class="col-sm-8">
		  <div id="loading"></div>
		  <div id="peringatan"></div>
	  </div>
	  <br>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="button" class="btn btn-primary" onclick="javascript:simpanData()"><i class="fa fa-save"></i> Simpan</button>
	      <button type="button" class="btn btn-default" onclick=javascript:refresh();><i class="fa fa-refresh"></i> Refresh</button>
	    </div>
	  </div>
		<div class="box-footer">
		</div>
	</form>
</div>
<!-- /.box -->
</div>
<!-- /.box -->