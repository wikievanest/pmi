<?php 
include 'module.php';
$sql = "SELECT a.barang_id,
			a.kode,
			a.nama_barang,
			a.grup_id,
			b.NAMA as nama_grup,
			a.satuan_id,
			c.SATUAN,
			a.harga_beli,
			a.harga_jual,
			a.stok,
			a.created
		FROM mst_barang a
		LEFT OUTER JOIN mst_barang_grup b
		ON b.GRUP_ID=a.GRUP_ID
		LEFT OUTER JOIN mst_satuan c
		ON c.SATUAN_ID=a.SATUAN_ID
		order by a.created desc
		";
$result = $statement->query($sql);
$no = 1;

while($row = $statement->fetch_array($result)) {
	$arr = array("'",'"');
	$orders[]= array(
			'No' => $no,
			'kode' => $row[1],
			'nama' => str_replace($arr, '', $row[2]),
			'grup' => $row[4],
			'satuan' => $row[6],
			'hrg_beli' => number_format($row[7], 2, '.', ','),
			'hrg_jual' => number_format($row[8], 2, '.', ','),
			'stok' => $row[9],
			'Aksi' => "<button class=\"btn btn-sm btn-outline btn-warning\" onClick=\"javascript:editData('$row[0]','$row[1]','$row[2]','$row[3]','$row[5]','$row[7]','$row[8]','$row[9]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Edit Data $row[2] \"><i class=\"fa fa-edit\"></i></button>
			<button class=\"btn btn-sm btn-outline btn-primary\" onClick=\"javascript:deleteData('$row[0]','$row[7]','$row[8]','$row[9]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Hapus Data $row[2] \"><i class=\"fa fa-trash-o\"></i></button>"

	);
	$no++;
}
$data= json_encode($orders);
print '{'.'"aaData":'.json_encode($orders).'}';

?>
