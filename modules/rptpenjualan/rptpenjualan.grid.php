<script>
    $(document).ready(function() {
		var data = "modules/rptpenjualan/rptpenjualan.data.php";
        var oTable = $('#tabletranpenjualan').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": true,
				"bFilter": true,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": true,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "notran","sWidth": "10%" },
					{ "mData": "nopo","sWidth": "10%" },
					{ "mData": "total_bayar","sWidth": "8%" },
					{ "mData": "tgl_buat","sWidth": "10%" },
					{ "mData": "status","sWidth": "10%" },
					{ "mData": "tgl_jatuh_tempo","sWidth": "10%" },
					{ "mData": "sales","sWidth": "10%" },
					{ "mData": "Aksi","sWidth": "5%" }
				]
		  });
        $('#tabletranpenjualan').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tabletranpenjualan" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>No. Transaksi</th>
			<th>No. PO</th>
			<th>Total</th>
			<th>Tanggal</th>
			<th>Status</th>
			<th>Jatuh Tempo</th>
			<th>Salesman</th>
			<th>Detail</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>