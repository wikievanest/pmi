<?php
include 'module.php';
$no_tran = $_POST['no_tran'];
?>
<?php 
		    $sql = "SELECT a.NAMA,
		    			a.ALAMAT,
		    			a.TELEPON
		    		FROM mst_pelanggan a 
		    		INNER JOIN tran_penjualan_item b
		    		ON b.PELANGGAN_ID=a.PELANGGAN_ID
				    WHERE b.NO_TRAN='$no_tran'";
		    $result = $statement->query($sql);
		    $row=$statement->fetch_array($result);
?>
<table class="table table-hover table-bordered">
			<tbody>
			<tr>
				<th>Nama Pelanggan :</th>
				<th><?php echo $row['0']; ?></th>
			</tr>
			<tr>
		    	<th>Alamat :</th>
		    	<th><?php echo $row['1']; ?></th>
		    </tr>
		    <tr>
		        <th>Telepon :</th>
		        <th><?php echo $row['2']; ?></th>
		    </tr>
		    </tbody>
		</table>
		
		<table class="table table-hover table-bordered">
			<tbody>
			<tr>
				<th>No.</th>
		    	<th>Kode</th>
		        <th>Nama Barang</th>
		        <th>Qty</th>
            	<th>Harga</th>
		        <th>Subtotal</th>
		    </tr>
		    <?php 
		    $sql = "SELECT a.penjualan_item_id,
				    	b.kode,
				    	b.nama_barang,
				    	a.jml,
				    	a.harga,
				    	a.subtotal
				    FROM tran_penjualan_item a
				    INNER JOIN mst_barang b
				    ON b.BARANG_ID=a.BARANG_ID
				    WHERE a.NO_TRAN='$no_tran'";
		    $result = $statement->query($sql);
		    $i=1;
		    while ($row=$statement->fetch_array($result)) {
		    	echo "<tr>";
		    	echo "<td>$i</td>";
				echo "<td style=\"width: 15%\">$row[1]</td>";
				echo "<td style=\"width: 35%\">$row[2]</td>";
				echo "<td style=\"width: 15%\">$row[3]</td>";
				echo "<td style=\"width: 15%\">".number_format($row[4], 0, '.', ',')."</td>";
				echo "<td style=\"width: 20%\">".number_format($row[5], 0, '.', ',')."</td>";
				echo "</tr>";
				$i++;
		    }
		    
		    ?>
		    </tbody>
		</table>
		<table class="table">
		<?php		
		$sql = "select a.subtotal,
					   a.diskon,
					   a.ongkos,
					   a.PPN,
					   a.TOTAL_BAYAR,
					   CASE a.STATUS_BAYAR
					   	WHEN 0 THEN 'KREDIT'
					   	WHEN 1 THEN 'LUNAS'
					   END as STATUS,
					   a.TGL_JATUH_TEMPO
					   FROM tran_penjualan a
					   WHERE a.no_tran='$no_tran'";
		$result = $statement->query($sql);
		$row = $statement->fetch_array($result);
		?>
			<tr>
				<th>Subtotal:</th>
					<td style="text-align: right"><b>Rp. <?php echo number_format($row[0], 2, '.', ',');?></b></td>
				</tr>
				<tr>
				<th>Diskon:</th>
					<td style="text-align: right"><b>Rp. <?php echo number_format($row[1], 2, '.', ',');?></b></td>
				</tr>
				<tr>
				<th>Ongkos:</th>
					<td style="text-align: right"><b>Rp. <?php echo number_format($row[2], 2, '.', ',');?></b></td>
				</tr>
				<tr>
				<th>PPN:</th>
					<td style="text-align: right"><b>Rp. <?php echo number_format($row[3], 2, '.', ',');?></b></td>
				</tr>
				<tr>
				<th>Total:</th>
					<td style="text-align: right"><b>Rp. <?php echo number_format($row[4], 2, '.', ',');?></b></td>
				</tr>
				<tr>
				<th>STATUS:</th>
					<td style="text-align: right"><b><?php echo $row[5];?></b></td>
				</tr>
				<tr>
				<th>Jatuh Tempo:</th>
					<td style="text-align: right"><b><?php echo $row[6];?></b></td>
				</tr>
		</table>