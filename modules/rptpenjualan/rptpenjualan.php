<?php 
include 'module.php';
?>
<script src="modules/rptpenjualan/rptpenjualan.js"></script>
<div class="col-xs-12">
<div id="container">
	<div class="box box-primary">
	<div class="box-header">
		<h2 class="box-title" id="judul">Laporan Data Transaksi Penjualan</h2>
		<div style="text-align: right; margin-top: 10px; margin-right: 10px">
			<button class="btn btn-primary"><i class="fa fa-print"></i> Cetak</button>
			<button class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
		</div>
	</div>
	<div class="box-body table-responsive">
		<?php include 'rptpenjualan.grid.php'; ?>
	</div>
</div>
</div>
</div>
<div class="modal fade" id="frmDetail" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="myModalLabel">Detail Transaksi Penjualan</h4>
        </div>
      <div class="modal-body">
        
          <div id="detailPembelian"></div>

      </div>
      <div class="modal-footer">
      <div style='float: left'>

      </div>
              <button type="button" class="btn btn-primary" onclick="javascript:tutupForm();"><i class="fa fa-sign-out"></i> Tutup</button>
      </div>
    </div>
  </div>
</div>
