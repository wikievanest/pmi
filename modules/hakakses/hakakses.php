
<?php 
include 'module.php';
?>
<script src="modules/hakakses/hakakses.js"></script>
<div class="col-xs-12">
<div id="container">
	<div class="box box-primary">
	<div class="box-header">
		<h2 class="box-title" id="judul">Setting Hak Akses Pengguna</h2>
	</div>
	<br>
	<form class="form-horizontal">
	  <div class="form-group">
	    <label for="user" class="col-sm-2 control-label">Pilih Grup :</label>
	    <div class="col-sm-5">
	       <select id='grup_id' name='grup_id' class="form-control"  onchange="javascript:tampilHakakses();" autofocus>
              <option value="" selected>-- Pilih Grup --</option>
              <?php
          		$sql = "SELECT a.GRUP_ID,a.NAMA_GRUP FROM mst_grup a";
                $result=$statement->query($sql);
          		while($row=$statement->fetch_array($result)){
        	  		?>
          		<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?></option>
        		<?php
          		}
              ?>
               </select>
	    </div>
	  </div>
	 </form>
	<div class="box-body table-responsive">
		<div id="loading"></div>
		<div id="grid-hakakses">
			
        	<div id="table-grid-full" style="margin-top: 20px;">
            </div>
        </div>
	</div>
</div>
</div>
</div>