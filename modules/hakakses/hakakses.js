function refresh() {
	document.location.reload();
}

function simpanData() {
	document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Success !</b> Data berhasil disimpan.</div>";
  tampilHakakses();
}

function tampilHakakses() {
  var grup_id = $('#grup_id').val();
  document.getElementById("loading").innerHTML="<img src='assets/img/loading.gif' height='16' width='16'/>";
  $.ajax({
    type: "POST",
    url: "modules/hakakses/hakakses.data.akses.php",
    data: {"grup_id":grup_id},
    success: function(resp){
    //alert(resp);
      document.getElementById("loading").innerHTML="";
      document.getElementById("grid-hakakses").innerHTML="";
      $("#grid-hakakses").html(resp);
    }
  });
}

function sahlo(modul_id) {
    var grup_id = $("#grup_id").val();
    if($("#write"+modul_id).prop("checked")) {
      var write = 1;
    } else {
      var write = 0;
    }
    if($("#edit"+modul_id).is(":checked")) {
      var edit = 1;
    } else {
      var edit = 0;
    }
    if($("#delete"+modul_id).is(":checked")) {
      var delet = 1;
    } else {
      var delet = 0;
    }
    if($("#view"+modul_id).is(":checked")) {
      var view = 1;
    } else {
      var view = 0;
    }
    
    $.ajax({
    type: "POST",
    url: "modules/hakakses/hakakses.action.php",
    data: "grup_id=" + grup_id + "&modul_id=" + modul_id + "&write=" + write
          + "&edit=" + edit + "&delet=" + delet + "&view=" + view,
    success: function(resp){
      	//alert(resp);
      }
    });
  
}

function sahloupdate(modul_id) {
    var grup_id = $("#grup_id").val();
    if($("#write"+modul_id).prop("checked")) {
      var write = 1;
    } else {
      var write = 0;
    }
    if($("#edit"+modul_id).is(":checked")) {
      var edit = 1;
    } else {
      var edit = 0;
    }
    if($("#delete"+modul_id).is(":checked")) {
      var delet = 1;
    } else {
      var delet = 0;
    }
    if($("#view"+modul_id).is(":checked")) {
      var view = 1;
    } else {
      var view = 0;
    }
    $.ajax({
    type: "POST",
    url: "modules/hakakses/hakakses.action.php",
    data: "stat=update&grup_id=" + grup_id + "&modul_id=" + modul_id + "&write=" + write
          + "&edit=" + edit + "&delet=" + delet + "&view=" + view,
    success: function(resp){
      	//alert(resp);
      }
    });
}