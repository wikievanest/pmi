<?php 
include 'module.php';
?>
<script src="modules/rptpenjualansales/rptpenjualansales.js"></script>
<div class="col-xs-12">
<div id="container">
	<div class="box box-primary">
	<div class="box-header">
		<h2 class="box-title" id="judul">Laporan Data Transaksi Penjualan</h2>
		<div style="text-align: right; margin-top: 10px; margin-right: 10px">
			<button class="btn btn-primary"><i class="fa fa-print"></i> Cetak</button>
			<button class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
		</div>
	</div>
	<div class="box-body table-responsive">
		<form class="form-horizontal" method="POST" action="#" enctype="multipart/form-data">
		<div class="form-group">
		  <label for="salesman" class="col-sm-2 control-label">Salesman :</label>
			<div class="col-sm-3">
			<select id='salesman_id' name='salesman_id' class="form-control" onchange="javascript:cariData()">
				<option value="" selected>-- Pilih Salesman --</option>
				<?php
					$sql = "SELECT a.KARYAWAN_ID,a.NAMA FROM mst_karyawan a
							WHERE a.BAGIAN='d34dd070-fdcd-11e3-9909-d63a9e6b40fc'";
					$result=$statement->query($sql);
					while($row=$statement->fetch_array($result)){
				?>
				<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?></option>
				<?php
					}
				?>
			</select>
			</div>
		  </div>
		</form>
		<div id="Loading"></div>
		<div id="dataPenjualan"></div>
	</div>
</div>
</div>
</div>
<div class="modal fade" id="frmDetail" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="myModalLabel">Detail Transaksi Penjualan</h4>
        </div>
      <div class="modal-body">
        
          <div id="detailPembelian"></div>

      </div>
      <div class="modal-footer">
      <div style='float: left'>

      </div>
              <button type="button" class="btn btn-primary" onclick="javascript:tutupForm();"><i class="fa fa-sign-out"></i> Tutup</button>
      </div>
    </div>
  </div>
</div>
