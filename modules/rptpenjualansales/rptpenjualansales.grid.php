<?php 
include 'module.php';
$salesman_id = $_POST['salesman_id'];
$sql = "select 	a.PENJUALAN_ID, 
			a.NO_TRAN, 
			a.NO_PO, 
			a.TOTAL_BAYAR, 
			DATE_FORMAT(a.CREATED,'%d-%m-%Y %h:%m:%s'),
			CASE STATUS_BAYAR WHEN 0
				THEN 'KREDIT'
			WHEN 1 
				THEN 'LUNAS'
			END AS STATUS,
			DATE_FORMAT(a.TGL_JATUH_TEMPO,'%d-%m-%Y'),
			c.nama
		from tran_penjualan a
		inner join mst_user b
		on b.USER_ID=a.USER_ID
		inner join mst_karyawan c
		on c.KARYAWAN_ID=b.PERSON_ID
		WHERE a.SALESMAN_ID='$salesman_id'
		ORDER by a.NO_TRAN asc
		";
$result = $statement->query($sql);
$sql1= "select SUM(a.TOTAL_BAYAR) as total
		from tran_penjualan a
		inner join mst_user b
		on b.USER_ID=a.USER_ID
		inner join mst_karyawan c
		on c.KARYAWAN_ID=b.PERSON_ID
		WHERE a.SALESMAN_ID='$salesman_id'";
$result1 = $statement->query($sql1);
$rows = $statement->fetch_array($result1);
?>

<table id="tabletranpenjualan" class="table table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>No. Transaksi</th>
			<th>No. PO</th>
			<th>Total</th>
			<th>Tanggal</th>
			<th>Status</th>
			<th>Jatuh Tempo</th>
			<th>Detail</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no = 1;
		while($row = $statement->fetch_array($result)) {
			echo "<tr>";
			echo "<td>$no</td>";
			echo "<td>$row[1]</td>";
			echo "<td>$row[2]</td>";
			echo "<td>".number_format($row[3], 2, '.', ',')."</td>";
			echo "<td>$row[4]</td>";
			echo "<td>$row[5]</td>";
			echo "<td>$row[6]</td>";
			echo "<td><button class=\"btn btn-sm btn-outline btn-primary\" onClick=\"javascript:detailData('$row[1]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Detail Data $row[2] \"><i class=\"fa fa-list\"></i> Detail</button></td>";
			echo "</tr>";
			$no++;
		}
		?>
		<tr><td colspan="7">Total Penjualan : Rp. <b><?php echo number_format($rows[0], 2, '.', ','); ?></b></td></tr>
	</tbody>

</table>