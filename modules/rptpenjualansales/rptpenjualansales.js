function detailData(a) {
	$.ajax({
		type: "POST",
		url: "modules/rptpenjualansales/rptpenjualansales.item.php",
		data: {"no_tran":a},
		success: function(resp){	
			$('#detailPembelian').html(resp);		
			$('#frmDetail').modal('show');	
			},	
	});	
}

function cariData() {
	var salesman_id = $("#salesman_id").val();
	document.getElementById("Loading").innerHTML="<img src='assets/img/loading.gif' height='50' width='50'/>";
	$.ajax({
		type: "POST",
		url: "modules/rptpenjualansales/rptpenjualansales.grid.php",
		data: {"salesman_id":salesman_id},
		success: function(resp){	
			document.getElementById("Loading").innerHTML="";
			$('#dataPenjualan').html(resp);		
			},	
	});	
}

function tutupForm() {
	$('#frmDetail').modal('hide');	
}
