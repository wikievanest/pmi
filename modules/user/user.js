function refresh() {
	document.location.reload();
}

function kosongkan(){
	  var user_id = document.getElementById("user_id");
	  user_id.value='';
	  var username = document.getElementById("username");
	  username.value='';
	  var password = document.getElementById("password");
	  password.value='';
	  var grup_id = document.getElementById("grup_id");
	  grup_id.value='';
	  var pegawai_id = document.getElementById("pegawai_id");
	  pegawai_id.value='';
	};

function MsgBox(res){
	if(res=="i"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil disimpan.</div>";
	}else if(res=="d"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil dihapus.</div>";
	}else if(res=="u"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil diupdate.</div>";
	}else if(res=="g"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data gagal diproses.</div>";
	}	
}
	
function tambahData(){
		$.ajax({
				type: "POST",
				url: "modules/user/user.form.php",
				success: function(resp){
				document.getElementById("container").innerHTML="";
				$("#container").html(resp);
				},
			});
};

function simpanData(){
	var user_id = $('#user_id').val();
	var username = $('#username').val();
	var password = $('#password').val();
	var grup_id = $('#grup_id').val();
	var pegawai_id = $('#pegawai_id').val();
	
	if(user_id =="") {
		if(pegawai_id == "" || username == "" || password ==""){
			document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
			$('#pegawai_id').focus();
		} else {
			$.ajax({
				type: "POST",
				url: "modules/user/user.action.php",
				data: {"user_id":user_id,"username":username,"password":password,"grup_id":grup_id,"pegawai_id":pegawai_id},
				success: function(resp){	
					MsgBox(resp);				
					refresh();						
					},	
			});	
		}
	} else {
		updateData(user_id);
	}
					
};

function editData(a,b,c,d,e) {
	$.ajax({
		type: "POST",
		url: "modules/user/user.form.php",
		success: function(resp){
			document.getElementById("container").innerHTML="";
			$("#container").html(resp);
			document.getElementById("title").innerHTML="";
			document.getElementById("title").innerHTML="Edit Data User";
			document.getElementById("confirm").innerHTML="<p style=\"color: red\">Tulis Ulang Password !</p>";
			var user_id = document.getElementById("user_id");
			user_id.value=a;
			var username = document.getElementById("username");
			username.value=b;
			var grup_id = document.getElementById("grup_id");
			grup_id.value=d;
			var pegawai_id = document.getElementById("pegawai_id");
			pegawai_id.value=e;
		},
	});	
}

function updateData(user_id){
	var username = $('#username').val();
	var password = $('#password').val();
	var grup_id = $('#grup_id').val();
	var pegawai_id = $('#pegawai_id').val();
	
	if(pegawai_id ==""){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#pegawai_id').focus();
	}else if(username ==""){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#username').focus();
	}else if(password ==""){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#password').focus();
	} else if(grup_id ==""){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#grup_id').focus();
	}
	
	else {
		$.ajax({
			type: "POST",
			url: "modules/user/user.action.php",
			data: {"user_id":user_id,"username":username,"password":password,"grup_id":grup_id,"pegawai_id":pegawai_id},
			success: function(resp){	
				MsgBox(resp);				
				refresh();						
				},	
		});	
	}				
};

function deleteData(user_id) {
	var r = confirm("Yakin data akan di hapus?");
	if(r==true) {
		$.ajax({
		  type: "POST",
		  url: "modules/user/user.action.php",
		  data: "action=delete"+"&user_id="+user_id,
		  success: function(resp){			
			  refresh();
		  },
		});
	};
}
