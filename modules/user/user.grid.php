<script>
    $(document).ready(function() {
		var data = "modules/user/user.data.php";
        var oTable = $('#tableuser').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": true,
				"bFilter": true,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": false,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "nama_lengkap","sWidth": "20%" },
					{ "mData": "username","sWidth": "10%" },
					{ "mData": "nama_grup","sWidth": "10%" },
					{ "mData": "bagian","sWidth": "20%" },
					{ "mData": "Aksi","sWidth": "7%" }
				]
		  });
        $('#tableuser').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tableuser" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Lengkap</th>
			<th>Username</th>
			<th>Grup</th>
			<th>Bagian</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>