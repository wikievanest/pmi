<?php 
include 'module.php';
$sql = "SELECT a.USER_ID,
			a.USERNAME,
			a.PASSWORD,
			a.GRUP_ID,
			b.NAMA_GRUP,
			c.NAMA,
			d.NAMA as BAGIAN,
			c.ALAMAT,
			c.TELEPON,
			c.EMAIL,
			a.PERSON_ID
		FROM mst_user a
		INNER JOIN mst_grup b
		ON a.GRUP_ID=b.GRUP_ID
		INNER JOIN mst_karyawan c
		ON a.person_id=c.karyawan_id
		LEFT OUTER JOIN mst_bagian d
		ON d.BAGIAN_ID=c.BAGIAN";
$result = $statement->query($sql);
$no = 1;
while($row = $statement->fetch_array($result)) {
	$orders[]= array(
			'No' => $no,
			'username' => $row[1],
			'nama_lengkap' => $row[5],
			'nama_grup' => $row[4],
			'bagian' => $row[6],
			'Aksi' => "<button class=\"btn btn-sm btn-outline btn-warning\" onClick=\"javascript:editData('$row[0]','$row[1]','$row[2]','$row[3]','$row[10]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Edit Data $row[1] \"><i class=\"fa fa-edit\"></i></button>
			<button class=\"btn btn-sm btn-outline btn-primary\" onClick=\"javascript:deleteData('$row[0]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Hapus Data $row[1] \"><i class=\"fa fa-trash-o\"></i></button>"

	);
	$no++;
}
$data= json_encode($orders);
print '{'.'"aaData":'.json_encode($orders).'}';

?>
