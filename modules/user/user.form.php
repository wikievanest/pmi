<?php 
use statements\statement;
include 'module.php';
$statement = new statement();

?>
<script>
  $(function() {
	  $("#pegawai_id").chosen();
  });
</script>
<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title" id="title">Tambah Data User</h3>
	</div>
	<br>
	<form class="form-horizontal">
	  <div class="form-group">
	    <label for="karyawan" class="col-sm-2 control-label">Pilih Pegawai :</label>
	    <div class="col-sm-5">
	       <select id='pegawai_id' name='pegawai_id' class="form-control" autofocus>
              <option value="" selected>-- Pilih Pegawai --</option>
              <?php
          		$sql = "SELECT a.KARYAWAN_ID,a.NAMA FROM mst_karyawan a";
                $result=$statement->query($sql);
          		while($row=$statement->fetch_array($result)){
        	  		?>
          		<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?></option>
        		<?php
          		}
              ?>
               </select>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="user" class="col-sm-2 control-label">Pilih Grup :</label>
	    <div class="col-sm-5">
	       <select id='grup_id' name='grup_id' class="form-control">
              <option value="" selected>-- Pilih Grup --</option>
              <?php
          		$sql = "SELECT a.GRUP_ID,a.NAMA_GRUP FROM mst_grup a";
                $result=$statement->query($sql);
          		while($row=$statement->fetch_array($result)){
        	  		?>
          		<option value="<?php print $row[0]; ?>"><?php print $row[1]; ?></option>
        		<?php
          		}
              ?>
               </select>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Username" class="col-sm-2 control-label">Username :</label>
	    <div class="col-sm-5">
	      <input type="hidden" class="form-control" id="user_id" name="username">
	      <input type="text" class="form-control" id="username" name="username" placeholder="Username">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Password" class="col-sm-2 control-label">Password :</label>
	    <div class="col-sm-5">
	      <input type="password" class="form-control" id="password" name="password" placeholder="***">
	      <div id="confirm"></div>
	    </div>
	  </div>
	  <div class="col-sm-8">
		  <div id="loading"></div>
		  <div id="peringatan"></div>
	  </div>
	  <br>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	       <button type="button" class="btn btn-primary" onclick="javascript:simpanData()"><i class="fa fa-save"></i> Simpan</button>
	      <button type="button" class="btn btn-default" onclick=javascript:refresh();><i class="fa fa-refresh"></i> Refresh</button>
	    </div>
	  </div>
		<div class="box-footer">
		</div>
	</form>
</div>
<!-- /.box -->
</div>
<!-- /.box -->