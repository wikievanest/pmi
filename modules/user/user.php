<?php 
include 'module.php';
?>
<script src="modules/user/user.js"></script>
<div class="col-xs-12">
<div id="container">
	<div class="box box-primary">
	<div class="box-header">
		<h2 class="box-title" id="judul">Data User Sistem</h2>
		<div style="text-align: right; margin-top: 10px; margin-right: 10px">
			<button class="btn btn-primary" onclick="javascript:tambahData()"><i class="fa fa-plus"></i> Tambah</button>
			<button class="btn btn-default" onclick=javascript:window.location.reload();><i class="fa fa-refresh"></i> Refresh</button>
		</div>
	</div>
	<div class="box-body table-responsive">
		<?php include 'user.grid.php'; ?>
	</div>
</div>
</div>
</div>