<script>
    $(document).ready(function() {
		var data = "modules/salbarang/salbarang.data.php";
        var oTable = $('#tablebarang').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": true,
				"bFilter": true,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": true,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "kode","sWidth": "8%" },
					{ "mData": "nama","sWidth": "20%" },
					{ "mData": "grup","sWidth": "15%" },
					{ "mData": "satuan","sWidth": "5%" },
					{ "mData": "hrg_jual","sWidth": "10%" },
					{ "mData": "stok","sWidth": "5%" }
				]
		  });
        $('#tablebarang').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tablebarang" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode</th>
			<th>Nama Barang</th>
			<th>Grup</th>
			<th>Satuan</th>
			<th>Harga Jual</th>
			<th>Stok</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>