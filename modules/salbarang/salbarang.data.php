<?php 
include 'module.php';
$sql = "SELECT a.barang_id,
			a.kode,
			a.nama_barang,
			b.NAMA as nama_grup,
			c.SATUAN,
			a.harga_jual,
			a.stok
		FROM mst_barang a
		LEFT OUTER JOIN mst_barang_grup b
		ON b.GRUP_ID=a.GRUP_ID
		LEFT OUTER JOIN mst_satuan c
		ON c.SATUAN_ID=a.SATUAN_ID
		order by a.created desc
		";
$result = $statement->query($sql);
$no = 1;
while($row = $statement->fetch_array($result)) {
	$orders[]= array(
			'No' => $no,
			'kode' => $row[1],
			'nama' => $row[2],
			'grup' => $row[3],
			'satuan' => $row[4],
			'hrg_jual' => number_format($row[5], 2, '.', ','),
			'stok' => $row[6]

	);
	$no++;
}
$data= json_encode($orders);
print '{'.'"aaData":'.json_encode($orders).'}';

?>
