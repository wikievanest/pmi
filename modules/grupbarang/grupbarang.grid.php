<script>
    $(document).ready(function() {
		var data = "modules/grupbarang/grupbarang.data.php";
        var oTable = $('#tablegrupbarang').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": true,
				"bFilter": true,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": true,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "kode","sWidth": "8%" },
					{ "mData": "nama","sWidth": "15%" },
					{ "mData": "input","sWidth": "8%" },
					{ "mData": "Aksi","sWidth": "3%" }
				]
		  });
        $('#tablegrupbarang').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tablegrupbarang" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode</th>
			<th>Nama</th>
			<th>Tanggal Buat</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>