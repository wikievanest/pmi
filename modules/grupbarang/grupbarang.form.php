<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title" id="title">Tambah Grup Barang</h3>
	</div>
	<br>
	<form class="form-horizontal">
	  <div class="form-group">
	    <label for="Grup" class="col-sm-2 control-label">Kode Grup :</label>
	    <div class="col-sm-4">
	      <input type="hidden" class="form-control" id="grup_id">
	      <input type="text" class="form-control" id="kode" placeholder="Kode Grup" autofocus>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Nama" class="col-sm-2 control-label">Nama Grup Barang :</label>
	    <div class="col-sm-4">
	      <input type="text" class="form-control" id="nama" placeholder="Nama Grup Barang">
	    </div>
	  </div>
	  <div class="col-sm-8">
		  <div id="loading"></div>
		  <div id="peringatan"></div>
	  </div>
	  <br>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="button" class="btn btn-primary" onclick="javascript:simpanData()"><i class="fa fa-save"></i> Simpan</button>
	      <button type="button" class="btn btn-default" onclick=javascript:refresh();><i class="fa fa-refresh"></i> Refresh</button>
	    </div>
	  </div>
		<div class="box-footer">
		</div>
	</form>
</div>
<!-- /.box -->
</div>
<!-- /.box -->