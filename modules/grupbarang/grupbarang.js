function refresh() {
	document.location.reload();
}

function kosongkan(){
	  var grup_id = document.getElementById("grup_id");
	  grup_id.value='';
	  var kode = document.getElementById("kode");
	  kode.value='';
	  var nama = document.getElementById("nama");
	  nama.value='';
	};

function MsgBox(res){
	if(res=="i"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil disimpan.</div>";
	}else if(res=="d"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil dihapus.</div>";
	}else if(res=="u"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil diupdate.</div>";
	}else if(res=="g"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data gagal diproses.</div>";
	}	
}
	
function tambahData(){
		$.ajax({
				type: "POST",
				url: "modules/grupbarang/grupbarang.form.php",
				success: function(resp){
				document.getElementById("container").innerHTML="";
				$("#container").html(resp);
				},
			});
};

function simpanData(){
	var grup_id = $('#grup_id').val();
	var kode = $('#kode').val();
	var nama = $('#nama').val();
	
	if(grup_id =="") {
		if(kode ==""){
			document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
			$('#kode').focus();
		} else {
			$.ajax({
				type: "POST",
				url: "modules/grupbarang/grupbarang.action.php",
				data: {"grup_id":grup_id,"kode":kode,"nama":nama},
				success: function(resp){	
					MsgBox(resp);				
					refresh();						
					},	
			});	
		}
	} else {
		updateData(grup_id);
	}
					
};

function editData(a,b,c) {
	$.ajax({
		type: "POST",
		url: "modules/grupbarang/grupbarang.form.php",
		success: function(resp){
			document.getElementById("container").innerHTML="";
			$("#container").html(resp);
			document.getElementById("title").innerHTML="";
			document.getElementById("title").innerHTML="Edit Data Menu";
			var grup_id = document.getElementById("grup_id");
			grup_id.value=a;
			var kode = document.getElementById("kode");
			kode.value=b;
			var nama = document.getElementById("nama");
			nama.value=c;
		},
	});	
}

function updateData(grup_id){
	var kode = $('#kode').val();
	var nama = $('#nama').val();
	
	if(kode ==""){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#kode').focus();
	} else {
		$.ajax({
			type: "POST",
			url: "modules/grupbarang/grupbarang.action.php",
			data: {"grup_id":grup_id,"kode":kode,"nama":nama},
			success: function(resp){	
				MsgBox(resp);				
				refresh();						
				},	
		});	
	}				
};

function deleteData(grup_id) {
	var r = confirm("Yakin data akan di hapus?");
	if(r==true) {
		$.ajax({
		  type: "POST",
		  url: "modules/grupbarang/grupbarang.action.php",
		  data: "action=delete"+"&grup_id="+grup_id,
		  success: function(resp){			
			  refresh();
		  },
		});
	};
}
