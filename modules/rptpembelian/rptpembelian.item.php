<?php
include 'module.php';
$no_tran = $_POST['no_tran'];
?>
		<table class="table table-hover table-bordered">
			<tbody>
			<tr>
				<th>No.</th>
		    	<th>Kode</th>
		        <th>Nama Barang</th>
		        <th>Qty</th>
            	<th>Harga</th>
		        <th>Subtotal</th>
		    </tr>
		    <?php 
		    $sql = "SELECT a.pembelian_item_id,
				    	b.kode,
				    	b.nama_barang,
				    	a.jml,
				    	a.harga,
				    	a.subtotal
				    FROM tran_pembelian_item a
				    INNER JOIN mst_barang b
				    ON b.BARANG_ID=a.BARANG_ID
				    WHERE a.NO_TRAN='$no_tran'";
		    $result = $statement->query($sql);
		    $i=1;
		    while ($row=$statement->fetch_array($result)) {
		    	echo "<tr>";
		    	echo "<td>$i</td>";
				echo "<td style=\"width: 15%\">$row[1]</td>";
				echo "<td style=\"width: 35%\">$row[2]</td>";
				echo "<td style=\"width: 15%\">$row[3]</td>";
				echo "<td style=\"width: 15%\">".number_format($row[4], 0, '.', ',')."</td>";
				echo "<td style=\"width: 20%\">".number_format($row[5], 0, '.', ',')."</td>";
				echo "</tr>";
				$i++;
		    }
		    
		    ?>
		    </tbody>
		</table>
		<table class="table">
		<?php		
		$sql = "select sum(a.subtotal) from tran_pembelian_item a
		WHERE a.no_tran='$no_tran'";
		$result = $statement->query($sql);
		$row = $statement->fetch_array($result);
		?>
			<tr>
				<th>Total:</th>
					<td style="text-align: right"><b>Rp. <?php echo number_format($row[0], 2, '.', ',');?></b></td>
				</tr>
		</table>