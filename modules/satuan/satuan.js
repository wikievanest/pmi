function refresh() {
	document.location.reload();
}

function kosongkan(){
	  var satuan_id = document.getElementById("satuan_id");
	  satuan_id.value='';
	  var nama = document.getElementById("nama");
	  nama.value='';
	  var deskripsi = document.getElementById("deskripsi");
	  deskripsi.value='';
	};

function MsgBox(res){
	if(res=="i"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil disimpan.</div>";
	}else if(res=="d"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil dihapus.</div>";
	}else if(res=="u"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil diupdate.</div>";
	}else if(res=="g"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data gagal diproses.</div>";
	}	
}
	
function tambahData(){
		$.ajax({
				type: "POST",
				url: "modules/satuan/satuan.form.php",
				success: function(resp){
				document.getElementById("container").innerHTML="";
				$("#container").html(resp);
				},
			});
};

function simpanData(){
	var satuan_id = $('#satuan_id').val();
	var nama = $('#nama').val();
	var deskripsi = $('#deskripsi').val();
	if(satuan_id =="") {
		if(nama ==""){
			document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
			$('#nama').focus();
		} else {
			$.ajax({
				type: "POST",
				url: "modules/satuan/satuan.action.php",
				data: {"satuan_id":satuan_id,"nama":nama,"deskripsi":deskripsi},
				success: function(resp){	
					MsgBox(resp);				
					refresh();						
					},	
			});	
		}
	} else {
		updateData(satuan_id);
	}
					
};

function editData(a,b,c) {
	$.ajax({
		type: "POST",
		url: "modules/satuan/satuan.form.php",
		success: function(resp){
			document.getElementById("container").innerHTML="";
			$("#container").html(resp);
			document.getElementById("title").innerHTML="";
			document.getElementById("title").innerHTML="Edit Data Satuan";
			var satuan_id = document.getElementById("satuan_id");
			satuan_id.value=a;
			var nama = document.getElementById("nama");
			nama.value=b;
			var deskripsi = document.getElementById("deskripsi");
			deskripsi.value=c;
		},
	});	
}

function updateData(satuan_id){
	var nama = $('#nama').val();
	var deskripsi = $('#deskripsi').val();
	if(nama ==""){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#nama').focus();
	} else {
		$.ajax({
			type: "POST",
			url: "modules/satuan/satuan.action.php",
			data: {"satuan_id":satuan_id,"nama":nama,"deskripsi":deskripsi},
			success: function(resp){	
				MsgBox(resp);				
				refresh();						
				},	
		});	
	}				
};

function deleteData(satuan_id) {
	var r = confirm("Yakin data akan di hapus?");
	if(r==true) {
		$.ajax({
		  type: "POST",
		  url: "modules/satuan/satuan.action.php",
		  data: "action=delete"+"&satuan_id="+satuan_id,
		  success: function(resp){			
			  refresh();
		  },
		});
	};
}
