<script>
    $(document).ready(function() {
		var data = "modules/satuan/satuan.data.php";
        var oTable = $('#tablesatuan').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": false,
				"bFilter": false,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": true,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "nama","sWidth": "20%" },
					{ "mData": "deskripsi","sWidth": "20%" },
					{ "mData": "created","sWidth": "10%" },
					{ "mData": "Aksi","sWidth": "5%" }
				]
		  });
        $('#tablesatuan').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tablesatuan" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Satuan</th>
			<th>Deksripsi</th>
			<th>Input</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>