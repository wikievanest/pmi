<script>
    $(document).ready(function() {
		var data = "modules/bagian/bagian.data.php";
        var oTable = $('#tablebagian').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": false,
				"bFilter": false,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": false,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "kode","sWidth": "10%" },
					{ "mData": "nama","sWidth": "20%" },
					{ "mData": "Aksi","sWidth": "5%" }
				]
		  });
        $('#tablebagian').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tablebagian" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode Bagian</th>
			<th>Nama Bagian</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>