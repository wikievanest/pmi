<?php 
include 'module.php';
?>
<script src="modules/grup/grup.js"></script>
<div class="col-xs-12">
	<div id="loading"></div>
	<div id="overlay"></div>
	<div class="box box-primary" id="container">
	<div class="box-header">
		<h2 class="box-title" id="judul">Data Grup Pengguna</h2>
		<div style="text-align: right; margin-top: 10px; margin-right: 10px">
			<button class="btn btn-primary" onclick="javascript:tambahData()"><i class="fa fa-plus"></i> Tambah Data</button>
			<button class="btn btn-default" onclick=javascript:window.location.reload();><i class="fa fa-refresh"></i> Refresh</button>
		</div>
	</div>
	<div class="box-body table-responsive">
		<?php include 'grup.grid.php'; ?>
	</div>
</div>
</div>