function refresh() {
	document.location.reload();
}

function kosongkan(){
	  var grup_id = document.getElementById("grup_id");
	  grup_id.value='';
	  var nama_grup = document.getElementById("nama_grup");
	  nama_grup.value='';
	  var deskripsi = document.getElementById("deskripsi");
	  deskripsi.value='';
	};

function MsgBox(res){
	if(res=="i"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil disimpan.</div>";
	}else if(res=="d"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil dihapus.</div>";
	}else if(res=="u"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil diupdate.</div>";
	}else if(res=="g"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data gagal diproses.</div>";
	}	
}
	
function tambahData(){
		$.ajax({
				type: "POST",
				url: "modules/grup/grup.form.php",
				success: function(resp){
				document.getElementById("container").innerHTML="";
				$("#container").html(resp);
				},
			});
};

function simpanData(){
	var grup_id = $('#grup_id').val();
	var nama_grup = $('#nama_grup').val();
	var deskripsi = $('#deskripsi').val();
	
	if(grup_id =="") {
		if(nama_grup ==""){
			document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
			$('#nama_grup').focus();
		} else {
			$.ajax({
				type: "POST",
				url: "modules/grup/grup.action.php",
				data: {"grup_id":grup_id,"nama_grup":nama_grup,"deskripsi":deskripsi,},
				success: function(resp){	
					MsgBox(resp);				
					refresh();						
					},	
			});	
		}
	} else {
		updateData(grup_id);
	}
					
};

function editData(a,b,c) {
	$.ajax({
		type: "POST",
		url: "modules/grup/grup.form.php",
		success: function(resp){
			document.getElementById("container").innerHTML="";
			$("#container").html(resp);
			document.getElementById("title").innerHTML="";
			document.getElementById("title").innerHTML="Edit Data Grup";
			var grup_id = document.getElementById("grup_id");
			grup_id.value=a;
			var nama_grup = document.getElementById("nama_grup");
			nama_grup.value=b;
			var deskripsi = document.getElementById("deskripsi");
			deskripsi.value=c;
		},
	});	
}

function updateData(grup_id){
	var nama_grup = $('#nama_grup').val();
	var deskripsi = $('#deskripsi').val();
	if(nama_grup ==""){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#nama_grup').focus();
	} else {
		$.ajax({
			type: "POST",
			url: "modules/grup/grup.action.php",
			data: {"grup_id":grup_id,"nama_grup":nama_grup,"deskripsi":deskripsi},
			success: function(resp){	
				MsgBox(resp);				
				refresh();						
				},	
		});	
	}				
};

function deleteData(grup_id) {
	var r = confirm("Yakin data akan di hapus?");
	if(r==true) {
		$.ajax({
		  type: "POST",
		  url: "modules/grup/grup.action.php",
		  data: "action=delete"+"&grup_id="+grup_id,
		  success: function(resp){			
			  refresh();
		  },
		});
	};
}
