<script>
    $(document).ready(function() {
		var data = "modules/grup/grup.data.php";
        var oTable = $('#tablegrup').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": false,
				"bFilter": false,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": false,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "nama_grup","sWidth": "30%" },
					{ "mData": "deskripsi","sWidth": "20%" },
					{ "mData": "Aksi","sWidth": "5%" }
				]
		  });
        $('#tablegrup').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tablegrup" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Grup</th>
			<th>Deksripsi</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>