<script>
    $(document).ready(function() {
		var data = "modules/rptkaryawan/rptkaryawan.data.php";
        var oTable = $('#tablekaryawan').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": true,
				"bFilter": true,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": false,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "kode","sWidth": "10%" },
					{ "mData": "nama","sWidth": "15%" },
					{ "mData": "alamat","sWidth": "20%" },
					{ "mData": "telepon","sWidth": "13%" },
					{ "mData": "bagian","sWidth": "18%" },
					{ "mData": "tgl_masuk","sWidth": "12%" }
				]
		  });
        $('#tablekaryawan').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tablekaryawan" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Kode</th>
			<th>Nama Karyawan</th>
			<th>Alamat</th>
			<th>Telepon</th>
			<th>Bagian</th>
			<th>Tanggal Masuk</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>