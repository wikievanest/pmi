<?php 
include 'module.php';
$sql = "SELECT a.KARYAWAN_ID,
			a.KODE,
			a.NAMA,
			a.ALAMAT,
			a.TELEPON,
			a.EMAIL,
			b.NAMA as BAGIAN,
			DATE_FORMAT(a.TMK,'%d-%m-%Y')
		FROM mst_karyawan a
		LEFT OUTER JOIN mst_bagian b
		ON b.bagian_id=a.bagian
		";
$result = $statement->query($sql);
$no = 1;
while($row = $statement->fetch_array($result)) {
	$orders[]= array(
			'No' => $no,
			'kode' => $row[1],
			'nama' => $row[2],
			'alamat' => $row[3],
			'telepon' => $row[4],
			'bagian' => $row[6],
			'tgl_masuk' => $row[7],
			'Aksi' => "<button class=\"btn btn-sm btn-outline btn-warning\" onClick=\"javascript:editData('$row[0]','$row[1]','$row[2]','$row[3]','$row[4]','$row[5]','$row[6]','$row[7]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Edit Data $row[1] \"><i class=\"fa fa-edit\"></i></button>
			<button class=\"btn btn-sm btn-outline btn-primary\" onClick=\"javascript:deleteData('$row[0]','$row[7]');\" data-toggle=\"tooltip\" data-placement=\"left\" title data-original-title=\"Hapus Data $row[0] \"><i class=\"fa fa-trash-o\"></i></button>"

	);
	$no++;
}
$data= json_encode($orders);
print '{'.'"aaData":'.json_encode($orders).'}';

?>
