function refresh() {
	document.location.reload();
}

function kosongkan(){
	  var karyawan_id = document.getElementById("karyawan_id");
	  karyawan_id.value='';
	  var kode = document.getElementById("kode");
	  kode.value='';
	  var nama = document.getElementById("nama");
	  nama.value='';
	  var alamat = document.getElementById("alamat");
	  alamat.value='';
	  var telepon = document.getElementById("telepon");
	  telepon.value='';
	  var email = document.getElementById("email");
	  email.value='';
	  var bagian = document.getElementById("bagian");
	  bagian.value='';
	  var tmk = document.getElementById("tmk");
	  tmk.value='';
	};

function MsgBox(res){
	if(res=="i"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil disimpan.</div>";
	}else if(res=="d"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil dihapus.</div>";
	}else if(res=="u"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil diupdate.</div>";
	}else if(res=="g"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data gagal diproses.</div>";
	}	
}
	
function tambahData(){
		$.ajax({
				type: "POST",
				url: "modules/karyawan/karyawan.form.php",
				success: function(resp){
				document.getElementById("container").innerHTML="";
				$("#container").html(resp);
				},
			});
};

function editData(a,b,c,d,e,f,g,h) {
	$.ajax({
		type: "POST",
		url: "modules/karyawan/karyawan.form.php",
		success: function(resp){
			document.getElementById("container").innerHTML="";
			$("#container").html(resp);
			document.getElementById("title").innerHTML="";
			document.getElementById("title").innerHTML="Edit Data Karyawan";
			var karyawan_id = document.getElementById("karyawan_id");
			karyawan_id.value=a;
			var kode = document.getElementById("kode");
			kode.value=b;
			var nama = document.getElementById("nama");
			nama.value=c;
			var alamat = document.getElementById("alamat");
			alamat.value=d;
			var telepon = document.getElementById("telepon");
			telepon.value=e;
			var email = document.getElementById("email");
			email.value=f;
			var bagian = document.getElementById("bagian");
			bagian.value=g;
			var tmk = document.getElementById("tmk");
			tmk.value=h;
		},
	});	
}

function deleteData(karyawan_id,tmk) {
	var r = confirm("Yakin data akan di hapus?");
	if(r==true) {
		$.ajax({
		  type: "POST",
		  url: "modules/karyawan/karyawan.action.php",
		  data: "action=delete"+"&karyawan_id="+karyawan_id+"&tmk="+tmk,
		  success: function(resp){			
			  refresh();
		  },
		});
	};
}
