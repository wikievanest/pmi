<?php 
use statements\statement;
include 'module.php';
$statement = new statement();

?>
<script>
  $(function() {
       $('#tmk').datetimepicker({
          pickTime: false,
        });
  });
</script>
<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title" id="title">Tambah Data Karyawan</h3>
	</div>
	<br>
	<form class="form-horizontal" method="POST" action="modules/karyawan/karyawan.action.php" enctype="multipart/form-data">
	  <div class="form-group">
	    <label for="karyawan" class="col-sm-2 control-label">Kode Karyawan :</label>
	    <div class="col-sm-5">
	      <input type="hidden" class="form-control" id="karyawan_id" name="karyawan_id">
	      <input type="hidden" class="form-control" id="action" name="action">
	      <input type="text" class="form-control" id="kode" name="kode" placeholder="Kode Karyawan" autofocus> 
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Alamat" class="col-sm-2 control-label">Nama Karyawan :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Karyawan">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Alamat" class="col-sm-2 control-label">Alamat :</label>
	    <div class="col-sm-5">
	      <textarea rows="2" id="alamat" name="alamat" class="form-control" placeholder="Alamat"></textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Telepon" class="col-sm-2 control-label">Telepon :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="telepon" name="telepon" placeholder="Telepon">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Email" class="col-sm-2 control-label">Email :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="email" name="email" placeholder="Email">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="NPWP" class="col-sm-2 control-label">Bagian :</label>
	    <div class="col-sm-5">
	      <select class="form-control" name="bagian" id="bagian">
	      	<option value="">Pilih Bagian</option>
	      	<?php 
	      		$sql = "SELECT bagian_id,nama from mst_bagian";
	      		$result = $statement->query($sql);
	      		while ($row=$statement->fetch_array($result)) {
	      			echo "<option value=\"$row[0]\">$row[1]</option>";
	      		}
	      	?>
	      </select>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="NPWP" class="col-sm-2 control-label">Tanggal Mulai Kerja :</label>
	    <div class="col-sm-5">
	      <input type="text" class="form-control" id="tmk" name="tmk" placeholder="DD-MM-YYYY"  data-format="DD-MM-YYYY">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Foto" class="col-sm-2 control-label">Foto :</label>
	    <div class="col-sm-3">
	      <input type="file" class="form-control" id="foto" name="foto">
	    </div>
	  </div>
	  <div class="col-sm-8">
		  <div id="loading"></div>
		  <div id="peringatan"></div>
	  </div>
	  <br>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
	      <button type="button" class="btn btn-default" onclick=javascript:refresh();><i class="fa fa-refresh"></i> Refresh</button>
	    </div>
	  </div>
		<div class="box-footer">
		</div>
	</form>
</div>
<!-- /.box -->
</div>
<!-- /.box -->