<script>
    $(document).ready(function() {
		var data = "modules/kemasan/kemasan.data.php";
        var oTable = $('#tablekemasanbarang').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": true,
				"bFilter": true,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": false,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "5%" },
					{ "mData": "kemasan","sWidth": "80%" },
					{ "mData": "Aksi","sWidth": "8%" }
				]
		  });
        $('#tablekemasanbarang').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tablekemasanbarang" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Kemasan</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>