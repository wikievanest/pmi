function refresh() {
	document.location.reload();
}

function kosongkan(){
	  var kemasan_id = document.getElementById("kemasan_id");
	  kemasan_id.value='';
	  var kemasan = document.getElementById("kemasan");
	  kemasan.value='';
	};

function MsgBox(res){
	if(res=="i"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil disimpan.</div>";
	}else if(res=="d"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil dihapus.</div>";
	}else if(res=="u"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil diupdate.</div>";
	}else if(res=="g"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data gagal diproses.</div>";
	}	
}
	
function tambahData(){
		$.ajax({
				type: "POST",
				url: "modules/kemasan/kemasan.form.php",
				success: function(resp){
				document.getElementById("container").innerHTML="";
				$("#container").html(resp);
				},
			});
};

function simpanData(){
	var kemasan_id = $('#kemasan_id').val();
	var kemasan = $('#kemasan').val();
	
	if(kemasan_id =="") {
		if(kemasan ==""){
			document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
			$('#kemasan').focus();
		} else {
			$.ajax({
				type: "POST",
				url: "modules/kemasan/kemasan.action.php",
				data: {"kemasan_id":kemasan_id,"kemasan":kemasan},
				success: function(resp){	
					MsgBox(resp);				
					refresh();						
					},	
			});	
		}
	} else {
		updateData(kemasan_id);
	}
					
};

function editData(a,b) {
	$.ajax({
		type: "POST",
		url: "modules/kemasan/kemasan.form.php",
		success: function(resp){
			document.getElementById("container").innerHTML="";
			$("#container").html(resp);
			document.getElementById("title").innerHTML="";
			document.getElementById("title").innerHTML="Edit Data Menu";
			var kemasan_id = document.getElementById("kemasan_id");
			kemasan_id.value=a;
			var kemasan = document.getElementById("kemasan");
			kemasan.value=b;
		},
	});	
}

function updateData(kemasan_id){
	var kemasan = $('#kemasan').val();
	if(kemasan ==""){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#kemasan').focus();
	} else {
		$.ajax({
			type: "POST",
			url: "modules/kemasan/kemasan.action.php",
			data: {"kemasan_id":kemasan_id,"kemasan":kemasan},
			success: function(resp){	
				MsgBox(resp);				
				refresh();						
				},	
		});	
	}				
};

function deleteData(kemasan_id) {
	var r = confirm("Yakin data akan di hapus?");
	if(r==true) {
		$.ajax({
		  type: "POST",
		  url: "modules/kemasan/kemasan.action.php",
		  data: "action=delete"+"&kemasan_id="+kemasan_id,
		  success: function(resp){			
			  refresh();
		  },
		});
	};
}
