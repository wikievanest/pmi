<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title" id="title">Tambah kemasan Barang</h3>
	</div>
	<br>
	<form class="form-horizontal">
	  <div class="form-group">
	    <label for="kemasan" class="col-sm-2 control-label">Nama Kemasan :</label>
	    <div class="col-sm-4">
	      <input type="hidden" class="form-control" id="kemasan_id">
	      <input type="text" class="form-control" id="kemasan" placeholder="Kode kemasan" autofocus>
	    </div>
	  </div>
	  <div class="col-sm-8">
		  <div id="loading"></div>
		  <div id="peringatan"></div>
	  </div>
	  <br>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="button" class="btn btn-primary" onclick="javascript:simpanData()"><i class="fa fa-save"></i> Simpan</button>
	      <button type="button" class="btn btn-default" onclick=javascript:refresh();><i class="fa fa-refresh"></i> Refresh</button>
	    </div>
	  </div>
		<div class="box-footer">
		</div>
	</form>
</div>
<!-- /.box -->
</div>
<!-- /.box -->