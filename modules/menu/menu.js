function refresh() {
	document.location.reload();
}

function kosongkan(){
	  var menu_id = document.getElementById("menu_id");
	  menu_id.value='';
	  var nama_menu = document.getElementById("nama_menu");
	  nama_menu.value='';
	  var deskripsi = document.getElementById("deskripsi");
	  deskripsi.value='';
	  var icon = document.getElementById("icon");
	  icon.value='';
	  var menu_order = document.getElementById("menu_order");
	  menu_order.value='';
	};

function MsgBox(res){
	if(res=="i"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-success alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil disimpan.</div>";
	}else if(res=="d"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil dihapus.</div>";
	}else if(res=="u"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-info alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data berhasil diupdate.</div>";
	}else if(res=="g"){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data gagal diproses.</div>";
	}	
}
	
function tambahData(){
		$.ajax({
				type: "POST",
				url: "modules/menu/menu.form.php",
				success: function(resp){
				document.getElementById("container").innerHTML="";
				$("#container").html(resp);
				},
			});
};

function simpanData(){
	var menu_id = $('#menu_id').val();
	var nama_menu = $('#nama_menu').val();
	var deskripsi = $('#deskripsi').val();
	var icon = $('#icon').val();
	var menu_order = $('#menu_order').val();
	
	if(menu_id =="") {
		if(nama_menu ==""){
			document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
			$('#nama_menu').focus();
		} else {
			$.ajax({
				type: "POST",
				url: "modules/menu/menu.action.php",
				data: {"menu_id":menu_id,"nama_menu":nama_menu,"deskripsi":deskripsi,"icon":icon,"menu_order":menu_order},
				success: function(resp){	
					MsgBox(resp);				
					refresh();						
					},	
			});	
		}
	} else {
		updateData(menu_id);
	}
					
};

function editData(a,b,c,d,e) {
	$.ajax({
		type: "POST",
		url: "modules/menu/menu.form.php",
		success: function(resp){
			document.getElementById("container").innerHTML="";
			$("#container").html(resp);
			document.getElementById("title").innerHTML="";
			document.getElementById("title").innerHTML="Edit Data Menu";
			var menu_id = document.getElementById("menu_id");
			menu_id.value=a;
			var nama_menu = document.getElementById("nama_menu");
			nama_menu.value=b;
			var deskripsi = document.getElementById("deskripsi");
			deskripsi.value=c;
			var icon = document.getElementById("icon");
			icon.value=d;
			var menu_order = document.getElementById("menu_order");
			menu_order.value=e;
		},
	});	
}

function updateData(menu_id){
	var nama_menu = $('#nama_menu').val();
	var deskripsi = $('#deskripsi').val();
	var icon = $('#icon').val();
	var menu_order = $('#menu_order').val();
	
	if(nama_menu ==""){
		document.getElementById("loading").innerHTML="<div class=\"alert alert-danger alert-dismissable\"><i class=\"fa fa-ban\"></i><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">x</button><b>Peringatan !</b> Data yang anda isikan belum lengkap.</div>";
		$('#nama_menu').focus();
	} else {
		$.ajax({
			type: "POST",
			url: "modules/menu/menu.action.php",
			data: {"menu_id":menu_id,"nama_menu":nama_menu,"deskripsi":deskripsi,"icon":icon,"menu_order":menu_order},
			success: function(resp){	
				MsgBox(resp);				
				refresh();						
				},	
		});	
	}				
};

function deleteData(menu_id) {
	var r = confirm("Yakin data akan di hapus?");
	if(r==true) {
		$.ajax({
		  type: "POST",
		  url: "modules/menu/menu.action.php",
		  data: "action=delete"+"&menu_id="+menu_id,
		  success: function(resp){			
			  refresh();
		  },
		});
	};
}
