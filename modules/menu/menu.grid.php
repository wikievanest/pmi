<script>
    $(document).ready(function() {
		var data = "modules/menu/menu.data.php";
        var oTable = $('#tablemenu').dataTable({
				"bProcessing": false,
				"bPaginate": true,
				"bLengthChange": false,
				"bFilter": false,
				"bSort": true,
				"bInfo": true,
				"bAutoWidth": false,
				"sAjaxSource": data,
				"aoColumns": [
					{ "mData": "No","sWidth": "3%" },
					{ "mData": "nama_menu","sWidth": "30%" },
					{ "mData": "deskripsi","sWidth": "20%" },
					{ "mData": "order","sWidth": "3%" },
					{ "mData": "Aksi","sWidth": "5%" }
				]
		  });
        $('#tablemenu').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })
    });
</script> 
<table id="tablemenu" class="table table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama Menu</th>
			<th>Deksripsi</th>
			<th>Order</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		
	</tbody>
</table>