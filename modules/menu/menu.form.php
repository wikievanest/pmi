<div class="box box-primary">
	<div class="box-header">
		<h3 class="box-title" id="title">Tambah Data Menu</h3>
	</div>
	<br>
	<form class="form-horizontal">
	  <div class="form-group">
	    <label for="Menu" class="col-sm-2 control-label">Nama Menu :</label>
	    <div class="col-sm-6">
	      <input type="hidden" class="form-control" id="menu_id">
	      <input type="text" class="form-control" id="nama_menu" placeholder="Nama Menu" autofocus>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Deskripsi" class="col-sm-2 control-label">Deskripsi :</label>
	    <div class="col-sm-6">
	      <textarea rows="2" id="deskripsi" class="form-control" placeholder="Deskripsi"></textarea>
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Menu" class="col-sm-2 control-label">Icon :</label>
	    <div class="col-sm-6">
	      <input type="text" class="form-control" id="icon" placeholder="fa fa-laptop">
	    </div>
	  </div>
	  <div class="form-group">
	    <label for="Deskripsi" class="col-sm-2 control-label">Menu Order :</label>
	    <div class="col-sm-1">
	      <input type="number" class="form-control" id="menu_order">
	    </div>
	  </div>
	  <div class="col-sm-8">
		  <div id="loading"></div>
		  <div id="peringatan"></div>
	  </div>
	  <br>
	  <div class="form-group">
	    <div class="col-sm-offset-2 col-sm-10">
	      <button type="button" class="btn btn-primary" onclick="javascript:simpanData()"><i class="fa fa-save"></i> Simpan</button>
	      <button type="button" class="btn btn-default" onclick=javascript:refresh();><i class="fa fa-refresh"></i> Refresh</button>
	    </div>
	  </div>
		<div class="box-footer">
		</div>
	</form>
</div>
<!-- /.box -->
</div>
<!-- /.box -->