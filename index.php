<?php
include_once 'config/application.config.php';
use statements\statement;
$statement = new statement();
$image = new Image();
error_reporting(-1);
if(empty($_SESSION['user_id']) and empty($_SESSION['username'])) {
	header('location: login.php');
}
include 'includes/header.php';
?>


    <body class="skin-blue fixed">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.php" class="logo" style="top: 0px">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                <!-- <img class="img-circle" alt="" src="assets/img/logo.jpg" height="50px"> -->
                PASADENA M.I
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <?php include 'includes/topnav.php';?>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include 'includes/profile.php'; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Main content -->
                <section class="content">
					<div class="row">
                    		<!-- Small boxes (Stat box) -->
                    		<?php include 'includes/content.php'; ?>
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


<?php 
include 'includes/footer.php';
?>
