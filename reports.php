<?php
include_once 'includes/header.php';
include_once 'library/database.php';
include_once 'library/statements.php';
include_once 'library/sessions.php';
require_once 'library/fpdf17/fpdf.php';
include_once 'library/helper.php';
$type = $_GET['type'];
$act = $_GET['act'];
if($type=='pdf') {
	if($act=='faktur') {
		include "reports/pdf/faktur.php";
	} elseif ($act=='pajak') {
		include "reports/pdf/pajak.php";
	} elseif ($act=='printfaktur') {
		include "reports/pdf/printfaktur.php";
	}
} else if($type=='html') {
	if($act=='faktur') {
		include "reports/faktur.php";
	} elseif ($act=='pajak') {
		include "reports/pajak.php";
	} elseif ($act=='printfaktur') {
		include "reports/printfaktur.php";
	}
}

?>