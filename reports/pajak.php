<?php 
error_reporting(E_ALL);
use statements\statement;
$statement = new statement();
$no_tran = $_GET['notran'];
$sql = "SELECT a.NAMA_PERUSAHAAN,
			a.ALAMAT,
			a.NPWP,
			DATE_FORMAT(a.TGL_NPWP,'%d-%m-%Y')
		FROM mst_perusahaan a ";
$result = $statement->query($sql);
$row = $statement->fetch_array($result);

$sql1 = "SELECT a.NAMA,
			a.ALAMAT,
			a.NPWP
		 FROM mst_pelanggan a 
		 INNER JOIN tran_penjualan_item b
		 ON b.PELANGGAN_ID=a.PELANGGAN_ID
		 WHERE b.NO_TRAN='$no_tran'";
$result1 = $statement->query($sql1);
$row1 = $statement->fetch_array($result1);

$sql2 = "SELECT a.NAMA_BARANG,
		b.JML,
		b.HARGA,
		b.SUBTOTAL
		FROM mst_barang a
		INNER JOIN tran_penjualan_item b
		ON b.BARANG_ID=a.BARANG_ID
		WHERE b.NO_TRAN = '$no_tran'";
$result2 = $statement->query($sql2);

$sql3="SELECT a.SUBTOTAL,
			a.DISKON,
			a.PPN,
			a.TOTAL_BAYAR
	   FROM tran_penjualan a 
	   WHERE a.NO_TRAN='$no_tran'";
$result3 = $statement->query($sql3);
$row3=$statement->fetch_array($result3);

?>

<!-- Main content -->
<div class="contentx invoicepajak">
	<div class="row">
		<div class="col-xs-12">
			<table class="table">
					<tr>
						<td style="width: 100%">
							<p style="text-align: center;font-size: 22px;">FAKTUR PAJAK</p>
							<p>Kode dan Nomor Faktur Pajak : 020.002.14.14369992</p>
						</td>
					</tr>
			</table>
			<table class="table" style="margin-top: -20px;">
					<tr>
						<td colspan="2" style="width: 100%">
							Pengusaha Kena Pajak
						</td>
					</tr>
					<tr>
						<td style="width: 20%">
							Nama
						</td>
						<td style="width: 80%">
							: <?php echo $row['0'];?>
						</td>
					</tr>
					<tr>
						<td style="width: 20%">
							Alamat
						</td>
						<td style="width: 80%">
							: <?php echo $row['1'];?>
						</td>
					</tr>
					<tr>
						<td style="width: 20%">
							N.P.W.P
						</td>
						<td style="width: 80%">
							: <?php echo $row['2'];?>
						</td>
					</tr>
					<tr>
						<td style="width: 20%">
							Tanggal Pengukuhan
						</td>
						<td style="width: 80%">
							: <?php echo $row['3'];?>
						</td>
					</tr>
			</table>
			<table class="table" style="margin-top: -20px;">
					<tr>
						<td colspan="4" style="width: 100%">
							Pembeli Barang Kena Pajak / Penerima Jasa Kena Pajak
						</td>
					</tr>
					<tr>
						<td colspan="2" style="width: 20%">
							Nama
						</td>
						<td colspan="2" style="width: 60%">
							: <?php echo $row1['0'];?>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="width: 20%">
							Alamat
						</td>
						<td style="width: 60%">
							: <?php echo $row1['1'];?>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="width: 20%">
							N.P.W.P
						</td>
						<td style="width: 60%">
							: <?php echo $row1['2'];?>
						</td>
						<td style="width: 20%">
							NPPKP : <?php echo $row1['2'];?>
						</td>
					</tr>
			</table>
			<table class="table table-bordered" style="margin-top: -20px">
				<tr>
					<td rowspan="2" style="text-align: center;width: 5%">No Urut</td>
					<td rowspan="2" colspan="3" style="text-align: center;width: 70%">Nama Barang Kena Pajak /<br>Jasa Kena Pajak</td>
					<td colspan="2" style="text-align: center;width: 25%">Harga Jual / Pengantian / <br>Uang Muka / Termijn</td>
				</tr>
				<tr>
					<td style="text-align: center;width: 10%">Valas *)</td>
					<td style="text-align: center;width: 15%">Rp. </td>
				</tr>
				
				<!-- Body for Item -->
				<?php 
					$i = 1;
						while ($row2=$statement->fetch_array($result2)) {
						echo "<tr style=\"width: 100px\">";
						echo "<td style=\"text-align: center;\">$i</td>";
						echo "<td>$row2[0]</td>";
						echo "<td style=\"text-align: center;\">$row2[1]</td>";
						echo "<td style=\"text-align: center;\">".number_format($row2[2],2)."</td>";
						echo "<td style=\"text-align: center;\"></td>";
						echo "<td style=\"text-align: right;\">".number_format($row2[3],2)."</td>";
						echo "</tr>";
						$i++;
					}
				
				?>
			</table>
			<table class="table table-bordered">
				<tr>
					<td rowspan="2" style="text-align: left;width: 75%">Jumlah Harga Jual / Penggantian / Uang Muka / Termijn *)</td>
				</tr>
				<tr>
					<td style="text-align: center;width: 10%"></td>
					<td style="text-align: right;width: 15%"><?php echo number_format($row3['0'],2);?></td>
				</tr>
				<tr>
					<td rowspan="2" style="text-align: left;width: 75%">Dikurangi Potongan Harga</td>
				</tr>
				<tr>
					<td style="text-align: center;width: 10%"></td>
					<td style="text-align: right;width: 15%"><?php echo number_format($row3['1'],2);?></td>
				</tr>
				<tr>
					<td rowspan="2" style="text-align: left;width: 75%">Dikurangi Uang Muka yang diterima</td>
				</tr>
				<tr>
					<td style="text-align: center;width: 10%"></td>
					<td style="text-align: right;width: 15%">-</td>
				</tr>
				<tr>
					<td rowspan="2" style="text-align: left;width: 75%">Dasar Pengenaan Pajak</td>
				</tr>
				<tr>
					<td style="text-align: center;width: 10%"></td>
					<td style="text-align: right;width: 15%"><?php echo number_format($row3['3'],2);?></td>
				</tr>
				<tr>
					<td rowspan="2" style="text-align: left;width: 75%">PPN = 10 % x Dasar Pengenaan Pajak</td>
				</tr>
				<tr>
					<td style="text-align: center;width: 10%"></td>
					<td style="text-align: right;width: 15%"><?php echo number_format($row3['2'],2);?></td>
				</tr>
			</table>
			<table style="width: 100%">
			  <tr>
			    <td style="width: 45%">Pajak Penjualan Atas Barang Mewah
			    <table class="table table-bordered">
			      <tr style="width: 30px;">
			        <td><div align="center">Tarif</div></td>
			        <td><div align="center">DPP</div></td>
			        <td><div align="center">PPn BM</div></td>
			      </tr>
			      <tr>
			        <td>...................%</td>
			        <td>Rp. ...................</td>
			        <td>Rp. ...................</td>
			      </tr>
			      <tr>
			        <td>...................%</td>
			        <td>Rp. ...................</td>
			        <td>Rp. ...................</td>
			      </tr>
			      <tr>
			        <td>...................%</td>
			        <td>Rp. ...................</td>
			        <td>Rp. ...................</td>
			      </tr>
			      <tr>
			        <td>...................%</td>
			        <td>Rp. ...................</td>
			        <td>Rp. ...................</td>
			      </tr>
			      <tr>
			        <td colspan="2"><div align="center">JUMLAH</div></td>
			        <td>Rp. ...................</td>
			      </tr>
			    </table></td>
			    <td style="width: 55%;text-align:center">
			    	Semarang, <?php echo date('d/m/Y');?>
			    	<br>
			    	<br>
			    	<br>
			    	<br>
			    	<br>
			    	<br>
			    	DENNY SURYAWAN
			    	
			    </td>
			  </tr>
			</table>
		</div>
		<!-- /.col -->
	</div>

	<!-- this row will not appear when printing -->
	<div class="row no-print">
		<div class="col-xs-12">
			<br>
			<br>
			<button class="btn btn-primary" onclick="window.print();">
				<i class="fa fa-print"></i> Cetak Faktur
			</button>
		</div>
	</div>
</div>
<!-- /.content -->