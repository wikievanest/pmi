<?php 

function Terbilang($x)
{
	$abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	if ($x < 12)
		return " " . $abil[$x];
	elseif ($x < 20)
	return Terbilang($x - 10) . "belas";
	elseif ($x < 100)
	return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
	elseif ($x < 200)
	return " seratus" . Terbilang($x - 100);
	elseif ($x < 1000)
	return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
	elseif ($x < 2000)
	return " seribu" . Terbilang($x - 1000);
	elseif ($x < 1000000)
	return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
	elseif ($x < 1000000000)
	return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}

$no_tran = $_GET['notran'];
$sql ="SELECT a.NAMA,a.NPWP from mst_pelanggan a 
	   INNER JOIN tran_penjualan_item b
	   ON b.PELANGGAN_ID=a.PELANGGAN_ID
	   WHERE b.NO_TRAN='$no_tran'";
$result = $statement->query($sql);
$row = $statement->fetch_array($result);

$sql1 = "SELECT a.NO_TRAN,
		 a.NO_PO,
		 DATE_FORMAT(a.CREATED,'%d-%m-%Y'),
		 CASE a.STATUS_BAYAR 
			WHEN 0 THEN 'KREDIT'
			WHEN 1 THEN 'LUNAS'
		 END as STATUS,
		 DATE_FORMAT(a.TGL_JATUH_TEMPO,'%d-%m-%Y'),
		 b.KODE,
		 a.SUBTOTAL,
		 a.DISKON,
		 a.SUBTOTAL - a.DISKON as TOTAL2,
		 a.PPN,
		 a.TOTAL_BAYAR
		 FROM tran_penjualan a 
		 LEFT OUTER JOIN mst_karyawan b
		 ON b.KARYAWAN_ID=a.SALESMAN_ID
		 WHERE a.NO_TRAN = '$no_tran'";
$result1 = $statement->query($sql1);
$row1 = $statement->fetch_array($result1);
$sql2 = "SELECT a.KODE,
			a.NAMA_BARANG,
			b.JML,
			b.HARGA,
			b.SUBTOTAL
		 FROM mst_barang a 
		 INNER JOIN tran_penjualan_item b
		 ON b.BARANG_ID=a.BARANG_ID
		WHERE b.NO_TRAN = '$no_tran'";
$result2 = $statement->query($sql2);

$pajak = strpos($row1['0'], 'P');

?>

<!-- Main content -->
<div class="contentx invoices">
	<!-- title row -->
	<div class="row">
		<div class="col-xs-2 noprint">
			<img alt="Logo" src="assets/img/logo2.jpg" width="120" height="150">
		</div>
		<div class="col-xs-5 noprint">
			<p style="font-size: 24px; margin-right: 90px;text-align:center"><b>FAKTUR</b></p>
			<address>
				<strong style="font-size: 20px;">pt. pasadena medical indonesia</strong><br>
				Jl. Candi Permata 1 No. 193 Pasadena - Semarang<br>
				Telpon: 024 7602068, Fax : 024 760 2068<br>
				Email : pmi_smg@yahoo.com<br>
			</address>
			
		</div>	
		<div class="col-xs-5 print-margin">
			<div>
			<strong class="noprint">Kepada :</strong><br>
			<address>
				
				<?php echo $row['0'].'<br>';?>
			</address>
			</div>
			<div style="padding-top: 8px;">
			<strong class="noprint">NPWP :</strong><br>
			<address>
				
				<?php 
				if($pajak !== false) {
					echo $row['1'];
				} 
				?>
			</address>
			</div>
		</div>	
		<!-- /.col -->
	</div>

	<div class="row">
		<div class="col-xs-12">
			<table class="table">
					<tr class="noprint">
						<th style="text-align: center;width: 15%">KODE FAKTUR</th>
						<th style="text-align: center;width: 12%">NO. FAKTUR</th>
						<th style="text-align: center;width: 12%">TANGGAL</th>
						<th style="text-align: center;width: 15%">NO. SP</th>
						<th style="text-align: center;width: 12%">PAMBAYARAN</th>
						<th style="text-align: center;width: 17%">TGL. J. TEMPO</th>
						<th style="text-align: center;width: 15%">PENJAJA</th>
					</tr>
					<tr>
						<td style="text-align: center;"><?php echo $row1['0']?></td>
						<td style="text-align: center;"><?php echo $row1['0']?></td>
						<td style="text-align: center;"><?php echo $row1['2']?></td>
						<td style="text-align: center;"></td>
						<td style="text-align: center;"><?php echo $row1['3']?></td>
						<td style="text-align: center;"><?php echo $row1['4']?></td>
						<td style="text-align: center;"><?php echo $row1['5']?></td>
					</tr>
			</table>
			<table class="table" style="margin-top: -20px">
				<tr class="noprint">
						<th style="text-align: center;width: 15%">KD. PRODUK</th>
						<th style="text-align: center;width: 39%">NAMA BARANG</th>
						<th style="text-align: center;width: 12%">DISKON (%)</th>
						<th style="text-align: center;width: 8%">UNIT</th>
						<th style="text-align: center;width: 9%">HARGA</th>
						<th style="text-align: center;width: 15%">TOTAL</th>
				</tr>
				<?php 
					while ($row2=$statement->fetch_array($result2)) {
						echo "<tr style=\"width: 100px;\">";
						echo "<td style=\"text-align: center;\">c</td>";
						echo "<td>$row2[1]</td>";
						echo "<td style=\"text-align: center;\">0</td>";
						echo "<td style=\"text-align: center;\">$row2[2]</td>";
						echo "<td style=\"text-align: center;\">".number_format($row2[3],2)."</td>";
						echo "<td style=\"text-align: center;\">".number_format($row2[4],2)."</td>";
						echo "</tr>";
					}
					
				?>
			</table>
			<table class="table">
					<tr class="noprint">
						<th style="text-align: center;width: 36%"></th>
						<th style="text-align: center;width: 12%">TOTAL 1</th>
						<th style="text-align: center;width: 12%">POTONGAN</th>
						<th style="text-align: center;width: 12%">TOTAL 2</th>
						<th style="text-align: center;width: 12%">PPN</th>
						<th style="text-align: center;width: 15%">JUMLAH TAGIHAN</th>
					</tr>
					<tr>
						<td></td>
						<td style="text-align: center"><?php echo number_format($row1['6'],2)?></td>
						<td style="text-align: center"><?php echo number_format($row1['7'],2)?></td>
						<td style="text-align: center"><?php echo number_format($row1['8'],2)?></td>
						<td style="text-align: center">
							<?php 
								if($pajak !== false) {
									echo number_format($row1['9'],2);
								} else {
									echo "0.00";
								}
							?>
						</td>
						<td style="text-align: center"><?php echo number_format($row1['10'],2)?></td>
					</tr>

			</table>
			<table class="table">
					<tr>
						<th style="width: 100%">TERBILANG RP.  <?php echo ucwords(Terbilang($row1['10'])); ?></th>
					</tr>
			</table>
			<table class="table no-print">
					<tr>
						<th style="width: 25%">
							<p style="text-align: center;">PENERIMA		TGL.</p>
							Cap & Tanda Tangan
							<br>
							<br>
							<br>
							<br>
							( ---------------------------- ) 
						</th>
						<th style="width: 23%">
						
							PERHATIAN : <br>
							<p style="font-size: 10px">
							- Faktur ini berlaku sebagai kwitansi 
							- Pembayaran dengan cheque/bilyet giro atau wesel dianggap lunas setelah melalui kliring 
							- Barang-barang yang sudah diserahkan tidak dapat dikembalikan/ditukar dengan barang jenis lain
							</p>
						</th>
						<th style="text-align: center;width: 24%"></th>
						<th style="text-align: center;width: 12%">
						
							<div style="border: 1px solid;width: 100%;height: 100%"><br><br><br>MATERAI<br><br><br></div>
						
						</th>
						<th style="text-align: center;width: 15%">HORMAT KAMI, <br><br><br><br><br> ( DENY SURYAWAN )</th>
					</tr>
			</table>
		</div>
		<!-- /.col -->
	</div>

	<!-- this row will not appear when printing -->
	<div class="row no-print">
		<div class="col-xs-12">
			<br>
			<br>
			<a href="reports.php?type=printfaktur&notran=<?php echo $row1['0']; ?>" class="btn btn-primary"><i class="fa fa-print"></i> Cetak Faktur</a>
		</div>
	</div>
</div>
<!-- /.content -->