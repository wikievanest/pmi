<?php 
error_reporting(0);
use statements\statement;
use helper\Helper;
ob_end_clean();
class PDF extends FPDF {
	function pt() {
		$this->Image('assets/img/logo2.jpg',5,8,18);
		$this->SetFont('Arial','B',16);
		$this->Text(45,9, 'FAKTUR');
		$this->SetFont('Arial','B',14);
		$sql = "SELECT nama_perusahaan,
					alamat,
					kota,
					telepon,
					fax,
					email,
					npwp
				from mst_perusahaan";
		$statement = new statement();
		$result = $statement->query($sql);
		$row = $statement->fetch_row($result);
		$this->SetTextColor(0,0,0);
		$this->Text(25,14, $row['0']);
		$this->SetFont('Arial','',7);
		$this->SetTextColor(255,0,0);
		$this->SetTextColor(255,0,0);
		$this->Text(25,18, $row['1']);
		$this->Text(25,21, $row['2']);
		$this->Text(25,24, 'Telpon: '.$row['3'].', Fax : '.$row['4'].'');
		$this->Text(25,27, 'Email : '.$row['5'].'');
		$this->Text(25,30, 'NPWP : '.$row['6'].'');
	}
	
	function customer() {
		$statement = new statement();
		$no_tran = $_GET['notran'];
		$sql ="SELECT a.NAMA,a.ALAMAT,a.KOTA,a.NPWP from mst_pelanggan a
			   INNER JOIN tran_penjualan_item b
			   ON b.PELANGGAN_ID=a.PELANGGAN_ID
			   WHERE b.NO_TRAN='$no_tran'";
		$result = $statement->query($sql);
		$row = $statement->fetch_row($result);
		$this->SetTextColor(0);
		$this->SetFont('Arial','B',10);
		$this->Text(110,9, 'Kepada :');
		$this->SetTextColor(255,0,0);
		$this->SetFont('Arial','',10);
		$this->Text(126,9,  $row['0']);
		$this->Text(126,14, $row['1']);
		$this->Text(126,19, $row['2']);
		$this->npwp($row['3']);
	}
	
	function npwp($npwp) {
		$this->SetTextColor(0,0,0);
		$this->SetFont('Arial','B',10);
		$this->Text(110,28, 'NPWP :');
		$this->SetFont('Arial','',10);
		$this->SetTextColor(255,0,0);
		$this->Text(126,28, $npwp);
	}
	
	function tblHeader() {
		$this->Ln(28);	
		$this->SetTextColor(0,0,0);
		$this->setFont('Arial','B',8);
		$this->setFillColor(255,255,255);
		$this->cell(26,4,'KODE FAKTUR',1,0,'C',1);
		$this->cell(25,4,'NO. FAKTUR',1,0,'C',1);
		$this->cell(25,4,'TANGGAL',1,0,'C',1);
		$this->cell(28,4,'NO. SP',1,0,'C',1);
		$this->cell(25,4,'PEMBAYARAN',1,0,'C',1);
		$this->cell(46,4,'TGL. J. TEMPO',1,0,'C',1);
		$this->cell(30,4,'PENJAJA',1,1,'C',1);
	}
	
	function tblHeaderValue() {
		$statement = new statement();
		$no_tran = $_GET['notran'];
		$sql1 = "SELECT a.NO_TRAN,
				a.NO_PO,
				DATE_FORMAT(a.CREATED,'%d-%m-%Y'),
				CASE a.STATUS_BAYAR
				WHEN 0 THEN 'KREDIT'
				WHEN 1 THEN 'LUNAS'
				END as STATUS,
				DATE_FORMAT(a.TGL_JATUH_TEMPO,'%d-%m-%Y'),
				b.KODE,
				a.SUBTOTAL,
				a.DISKON,
				a.SUBTOTAL - a.DISKON as TOTAL2,
				a.PPN,
				a.TOTAL_BAYAR
				FROM tran_penjualan a
				LEFT OUTER JOIN mst_karyawan b
				ON b.KARYAWAN_ID=a.SALESMAN_ID
				WHERE a.NO_TRAN = '$no_tran'";
		$result1 = $statement->query($sql1);
		$row1 = $statement->fetch_array($result1);
		$this->setFont('Arial','',8);
		$this->SetTextColor(255,0,0);
		$this->setFillColor(255,255,255);
		$this->cell(26,5,$row1['0'],1,0,'C',1);
		$this->cell(25,5,$row1['0'],1,0,'C',1);
		$this->cell(25,5,$row1['2'],1,0,'C',1);
		$this->cell(28,5,'',1,0,'C',1);
		$this->cell(25,5,$row1['3'],1,0,'C',1);
		$this->cell(46,5,$row1['4'],1,0,'C',1);
		$this->cell(30,5,$row1['5'],1,1,'C',1);
	}
	
	function tblHeaderItem() {
		$this->SetTextColor(0,0,0);
		$this->setFont('Arial','B',8);
		$this->setFillColor(255,255,255);
		$this->cell(26,4,'KD. PRODUK',1,0,'C',1);
		$this->cell(78,4,'NAMA BARANG',1,0,'C',1);
		$this->cell(25,4,'DISKON (%)',1,0,'C',1);
		$this->cell(20,4,'UNIT',1,0,'C',1);
		$this->cell(26,4,'HARGA',1,0,'C',1);
		$this->cell(30,4,'TOTAL',1,1,'C',1);
	}
	
	function Content() {
		$statement = new statement();
		$no_tran = $_GET['notran'];
		$sql = "SELECT a.KODE,
			a.NAMA_BARANG,
			b.JML,
			b.HARGA,
			b.SUBTOTAL
		 FROM mst_barang a 
		 INNER JOIN tran_penjualan_item b
		 ON b.BARANG_ID=a.BARANG_ID
		WHERE b.NO_TRAN = '$no_tran'";
		$result = $statement->query($sql);
		$i=0;
		while ($row=$statement->fetch_array($result)) {
			
			if($i==11) {
				$this->AddPage();
			}
			$this->SetTextColor(255,0,0);
			$this->setFont('Arial','',8);
			$this->setFillColor(255,255,255);
			$this->cell(26,4,$row[0],1,0,'C',1);
			$this->cell(78,4,$row[1],1,0,'L',1);
			$this->cell(25,4,'0',1,0,'C',1);
			$this->cell(20,4,$row[2],1,0,'C',1);
			$this->cell(26,4,number_format($row[3],2),1,0,'C',1);
			$this->cell(30,4,number_format($row[4],2),1,1,'C',1);
			$i++;
		}
	}
	function Header() {
	    $this->pt();
	    $this->customer();
	    $this->npwp();
	    $this->tblHeader();
	    $this->tblHeaderValue();
	    $this->tblHeaderItem();
	    
	}
	
	function totalHeader() {
		$this->SetTextColor(255,0,0);
		$this->cell(71,5,'Halaman '.$this->PageNo().'/{nb}',1,0,'C',1);
		$this->setFont('Arial','B',8);
		$this->SetTextColor(0);
		$this->setFillColor(255,255,255);
		$this->cell(26,5,'TOTAL 1',1,0,'C',1);
		$this->cell(26,5,'POTONGAN',1,0,'C',1);
		$this->cell(26,5,'TOTAL 2',1,0,'C',1);
		$this->cell(26,5,'PPN',1,0,'C',1);
		$this->cell(30,5,'JUMLAH TAGIHAN',1,0,'C',1);
		$this->Ln();
	}
	
	function total() {
		
		$page = $this->PageNo();
		$pageNumber = 2;
		if($this->isFinished) {
			$statement = new statement();
			$no_tran = $_GET['notran'];
			$sql2 = "SELECT a.NO_TRAN,
				a.NO_PO,
				DATE_FORMAT(a.CREATED,'%d-%m-%Y'),
				CASE a.STATUS_BAYAR
				WHEN 0 THEN 'KREDIT'
				WHEN 1 THEN 'LUNAS'
				END as STATUS,
				DATE_FORMAT(a.TGL_JATUH_TEMPO,'%d-%m-%Y'),
				b.KODE,
				a.SUBTOTAL,
				a.DISKON,
				a.SUBTOTAL - a.DISKON as TOTAL2,
				a.PPN,
				a.TOTAL_BAYAR
				FROM tran_penjualan a
				LEFT OUTER JOIN mst_karyawan b
				ON b.KARYAWAN_ID=a.SALESMAN_ID
				WHERE a.NO_TRAN = '$no_tran'";
			$result2 = $statement->query($sql2);
			$row2 = $statement->fetch_array($result2);
			$pajak = strpos($row2['0'], 'P');
			$total1=number_format($row2['6'],2);
			$potongan=number_format($row2['7'],2);
			$total2=number_format($row2['8'],2);
			if($pajak !== false) {
				$ppn = number_format($row2['9'],2);
			} else {
				$ppn = "0.00";
			}
			$totalbayar=number_format($row2['10'],2);
			$this->SetTextColor(255,0,0);
			$this->setFont('Arial','',8);
			$this->setFillColor(255,255,255);
			$this->cell(71,6,'',1,0,'C',1);
			$this->cell(26,6,$total1,1,0,'C',1);
			$this->cell(26,6,$potongan,1,0,'C',1);
			$this->cell(26,6,$total2,1,0,'C',1);
			$this->cell(26,6,$ppn,1,0,'C',1);
			$this->cell(30,6,$totalbayar,1,1,'C',1);
			$terbilang = new Helper();
			$bil = ucwords($terbilang->Terbilang($row2[10]));
			$this->terbilang($bil);
		} else {
			$this->setFont('Arial','',8);
			$this->setFillColor(255,255,255);
			$this->cell(71,6,'',1,0,'C',1);
			$this->cell(26,6,'',1,0,'C',1);
			$this->cell(26,6,'',1,0,'C',1);
			$this->cell(26,6,'',1,0,'C',1);
			$this->cell(26,6,'',1,0,'C',1);
			$this->cell(30,6,'',1,1,'C',1);
			$terbilang = '';
			$terbilang = new Helper();
			$bil = ucwords($terbilang->Terbilang($row2[10]));
			$this->terbilang($bil);
			
		}
	}
	
	function terbilang($x) {
		$this->setFont('Arial','',8);
		$this->setFillColor(255,255,255);
		$this->cell(205,6,'Terbilang: '.$x,1,1,'L',1);
	}
	
	function signature() {
		$this->SetTextColor(0,0,0);
		$this->setFillColor(255,255,255);
		$this->cell(56,28,'',1,0,'C',1);
		$this->cell(41,28,'',1,0,'C',1);
		$this->cell(52,28,'',1,0,'C',1);
		$this->cell(56,28,'',1,1,'C',1);
	}
	
	function kepada() {
		$this->SetFont('Arial','B',8);
		$this->Text(184,112, 'HORMAT KAMI,');
		$this->Text(181,133, '( DENI SURYAWAN )');
	}
	
	function materai() {
		$this->Text(162,122, 'MATERAI');
	}
	
	function perhatian() {
		$this->Text(64,112, 'PEHATIAN :');
		$this->SetFont('Arial','',6);
		$this->Text(63,115, '-Faktur ini berlaku sebagai kuintansi');
		$this->Text(63,118, '-Pembayaran dengan cheque/bilyet giro');
		$this->Text(63,121, ' atau wesel dianggap lunas setelah');
		$this->Text(63,124, ' melalui kliring');
		$this->Text(63,127, '-Barang-barang yang sudah diserahkan');
		$this->Text(63,130, ' tidak dapat dikembalikan/ditukar dengan');
		$this->Text(63,133, ' barang jenis lain');
	}
	
	function penerima() {
		$this->SetFont('Arial','B',8);
		$this->Text(14,112, 'PENERIMA');
		$this->Text(40,112, 'TGL. ');
		$this->Text(7,115, 'Cap & Tanda Tangan');
		$this->Text(7,133, '(_________________)');
	}
	// Page footer
	function Footer() {
		// Go to 1.5 cm from bottom
		$this->SetY(-50);
		$this->totalHeader();
		$this->total();
		$this->signature();
		$this->kepada();
		$this->materai();
		$this->perhatian();
		$this->penerima();
	}
}

$pdf=new PDF('L','mm',array(215.7,139.77));
$pdf->SetMargins(5,5,5,5);
$pdf->Line(0,217.7,0,0);
$pdf->AliasNbPages();
$pdf->SetAutoPageBreak(true);
$pdf->AddPage();
$pdf->Content();
$pdf->isFinished = true;
$pdf->Output();
?>