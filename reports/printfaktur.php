<?php 
function Terbilang($x)
{
	$abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	if ($x < 12)
		return " " . $abil[$x];
	elseif ($x < 20)
	return Terbilang($x - 10) . "belas";
	elseif ($x < 100)
	return Terbilang($x / 10) . " puluh" . Terbilang($x % 10);
	elseif ($x < 200)
	return " seratus" . Terbilang($x - 100);
	elseif ($x < 1000)
	return Terbilang($x / 100) . " ratus" . Terbilang($x % 100);
	elseif ($x < 2000)
	return " seribu" . Terbilang($x - 1000);
	elseif ($x < 1000000)
	return Terbilang($x / 1000) . " ribu" . Terbilang($x % 1000);
	elseif ($x < 1000000000)
	return Terbilang($x / 1000000) . " juta" . Terbilang($x % 1000000);
}
$no_tran = $_GET['notran'];
$sql ="SELECT a.NAMA,a.ALAMAT,a.NPWP from mst_pelanggan a 
	   INNER JOIN tran_penjualan_item b
	   ON b.PELANGGAN_ID=a.PELANGGAN_ID
	   WHERE b.NO_TRAN='$no_tran'";
$result = $statement->query($sql);
$row = $statement->fetch_array($result);

$sql1 = "SELECT a.NO_TRAN,
		 a.NO_PO,
		 DATE_FORMAT(a.CREATED,'%d-%m-%Y'),
		 CASE a.STATUS_BAYAR 
			WHEN 0 THEN 'KREDIT'
			WHEN 1 THEN 'LUNAS'
		 END as STATUS,
		 DATE_FORMAT(a.TGL_JATUH_TEMPO,'%d-%m-%Y'),
		 b.KODE,
		 a.SUBTOTAL,
		 a.DISKON,
		 a.SUBTOTAL - a.DISKON as TOTAL2,
		 a.PPN,
		 a.TOTAL_BAYAR
		 FROM tran_penjualan a 
		 LEFT OUTER JOIN mst_karyawan b
		 ON b.KARYAWAN_ID=a.SALESMAN_ID
		 WHERE a.NO_TRAN = '$no_tran'";
$result1 = $statement->query($sql1);
$row1 = $statement->fetch_array($result1);
$sql2 = "SELECT a.KODE,
			a.NAMA_BARANG,
			b.JML,
			b.HARGA,
			b.SUBTOTAL
		 FROM mst_barang a 
		 INNER JOIN tran_penjualan_item b
		 ON b.BARANG_ID=a.BARANG_ID
		WHERE b.NO_TRAN = '$no_tran'";
$result2 = $statement->query($sql2);
$pajak = strpos($row1['0'], 'P');
?>

<!-- Main content -->
<div class="contentx invoice">
	<table width="760" border="0">
	  <tr>
	  	<td width="433.2" height="60.8"></td>
	    <td><?php echo $row['0'].'<br>'.$row['1'];?></td>
	  </tr>
	  <tr>
	  	<td width="433.2" height="60.8"></td>
	    <td>
				
				<?php 
				if($pajak !== false) {
					echo $row['2'];
				} 
				?>
	    </td>
	  </tr>
	</table>
	<table width="760" border="0">
	  <tr>
	  	<td width="106.4" height="35.8"></td>
	    <td width="91.2" height="35.8"></td>
	    <td width="100.7" height="35.8"></td>
	    <td width="121.6" height="35.8"></td>
	    <td width="79.8" height="35.8"></td>
	    <td width="152" height="35.8"></td>
	    <td width="106.4" height="35.8"></td>
	  </tr>
	</table>
	<!-- KODE FAKTUR-->
	<table width="760" border="0">
	  <tr>
	  	<td width="106.4" height="22.8"><div align="center"><?php echo $row1['0']?></div></td>
	    <td width="91.2" height="22.8"><div align="left"><?php echo $row1['0']?></div></td>
	    <td width="120.7" height="22.8"><div align="left"><?php echo $row1['2']?></div></td>
	    <td width="85" height="22.8"><div align="left"></div></td>
	    <td width="60" height="22.8"><div align="left"><?php echo $row1['3']?></div></td>
	    <td width="124" height="22.8"><div align="left"><?php echo $row1['4']?></div></td>
	    <td width="90.4" height="22.8"><div align="left"><?php echo $row1['5']?></div></td>
	  </tr>
	</table>
	<!-- LIST ITEM-->
	<table width="760" border="0">
	  <tr>
	  	<td width="106.4" height="19"></td>
	    <td width="315.4" height="19"></td>
	    <td width="79.8" height="19"></td>
	    <td width="57" height="19"></td>
	    <td width="95" height="19"></td>
	    <td width="106.4" height="19"></td>
	</tr>
	</table>
	<div style="width:760px;height:184.3px">
	    <table width="760" border="0">
	      <tr>
	        
	        
	        
	        
	        
	        
	      </tr>
	      	  
	  <?php 
		while ($row2=$statement->fetch_array($result2)) {
			echo "<tr>";
			echo "<td width=\"106.4\" height=\"19\"><div align=\"center\">$row2[0]</div></td>";
			echo "<td width=\"228\" height=\"19\">&nbsp;&nbsp;&nbsp;&nbsp;$row2[1]</td>";
			echo "<td width=\"79.8\" height=\"19\"><div align=\"center\">0</div></td>";
			echo "<td width=\"57\" height=\"19\"><div align=\"center\">$row2[2]</div></td>";
			echo "<td width=\"95\" height=\"19\"><div align=\"left\">&nbsp;&nbsp;".number_format($row2[3],2)."</div></td>";
			echo "<td width=\"106.4\" height=\"19\"><div align=\"left\">".number_format($row2[4],2)."</div></td>";
			echo "</tr>";
		}
	  ?>
	    </table>
	</div>
	<!-- TOTAL -->
	<table width="760" border="0">
		<tr>
	   	  	<td width="280.8" height="22.8"></td>
	        <td width="95" height="22.8"><div align="center"><?php echo number_format($row1['6'],2)?></div></td>
	        <td width="90" height="22.8"><div align="center"><?php echo number_format($row1['7'],2)?></div></td>
	        <td width="80" height="22.8"><div align="left"><?php echo number_format($row1['8'],2)?></div></td>
						<td style="text-align: left">
							<?php 
								if($pajak !== false) {
									echo number_format($row1['9'],2);
								} else {
									echo "0.00";
								}
							?>
						</td>
	        <td width="120.6" height="22.8"><div align="left"><?php echo number_format($row1['10'],2)?></div></td>
	    </tr>
	</table>
	<table width="760" border="0">
		<tr>
	    	<td width="760" height="26.6"><div align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ucwords(Terbilang($row1['10'])); ?></div></td>
	    </tr>
	</table>
	<table width="760" border="0">
		<tr>
	    	<td width="661.2" height="66"></td>
	        <td width="98.8" height="66"></td>
	    </tr>
	</table>
	<table width="760" border="0">
		<tr>
	    	<td width="470.2" height="19"></td>
	        <td width="108.8" height="19"><div align="left" class="style1">DENY SURYAWAN</div></td>
	    </tr>
	</table>

	<div class="row no-print">
		<div class="col-xs-12">
			<br>
			<br>
			<button class="btn btn-primary" onclick="window.print();">
				<i class="fa fa-print"></i> Print
			</button>
		</div>
	</div>
</div>
<!-- /.content -->