<?php
namespace helper;
class Helper {
	function Terbilang($x) {
		$abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		if ($x < 12)
			return " " . $abil[$x];
		elseif ($x < 20)
			return $this->Terbilang($x - 10) . "belas";
		elseif ($x < 100)
			return $this->Terbilang($x / 10) . " puluh" . $this->Terbilang($x % 10);
		elseif ($x < 200)
			return " seratus" . $this->Terbilang($x - 100);
		elseif ($x < 1000)
			return $this->Terbilang($x / 100) . " ratus" . $this->Terbilang($x % 100);
		elseif ($x < 2000)
			return " seribu" . $this->Terbilang($x - 1000);
		elseif ($x < 1000000)
			return $this->Terbilang($x / 1000) . " ribu" . $this->Terbilang($x % 1000);
		elseif ($x < 1000000000)
			return $this->Terbilang($x / 1000000) . " juta" . $this->Terbilang($x % 1000000);
		elseif ($x < 10000000000)
			return $this->Terbilang($x / 10000000) . " Milyar" . $this->Terbilang($x % 1000000);
		elseif ($x < 100000000000)
			return $this->Terbilang($x / 100000000) . " Trilyun" . $this->Terbilang($x % 1000000);
	}
}