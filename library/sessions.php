<?php 
    class session { 
        // session-lifetime 
        var $lifeTime; 
        // mysql-handle 
        var $dbHandle;
        var $conn;

        function __construct($conn) {
        	$this->dbHandle=$conn;
        }
        function open($savePath, $sessName) { 
           // get session-lifetime 
           $this->lifeTime = get_cfg_var("session.gc_maxlifetime"); 
           // open database-connection  
           // return success 
           if(!$this->dbHandle) 
               return false; 
           $this->dbHandle = $this->dbHandle; 
           return true; 
        } 
        function close() { 
            $this->gc(ini_get('session.gc_maxlifetime')); 
            // close database-connection 
            return @mysql_close($this->dbHandle); 
        } 
        function read($sessID) { 
            // fetch session-data 
            $query = "SELECT SESSION_DATA FROM tmp_session 
                                WHERE SESSION_ID = '$sessID'";
            $data = mysql_query($query);
            // $res = mysql_query("SELECT userdata AS d FROM SQL1017.[RBAC].[dbo].[TMPSESSION] 
            //                     WHERE sessionid = '$sessID' 
            //                     AND sessexpired > ".time()); 
            // return data or an empty string at failure 
            if($data) 
            	$row = mysql_fetch_array($data);
                return $row[0]; 
            return ""; 
        } 
        function write($sessID,$sessData) { 
            // new session-expire-time 
            $newExp = time() + $this->lifeTime; 
            // is a session with this id in the database? 
            $query = "SELECT * FROM tmp_session 
                                WHERE SESSION_ID = '$sessID'";
            $data = mysql_query($query);
            // if yes, 
            if(mysql_num_rows($data)) { 
                // ...update session-data 
                $query = "UPDATE tmp_session 
                             SET LIFE_TIME = '$newExp',
                             SESSION_NAME = '".$_SESSION['username']."',
                             MODIFIED = '$newExp',
                             SESSION_DATA = '$sessData' 
                             WHERE SESSION_ID = '$sessID'";
                // if something happened, return true 
                if(mysql_query($query)) 
                    return true; 
            } 
            // if no session-data was found, 
            else { 
                // create a new row 
                $ipaddress = '';
                if ($_SERVER['HTTP_CLIENT_IP'])
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if($_SERVER['HTTP_X_FORWARDED_FOR'])
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if($_SERVER['HTTP_X_FORWARDED'])
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if($_SERVER['HTTP_FORWARDED_FOR'])
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if($_SERVER['HTTP_FORWARDED'])
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if($_SERVER['REMOTE_ADDR'])
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';
                $ipaddr =  $ipaddress;
                $agent = $_SERVER['HTTP_USER_AGENT'];
                $access = time();
                $servername = gethostbyname($_SERVER['SERVER_NAME']);
                $query = "INSERT INTO tmp_session
                               (SESSION_ID
                               ,SESSION_NAME
                               ,IP_ADDRESS
                               ,USER_AGENT
                               ,SERVER_NAME
                               ,MODIFIED
                               ,LIFE_TIME
                               ,SESSION_DATA)
                         VALUES
                               ('$sessID'
                               ,NULL
                               ,'$ipaddr'
                               ,'$agent'
                               ,'$servername'
                               ,'$access'
                               ,'$newExp'
                               ,NULL)";
                // if row was created, return true 
                if(mysql_query($query))
                    return true; 
            } 
            // an unknown error occured 
            return false; 
        } 
        function destroy($sessID) { 
            // delete session-data 
            $query = "DELETE FROM tmp_session WHERE SESSION_ID = '$sessID'";
            // if session was deleted, return true, 
            if(mysql_query($query)) 
                return true; 
            // ...else return false 
            return false; 
        } 
        function gc($sessMaxLifeTime) { 
            // delete old SQL1017.[RBAC].[dbo].[TMPSESSION] 
            $query = "DELETE FROM tmp_session WHERE LIFE_TIME < ".time();
            // return affected rows 
            return mysql_query($query); 
        }  
    } 
    $session = new session($conn);  
    session_set_save_handler(array(&$session,"open"), 
                             array(&$session,"close"), 
                             array(&$session,"read"), 
                             array(&$session,"write"), 
                             array(&$session,"destroy"), 
                             array(&$session,"gc")); 
    session_start(); 