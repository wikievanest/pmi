<?php

require_once('AES.class.php');

class Enkripsi {
	function Encrypt($x) {
		$Cipher = new AES();
		// kunci enkripsi (Anda bisa memodifikasi kuncinya)
		$key_128bit = '603DeB1015cA71BE2b73Xef0857d77811F352C073X6108D72F9810a30914dGf4';
	
		// membagi panjang string yang akan dienkripsi dengan panjang 16 karakter
		$n = ceil(strlen($x)/16);
		$encrypt = "";
	
		for ($i=0; $i<=$n-1; $i++){
		// mengenkripsi setiap 16 karakter
		$cryptext = $Cipher->encrypt($Cipher->stringToHex(substr($x, $i*16, 16)), $key_128bit);
		// menggabung hasil enkripsi setiap 16 karakter menjadi satu string enkripsi utuh
		$encrypt .= $cryptext;
		}
	
		return $encrypt;
	}
	
	function Decrypt($x){
		$Cipher = new AES();
		// kunci dekripsi (kunci ini harus sama dengan kunci enkripsi)
		$key_128bit = '603DeB1015cA71BE2b73Xef0857d77811F352C073X6108D72F9810a30914dGf4';
	
		// karena string hasil enkripsi memiliki panjang 32 karakter, maka untuk proses dekripsi ini panjang string dipotong2 dulu menjadi 32 karakter
	
		$n = ceil(strlen($x)/32);
		$decrypt = "";
	
		for ($i=0; $i<=$n-1; $i++){
		// mendekrip setiap 32 karakter hasil enkripsi
		$result = $Cipher->decrypt(substr($x, $i*32, 32), $key_128bit);
		// menggabung hasil dekripsi 32 karakter menjadi satu string dekripsi utuh
		$decrypt .= $Cipher->hexToString($result);
		}
		return $decrypt;
	}
	
	function decode($x){
	// proses decoding: memecah parameter dan masing-masing value yang terkait
	
		$pecahURI = explode('?id=', $x);
		$parameter = $pecahURI[1];
	
		$pecahParam = explode('&', paramDecrypt($parameter));
	
		for ($i=0; $i <= count($pecahParam)-1; $i++) {
		$decode = explode('=', $pecahParam[$i]);
		$var[$decode[0]] = $decode[1];
		}
	
		return $var;
	}
}
?>