<?php
class Image {
	
	function getimage($str) {
		$mime = 'image/jpeg';
		$hasil = 'data:' . $mime . ';base64,' . $str;
		return $hasil;
	}
	function saveImage($fileTmp,$fileSize,$fileError,$fileTipe,$filePath) {
			$move = move_uploaded_file($fileTmp, $filePath);
			$lokasi=$filePath;
			$image=$lokasi;
				
			$orig_image = imagecreatefromjpeg($image);
			$image_info = getimagesize($image);
			$width_orig  = $image_info[0]; // Mendapatkan ukuran panjang foto asli
			$height_orig = $image_info[1]; // Mendapatkan ukuran lebar foto asli
			$width = 200; // Ukuran Panjang
			$height = 200; // Ukuran Lebar
			$destination_image = imagecreatetruecolor($width, $height);
			imagecopyresampled($destination_image, $orig_image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
			imagejpeg($destination_image, $image, 100);
				
			$data = fopen ($image, 'rb');
			$size=filesize ($image);
			$contents= fread ($data, $size);
			fclose ($data);
			$encoded= base64_encode($contents);
			//unlink($lokasi);
		
			return $encoded;
		}
}

?>